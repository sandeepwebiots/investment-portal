<?php $__env->startSection('title'); ?> Packages <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
} 
.checked {
  color: orange;
}  
.m-form .form-control-label, .m-form label {
    font-weight: 400;
    font-size: 1rem;
    cursor: pointer;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Our Plans</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home "></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Our Plans</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Investment Packages
                            </h3>
                        </div>
                    </div>
                </div>
                <form class="m-form" method="post" action="<?php echo e(route('package.select')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <div class="alert m-alert m-alert--default" role="alert">
                                You need to do deposit first before you purchase the package. <a href="<?php echo e(url('deposit')); ?>"> Click here to Deposit</a>
                            </div>
                        </div>
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group">
                                <?php if($user->referral_id): ?>
                                    <?php if($user->parent_id == ''): ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        <strong>Note</strong> Your position is not set yet&nbsp;! &nbsp;&nbsp;Please say your Referrer to set your position.
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if($errors->has('package')): ?>
                                <div class="m-alert m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    <strong>Error</strong> <?php echo e($errors->first('package')); ?>

                                </div>
                                <?php endif; ?>
                                <div class="row">
                                    <?php $__currentLoopData = $package; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pack): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-lg-6">
                                        <label class="m-option">
                                            <span class="m-option__control">
                                                <span class="m-radio m-radio--brand m-radio--check-bold">
                                                    <input type="radio" name="package" id="radio<?php echo e($pack->id); ?>" value="<?php echo e($pack->id); ?>">
                                                    <span></span>
                                                </span>
                                            </span>
                                            <span class="m-option__label">
                                                <span class="m-option__head">
                                                    <span class="m-option__title">
                                                        <?php echo e($pack->title); ?> 
                                                    </span>
                                                    <span class="m-option__focus">
                                                       <span class="m-badge m-badge--accent m-badge--wide"> $ <?php echo e($pack->min); ?> - $ <?php echo e($pack->max); ?> </span>
                                                    </span>
                                                </span>
                                                <?php if($pack->duration == '35'): ?>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                <?php elseif($pack->duration == '40'): ?>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                <?php elseif($pack->duration == '45'): ?>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                <?php elseif($pack->duration == '50'): ?>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                <?php endif; ?>
                                                <span class="m-option__body">
                                                    Estimated &nbsp;<?php echo e($pack->profit); ?>% daily ROI for <?php echo e($pack->duration); ?> weeks
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions m--align-right">
                            <button type="submit" class="btn btn m-btn--pill m-btn--air btn-outline-accent">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>