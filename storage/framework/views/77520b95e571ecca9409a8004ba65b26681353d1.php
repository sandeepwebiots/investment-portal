<?php $__env->startSection('title'); ?> Investment Portal | Wel-Come <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<style>
    .morecontent span {
        display: none;
    }
    .morelink {
        display: block;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<!-- slider -->
<section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_home-1" style="height: 85vh;">
    <div class="rev_slider_wrapper fullscreen-container" id="rev_slider_1_1_wrapper">
        <div class="rev_slider fullscreenbanner" data-version="5.2.6" id="rev_slider_1_1">
            <ul>
                <li data-description="" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1" data-masterspeed="default,default,default,default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0,0,0,0" data-saveperformance="off" data-slotamount="default,default,default,default" data-thumb="images/slider_1-100x50.jpg" data-title="Slide" data-transition="slidingoverlayup,slidingoverlaydown,slidingoverlayright,slidingoverlayleft">
                    <img  alt="" class="rev-slidebg" data-bgfit="cover" data-bgposition="bottom" data-bgrepeat="no-repeat" data-no-retina="" height="1079" src="<?php echo e(URL::asset('assets/home/images/img/1.jpg')); ?>" title="slider_1" width="1920">
                    <div class="tp-caption trx-big tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-voffset="-40" data-width="['auto']" data-x="center" data-y="center" id="slide-1-layer-1">
                        Consulting & Investment.
                    </div>
                    <div class="tp-caption trx-norm tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','on','off']" data-voffset="46" data-width="['auto']" data-x="center" data-y="center" id="slide-1-layer-2">
                        Preparing your money is a daunting challenge for today's investors.<br>
                        Will give you a complete account of the system.
                    </div>
                    <div class="tp-caption trx-no-css tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','off','off']" data-voffset="137" data-width="['auto']" data-x="center" data-y="center" id="slide-1-layer-3">
                        <a class="sc_button sc_button_style_filled sc_button_size_medium" href="#">Read more</a>&nbsp;&nbsp;<a class="sc_button sc_button_style_filled2 sc_button_size_medium light_color" href="#">Purchase</a>
                    </div>
                </li>
                <li data-description="" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-2" data-masterspeed="default,default,default,default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0,0,0,0" data-saveperformance="off" data-slotamount="default,default,default,default" data-thumb="images/slider_2-100x50.jpg" data-title="Slide" data-transition="slidingoverlayright,slidingoverlayleft,slidingoverlayup,slidingoverlaydown">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgposition="bottom" data-bgrepeat="no-repeat" data-no-retina="" height="1076" src="<?php echo e(URL::asset('assets/home/images/img/2.jpg')); ?>" title="slider_2" width="1920">
                    <div class="tp-caption trx-big tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-voffset="-40" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-1">
                        The Best Business Support
                    </div>
                    <div class="tp-caption trx-norm tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','on','off']" data-voffset="46" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-2">
                        Preparing your money is a daunting challenge for today's investors.<br>
                        Will give you a complete account of the system.
                    </div>
                    <div class="tp-caption trx-no-css tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','off','off']" data-voffset="137" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-3">
                        <a class="sc_button sc_button_style_filled sc_button_size_medium" href="#">Read more</a>
                    </div>
                </li>
                <li data-description="" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-3" data-masterspeed="default,default,default,default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0,0,0,0" data-saveperformance="off" data-slotamount="default,default,default,default" data-thumb="images/slider_3-100x50.jpg" data-title="Slide" data-transition="slidingoverlayup,slidingoverlaydown,slidingoverlayright,slidingoverlayleft">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgposition="bottom" data-bgrepeat="no-repeat" data-no-retina="" height="1076" src="<?php echo e(URL::asset('assets/home/images/img/3.jpg')); ?>" title="slider_3" width="1920">
                    <div class="tp-caption trx-big tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-voffset="-40" data-width="['auto']" data-x="center" data-y="center" id="slide-3-layer-1">
                        We Have Talented Team
                    </div>
                    <div class="tp-caption trx-norm tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','on','off']" data-voffset="46" data-width="['auto']" data-x="center" data-y="center" id="slide-3-layer-2">
                        Preparing your money is a daunting challenge for today's investors.<br>
                        Will give you a complete account of the system.
                    </div>
                    <div class="tp-caption trx-no-css tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','off','off']" data-voffset="137" data-width="['auto']" data-x="center" data-y="center" id="slide-3-layer-3">
                        <a class="sc_button sc_button_style_filled sc_button_size_medium" href="#">Read more</a>&nbsp;&nbsp;<a class="sc_button sc_button_style_filled2 sc_button_size_medium light_color" href="#">Contact Us</a>
                    </div>
                </li>
                <li data-description="" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-2" data-masterspeed="default,default,default,default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0,0,0,0" data-saveperformance="off" data-slotamount="default,default,default,default" data-thumb="images/slider_2-100x50.jpg" data-title="Slide" data-transition="slidingoverlayright,slidingoverlayleft,slidingoverlayup,slidingoverlaydown">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgposition="bottom" data-bgrepeat="no-repeat" data-no-retina="" height="1076" src="<?php echo e(URL::asset('assets/home/images/img/4.jpg')); ?>" title="slider_2" width="1920">
                    <div class="tp-caption trx-big tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-voffset="-40" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-1">
                        The Best Business Support
                    </div>
                    <div class="tp-caption trx-norm tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','on','off']" data-voffset="46" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-2">
                        Preparing your money is a daunting challenge for today's investors.<br>
                        Will give you a complete account of the system.
                    </div>
                    <div class="tp-caption trx-no-css tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','off','off']" data-voffset="137" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-3">
                        <a class="sc_button sc_button_style_filled sc_button_size_medium" href="#">Read more</a>
                    </div>
                </li>
                <li data-description="" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-2" data-masterspeed="default,default,default,default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0,0,0,0" data-saveperformance="off" data-slotamount="default,default,default,default" data-thumb="images/slider_2-100x50.jpg" data-title="Slide" data-transition="slidingoverlayright,slidingoverlayleft,slidingoverlayup,slidingoverlaydown">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgposition="bottom" data-bgrepeat="no-repeat" data-no-retina="" height="1076" src="<?php echo e(URL::asset('assets/home/images/img/5.jpg')); ?>" title="slider_2" width="1920">
                    <div class="tp-caption trx-big tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-voffset="-40" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-1">
                        The Best Business Support
                    </div>
                    <div class="tp-caption trx-norm tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','on','off']" data-voffset="46" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-2">
                        Preparing your money is a daunting challenge for today's investors.<br>
                        Will give you a complete account of the system.
                    </div>
                    <div class="tp-caption trx-no-css tp-resizeme" data-end="8700" data-height="['auto']" data-hoffset="" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="900" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:600;" data-visibility="['on','on','off','off']" data-voffset="137" data-width="['auto']" data-x="center" data-y="center" id="slide-2-layer-3">
                        <a class="sc_button sc_button_style_filled sc_button_size_medium" href="#">Read more</a>
                    </div>
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>
<!-- slider end -->
<!-- content start -->
<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-4 sc_services_type_icons_img margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">We are the First Mover...</h2>
                    <div class="sc_services_descr sc_item_descr">
                        A unique business idea that is 100% realistic! You will never have heard about such a fantastic plan ever before!
                    </div>
                    <div class="sc_columns columns_wrap"  data-aos="zoom-out-down" data-aos-duration="3000">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="top_post_image">
                                    <a href="#"><img alt="" class="services-post-image" src="<?php echo e(URL::asset('assets/home/images/service_04.png')); ?>"></a>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="#">Market Forecasting</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Detailed analysis of market by our expert marketers! They forecast on the basis of demand and competition of different products.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_2">
                                <div class="top_post_image">
                                    <a href="#"><img alt="" class="services-post-image" src="<?php echo e(URL::asset('assets/home/images/service_05.png')); ?>"></a>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="#">Flexible Investment plans</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Investment plans being offered here are flexible and hence suitable not only for beginners but also for investment pros.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_3">
                                <div class="top_post_image">
                                    <a href="#"><img alt="" class="services-post-image" src="<?php echo e(URL::asset('assets/home/images/service_06.png')); ?>"></a>
                                </div>
                                <div class="sc_services_item_content">
                                    <h4 class="sc_services_item_title"><a href="#">Secure Revenue Generating</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>We assess the markets, sales data and economic indicators professionally so as to generate secure revenue for all.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="packages-section text-centersc_section custom_bg_1"  data-aos="fade-down-right" data-aos-duration="3000">
<!-- <div class="container">
<div class="row">
<div class="main-title">
    <h2 class="sc_services_title sc_item_title" data-aos="fade-left" data-aos-duration="3000">Our Packages</h2>
    <div class="sc_services_descr sc_item_descr">
                      Explore opportunities
                    </div>
<div class="line"></div>
</div>
<div class="col-md-3 col-sm-4 col-xs-6">
<a href="#" class=""><img src="<?php echo e(URL::asset('assets/home/images/thumb_18_default_medium.png')); ?>" alt="" class="img-responsive img-fluid m-0-auto"></a>
 <div class="sc_services_item_content text-center">
<h4 class=" sc_services_item_title">
ROOKIE PACKAGE
<div class="sc_services_item_description">
                                       <p class="color-gray">Free</p>
                                    </div>
</h4>
</div>
</div>
<div class="col-md-3 col-sm-4 col-xs-6">
<a href="#" class=""><img src="<?php echo e(URL::asset('assets/home/images/thumb_19_default_medium.png')); ?>" alt="" class="img-responsive img-fluid m-0-auto"></a>
 <div class="sc_services_item_content text-center">
<h4 class=" sc_services_item_title">
STARTER PACKAGE
<div class="sc_services_item_description">
                                        <p class="color-gray">Free</p>
                                    </div>
</h4>
</div>
</div>
<div class="col-md-3 col-sm-4 col-xs-6">
<a href="#" class=""><img src="<?php echo e(URL::asset('assets/home/images/thumb_20_default_medium.png')); ?>" alt="" class="img-responsive img-fluid m-0-auto"></a>
 <div class="sc_services_item_content text-center">
<h4 class=" sc_services_item_title ">
TRADER PACKAGE
<div class="sc_services_item_description">
                                        <p class="color-gray">Free</p>
                                    </div>
</h4>
</div>
</div>
<div class="col-md-3 col-sm-4 col-xs-6">
<a href="#" class=""><img src="<?php echo e(URL::asset('assets/home/images/thumb_21_default_medium.png')); ?>" alt="" class="img-responsive img-fluid m-0-auto"></a>
 <div class="sc_services_item_content text-center">
<h4 class=" sc_services_item_title">
PRO TRADER PACKAGE
<div class="sc_services_item_description">
                                       <p class="color-gray">Free</p>
                                    </div>
</h4>
</div>
</div>
</div>
</div> -->
</section>
    <div class="sc_section custom_bg_1"  data-aos="fade-down-right" data-aos-duration="3000">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">What is drop shipping?</h2>
                    <div class="sc_services_descr sc_item_descr">
                        Many of you will have heard about “drop shipping” term a lot. You might be having confusion in your mind regarding this business. Let’s open the mystery box and get to know that what Drop shipping is all about! Well, it is a type of business in which real products are traded but the person who is involved in drop shipping business does not maintain any inventory or does not own any store. Surprised, how!
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/investment/2.jpg')); ?>"></a>
                                    </div>
                                    <!-- <div class="sc_services_item_count">
                                        01
                                    </div> -->
                                </div>
                                <div class="sc_services_item_content">
                                    <!-- <span class="sc_services_item_subtitle"></span> -->
                                    <h4 class="sc_services_item_title">What is Drop Shipping business about!</h4>
                                    <div class="sc_services_item_description">
                                        <p class="more">A person who is doing drop shipping does not store the products anywhere or he does not own the products physically. All he has to do is to work like a middle man. He will be taking the orders from the customer for any product and then he will place order for the same product from wholesaler telling him to deliver them at the address of the customer from whom he actually took the order. </p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_2">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/investment/4.jpg')); ?>"></a>
                                    </div>
                                    <!-- <div class="sc_services_item_count">
                                        02
                                    </div> -->
                                </div>
                                <div class="sc_services_item_content">
                                    <!-- <span class="sc_services_item_subtitle">The Best Corporate Company</span> -->
                                    <h4 class="sc_services_item_title">Drop shipper earns from his marketing skills:</h4>
                                    <div class="sc_services_item_description">
                                        <p class="more">If you want to be a drop shipper then you have to be good at marketing. You must have good marketing skills so that you can analyze market trends, competition of different products, compare the prices of the products and can forecast the profit. A drop shipper should not only be good at satisfying his customer but on the other side, he should have a good relationship with wholesaling company.</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_3">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/investment/5.jpg')); ?>"></a>
                                    </div>
                                   <!--  <div class="sc_services_item_count">
                                        03
                                    </div> -->
                                </div>
                                <div class="sc_services_item_content">
                                    <!-- <span class="sc_services_item_subtitle">Always Forvard</span> -->
                                    <h4 class="sc_services_item_title"><a href="#">Is Drop shipping an online business?</a></h4>
                                    <div class="sc_services_item_description"><p class="more">Drop shipping is entirely an online business in which shop shipper is engaged in selling tangible products but even without holding their possession. Whole seller or the manufacturer will be maintaining the stock. These days, everything is getting online. You see that there are a big number of online stores and many of them are not engaged in retailing or wholesaling business but they are doing drop shipping business. Such websites are taking orders from the customers at expensive rates and they are placing orders for the same products at cheaper rates from the companies manufacturing those products. Hence, they are generating huge profit margin and even without any fatigue of managing the inventory.Don’t you think that drop shipping is such an appealing type of business that you can incorporate just with little investment and even without any hassle! If you are fed up of your job and don’t want to bear your boss anymore then start your own drop shipping business and be your own boss! </p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sc_section custom_bg_1"  data-aos="fade-down-right" data-aos-duration="3000">
        <div class="content_wrap">
            <div class="">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge">
                    <h2 class="sc_services_title sc_item_title">Why Us?</h2>
                    <div class="sc_services_descr sc_item_descr">
                        Why only us? Why you should rely on our services? What is so special that you should choose to invest here! Well, there are many reasons that will induce you to invest your money here. Here are a few things that will answer you that why you should choose us only? 
                    </div>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            <div class="panel-heading">
                              <h5 class="panel-title">We offer a solid investment plan</h5>
                            </div>
                          </a>
                          <div id="collapse1" class="panel-collapse in">
                            <div class="panel-body"> 
                              <p>At ABC investment, you will find a solid business plan that is realistic and practical. You have seen that many business plans in the internet field are just scam. Neither those plans are profitable not are those realistic. You can invest your money confidently here because it is the business related to purchases and sales and real trading will be done using the amount that our clients will be investing.</p>
                            </div>    
                          </div>
                        </div>
                        <div class="panel panel-default">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                            <div class="panel-heading">
                              <h5 class="panel-title">We are well-reputed</h5>
                            </div>
                          </a>
                          <div id="collapse2" class="panel-collapse collapse collapse">
                            <div class="panel-body">
                              <p>Reputation of any organization really matters and we take pride in being well reputed. We have been providing services in the same field for many years and that's why we have succeeded to gain the confidence of many clients. We have been long term relationship with them. Hence, we are happy to announce that we are a trustworthy organization where you can bring in your money confidently and blindly.</p>
                            </div> 
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                            <div class="panel-heading">
                              <h5 class="panel-title">Our services are the best</h5>
                            </div>
                          </a>
                          <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p>We are trying really hard to provide our clients the best services. Either it comes to business related services or customer support services, we claim to be the best. We have a team of experts who have been providing marketing services and that are why we guarantee a successful and profitable business.</p>
                            </div> 
                          </div>
                        </div>
                        <div class="panel panel-default">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                            <div class="panel-heading">
                              <h5 class="panel-title">We guarantee profit</h5>
                            </div>
                          </a>
                          <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p>You are not going to waste even a single penny and the entire amount that you will be investing here will be profitable. We have already mentioned that we have the team of expert market who are skilled enough to analyze the markets properly and to perform business activities accordingly. Hence, your investment will not be wasted at all but it will definitely bring a lot of profit. Not only we claim profit but we claim "big profit".</p>
                            </div> 
                          </div>
                        </div>
                        <div class="panel panel-default">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                            <div class="panel-heading">
                              <h5 class="panel-title">We welcome everyone</h5>
                            </div>
                          </a>
                          <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p>At ABC Investment, we welcome everyone to invest. We offer a flexible business plan in which there is no limit of maximum investment. Whatever amount you want to invest, you can take the start and when you will start getting the profit then you can bring in more money.</p>
                              <p>Now you have come to know that why you should prefer us to invest your money. We guarantee huge profit and we guarantee that you will not be facing any sort of loss by investing your money here. You are provided with a solid business plan that is not only valid but it is also profitable. Hence, think no more and bring and your money if you are interested to run your own business and to produce more and more profit out of your existing money. Already, there are many clients who have been working with us and they are earning the profit on weekly basis.</p>
                            </div> 
                          </div>
                        </div>
                    </div>
                </div><!-- /.sc_services -->
            </div><!-- /.sc_services_wrap -->
        </div>
    </div>
    
     <div class="sc_section custom_bg_2 container payment-gateway">
        <div class="sc_empty_space" data-height="2em"></div>
        <div class="sc_testimonials sc_testimonials_style_testimonials-1 margin_bottom_huge aligncenter">
            <h2 class="sc_testimonials_title sc_item_title">Payment gateway</h2>
            <div class="sc_testimonials_descr sc_item_descr">
                Testimonials from customers who are satisfied with services that have been effectively<br>
                used in marketing for as long as marketing exists.
            </div>
            <div class="sc_slider_swiper swiper-slider-container sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols" data-interval="7440" data-slides-min-width="150" data-slides-per-view="1">
                <div class="slides swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="sc_testimonial_item row">
                            <div class="sc_testimonial_avatar column-1_2"><img alt="" src="<?php echo e(URL::asset('assets/home/images/payment-gateway/1.jpg')); ?>"></div>
                            <div class="sc_testimonial_content column-1_2">
                                 <div class="sc_testimonial_author">
                                <span class="sc_testimonial_author_name">James Watson</span>
                            </div>
                                <p>Being a company's director requires maximum attention and devotion. This was exactly what I felt when turned to your products and services. All our questions and inquiries were answered effectively and right away. Our website has never looked better, ever. Thank you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="sc_testimonial_item row">
                            <div class="sc_testimonial_avatar column-1_2"><img alt="" src="<?php echo e(URL::asset('assets/home/images/payment-gateway/2.jpg')); ?>"></div>
                            <div class="sc_testimonial_content column-1_2">
                                 <div class="sc_testimonial_author">
                                <span class="sc_testimonial_author_name">Lisa Larson</span>
                            </div>
                                <p>You never know what is gooing to happen until you try. But let me tell you that taking risk with these guys was totally worth it. Now we are a regular client, and this was probably the best decision we ever made! Our company appreciates your assistance and great work!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sc_slider_controls_wrap">
                    <a class="sc_slider_prev" href="#"></a> <a class="sc_slider_next" href="#"></a>
                </div>
                <div class="sc_slider_pagination_wrap"></div>
            </div>
        </div>
        <div class="sc_empty_space" data-height="2em"></div>
    </div>
    <div class="sc_section custom_section_1">
        <div class="columns_wrap no_margins custom_bg_1">
            <!-- <div class="column-1_4">
                <div class="sc_empty_space" data-height="35px"></div>
                <div class="sc_section margin_top_small margin_right_small margin_bottom_large margin_left_small aligncenter">
                    <div class="sc_section_inner">
                        <div class="sc_section_content_wrap">
                            <div class="sc_empty_space" data-height="0.5em"></div>
                            <figure class="sc_image sc_image_shape_square">
                                <img alt="" src="<?php echo e(URL::asset('assets/home/images/colored_service_1.png')); ?>">
                            </figure>
                            <div class="sc_empty_space" data-height="0.7em"></div>
                            <h4 class="sc_title sc_title_regular margin_top_tiny margin_bottom_tiny">Investment Banking</h4>
                            <h6 class="sc_custom_heading style_1 custom_cl_3">Perspiciatis unde omnis</h6><span>Perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium.</span>
                            <div class="sc_empty_space" data-height="1.1em"></div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="column-1_3 custom_bg_3">
                <div class="sc_empty_space" data-height="35px"></div>
                <div class="sc_section margin_top_small margin_right_small margin_bottom_large margin_left_small aligncenter">
                    <div class="sc_section_inner">
                        <div class="sc_section_content_wrap">
                            <div class="sc_empty_space" data-height="0.5em"></div>
                            <figure class="sc_image sc_image_shape_square">
                                <img alt="" src="<?php echo e(URL::asset('assets/home/images/colored_service_2.png')); ?>">
                            </figure>
                            <div class="sc_empty_space" data-height="0.7em"></div>
                            <h4 class="sc_title sc_title_regular margin_top_tiny margin_bottom_tiny custom_cl_4">Products selling & Drop shipping</h4><span class=".custom_cl_6">We analyze the trends in the markets, make forecasting reports and then we offer profitable sales and trading plans to our clients.</span>
                            <div class="sc_empty_space" data-height="1.1em"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column-1_3 custom_bg_4">
                <div class="sc_empty_space" data-height="35px"></div>
                <div class="sc_section margin_top_small margin_right_small margin_bottom_large margin_left_small aligncenter">
                    <div class="sc_section_inner">
                        <div class="sc_section_content_wrap">
                            <div class="sc_empty_space" data-height="0.5em"></div>
                            <figure class="sc_image sc_image_shape_square">
                                <img alt="" src="<?php echo e(URL::asset('assets/home/images/colored_service_3.png')); ?>">
                            </figure>
                            <div class="sc_empty_space" data-height="0.7em"></div>
                            <h4 class="sc_title sc_title_regular margin_top_tiny margin_bottom_tiny custom_cl_4">Investment Management</h4>
                            <span class="custom_cl_6">Perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium.</span>
                            <div class="sc_empty_space" data-height="1.1em"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column-1_3 custom_bg_5">
                <div class="sc_empty_space" data-height="35px"></div>
                <div class="sc_section margin_top_small margin_right_small margin_bottom_large margin_left_small aligncenter">
                    <div class="sc_section_inner">
                        <div class="sc_section_content_wrap">
                            <div class="sc_empty_space" data-height="0.5em"></div>
                            <figure class="sc_image sc_image_shape_square">
                                <img alt="" src="<?php echo e(URL::asset('assets/home/images/colored_service_4.png')); ?>">
                            </figure>
                            <div class="sc_empty_space" data-height="0.7em"></div>
                            <h4 class="sc_title sc_title_regular margin_top_tiny margin_bottom_tiny custom_cl_4">Market Research</h4>
                            <span class="custom_cl_6">Our professional marketers are pro at researching the market so that every penny invested can pay back big profit to our clients.</span>
                            <div class="sc_empty_space" data-height="1.1em"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    <div class="sc_section custom_bg_2 container">
        <div class="sc_empty_space" data-height="2em"></div>
        <div class="sc_testimonials sc_testimonials_style_testimonials-1 margin_top_huge aligncenter">
            <h2 class="sc_testimonials_title sc_item_title">Testimonials</h2>
            <div class="sc_testimonials_descr sc_item_descr">
                Testimonials from customers who are satisfied with services that have been effectively<br>
                used in marketing for as long as marketing exists.
            </div>
            <div class="sc_slider_swiper swiper-slider-container sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols" data-interval="7440" data-slides-min-width="150" data-slides-per-view="1">
                <div class="slides swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="sc_testimonial_item">
                            <div class="sc_testimonial_avatar"><img alt="" src="<?php echo e(URL::asset('assets/home/images/testimonials-1-80x80.jpg')); ?>"></div>
                            <div class="sc_testimonial_content">
                                <p>In every business, investors take the risk and I thought of taking the risk to invest in this platform. I highly appreciate not only their services but also their loyalty and sincerity. I have been paid what was promised. Feeling so proud to be a partner with them! </p>
                            </div>
                            <div class="sc_testimonial_author">
                                <span class="sc_testimonial_author_name">Lisa Larson</span> <span class="sc_testimonial_author_position">Company Director</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="sc_testimonial_item">
                            <div class="sc_testimonial_avatar"><img alt="" src="<?php echo e(URL::asset('assets/home/images/testimonials-3-80x80.jpg')); ?>"></div>
                            <div class="sc_testimonial_content">
                                <p>Everything is bad until proven good, I believed in this always. I am an investor in this platform l and it has been proven not only good but simply fantastic to me.</p>
                            </div>
                            <div class="sc_testimonial_author">
                                <span class="sc_testimonial_author_name">James Watson</span> <span class="sc_testimonial_author_position">Senior Manager</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="sc_testimonial_item">
                            <div class="sc_testimonial_avatar"><img alt="" src="<?php echo e(URL::asset('assets/home/images/testimonials-2-80x80.jpg')); ?>"></div>
                            <div class="sc_testimonial_content">
                                <p>I have different portfolios of investment and in this time where global markets are going through recession phase, I am glad to invest in this platform because it is paying me back handsome amount of money every week. </p>
                            </div>
                            <div class="sc_testimonial_author">
                                <span class="sc_testimonial_author_name">Chris Doe</span> <span class="sc_testimonial_author_position">Startup SEO</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="sc_testimonial_item">
                            <div class="sc_testimonial_avatar"><img alt="" src="<?php echo e(URL::asset('assets/home/images/testimonials-4-80x80.jpg')); ?>"></div>
                            <div class="sc_testimonial_content">
                                <p>Highly recommended this business to everyone. I have become a regular client here and I have taken the best decision of my life to invest my money here. I have been investing my profit as well so as to grow my business.</p>
                            </div>
                            <div class="sc_testimonial_author">
                                <span class="sc_testimonial_author_name">Susan Smith</span><span class="sc_testimonial_author_position">Handmade Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sc_slider_controls_wrap">
                    <a class="sc_slider_prev" href="#"></a> <a class="sc_slider_next" href="#"></a>
                </div>
                <div class="sc_slider_pagination_wrap"></div>
            </div>
        </div>
        <div class="sc_empty_space" data-height="2em"></div>
    </div>
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2.5em"></div>
            <div class="sc_clients_wrap">
                <div class="sc_clients sc_clients_style_clients-1 margin_top_huge">
                    <h2 class="sc_clients_title sc_item_title">Our Clients</h2>
                    <div class="sc_clients_descr sc_item_descr">
                        Perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque<br>
                        laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_6 column_padding_bottom">
                            <div class="sc_clients_item">
                                <div class="sc_client_image"><img alt="" src="<?php echo e(URL::asset('assets/home/images/client5.png')); ?>"></div>
                            </div>
                        </div>
                        <div class="column-1_6 column_padding_bottom">
                            <div class="sc_clients_item">
                                <div class="sc_client_image"><img alt="" src="<?php echo e(URL::asset('assets/home/images/client4.png')); ?> "></div>
                            </div>
                        </div>
                        <div class="column-1_6 column_padding_bottom">
                            <div class="sc_clients_item">
                                <div class="sc_client_image"><img alt="" src="<?php echo e(URL::asset('assets/home/images/client3.png')); ?>"></div>
                            </div>
                        </div>
                        <div class="column-1_6 column_padding_bottom">
                            <div class="sc_clients_item">
                                <div class="sc_client_image"><img alt="" src="<?php echo e(URL::asset('assets/home/images/client2.png')); ?>"></div>
                            </div>
                        </div>
                        <div class="column-1_6 column_padding_bottom">
                            <div class="sc_clients_item">
                                <div class="sc_client_image"><img alt="" src="<?php echo e(URL::asset('assets/home/images/client1.png')); ?>"></div>
                            </div>
                        </div>
                        <div class="column-1_6 column_padding_bottom">
                            <div class="sc_clients_item">
                                <div class="sc_client_image"><img alt="" src="<?php echo e(URL::asset('assets/home/images/client2.png')); ?>"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.sc_clients -->
            </div><!-- /.sc_clients_wrap -->
            <div class="sc_empty_space" data-height="0.5em"></div>
        </div>
    </div>
    <div class="sc_section">
                    <div class="content_wrap">
                        <div class="sc_empty_space" data-height="1.4em"></div>
                        <div class="sc_blogger layout_classic_3 template_masonry margin_top_huge margin_bottom_huge">
                            <h2 class="sc_blogger_title sc_item_title">Our Blog Posts</h2>
                            <div class="sc_blogger_descr sc_item_descr">
                                Perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque<br>
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.
                            </div>
                            <div class="isotope_wrap" data-columns="3">
                                <div class="isotope_item isotope_item_classic isotope_column_3">
                                    <div class="post_item post_item_classic post_format_standard">
                                        <div class="post_featured">
                                            <div class="post_thumb">
                                                <a class="hover_icon hover_icon_link" href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/post-3-370x270.jpg')); ?>"></a>
                                            </div>
                                            <div class="cat_post_info">
                                                <span class="post_categories"><a class="category_link" href="#">Business</a></span>
                                            </div>
                                        </div>
                                        <div class="post_content isotope_item_content">
                                            <h5 class="post_title"><a href="#">Money Market Rates Finding the Best Accounts in 2016</a></h5>
                                            <div class="post_info">
                                                <span class="post_info_item"><a class="post_info_date" href="#">April 26, 2016</a></span> 
                                            </div>
                                            <div class="post_descr">
                                                <p>Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque...</p><a class="post_readmore readmore" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="isotope_item isotope_item_classic isotope_column_3">
                                    <div class="post_item post_item_classic post_format_standard">
                                        <div class="post_featured">
                                            <div class="post_thumb">
                                                <a class="hover_icon hover_icon_link" href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/post-2-370x270.jpg')); ?>"></a>
                                            </div>
                                            <div class="cat_post_info">
                                                <span class="post_categories"><a class="category_link" href="#">Business</a></span>
                                            </div>
                                        </div>
                                        <div class="post_content isotope_item_content">
                                            <h5 class="post_title"><a href="#">Sustainable Investing from Bugs to Biodrying</a></h5>
                                            <div class="post_info">
                                                <span class="post_info_item"><a class="post_info_date" href="#">April 20, 2016</a></span> 
                                            </div>
                                            <div class="post_descr">
                                                <p>Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque...</p><a class="post_readmore readmore" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="isotope_item isotope_item_classic isotope_column_3">
                                    <div class="post_item post_item_classic post_format_standard">
                                        <div class="post_featured">
                                            <div class="post_thumb">
                                                <a class="hover_icon hover_icon_link" href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/post-6-370x270.jpg')); ?>"></a>
                                            </div>
                                            <div class="cat_post_info">
                                                <span class="post_categories"><a class="category_link" href="#">Funding Trends</a></span>
                                            </div>
                                        </div>
                                        <div class="post_content isotope_item_content">
                                            <h5 class="post_title"><a href="#">Stay in Trend: How to Be Ahead of Stock Changes</a></h5>
                                            <div class="post_info">
                                                <span class="post_info_item"><a class="post_info_date" href="#">January 4, 2016</a></span> 
                                            </div>
                                            <div class="post_descr">
                                                <p>Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque...</p><a class="post_readmore readmore" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sc_blogger_button sc_item_button">
                                <a class="sc_button sc_button_style_border sc_button_size_medium" href="#">View More Posts</a>
                            </div>
                        </div>
                        <div class="sc_empty_space" data-height="2.8em"></div>
                    </div>
                </div>
</div>

<!-- </.page_content_wrap> -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 150;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.home.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>