	
	<div class="popup_wrap_bg"></div><a class="scroll_to_top icon-up" href="#" title="Scroll to top"></a> 
	
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/jquery.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/jquery.min.js")); ?>' type='text/javascript'></script> 
    <script type="text/javascript" src="<?php echo e(url('assets/home/js/bootstrap.min.js')); ?>"></script>
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/jquery-migrate.min.js")); ?>' type='text/javascript'></script>
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/essgrid/jquery.themepunch.tools.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/essgrid/jquery.themepunch.essential.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/revslider/jquery.themepunch.revolution.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/revslider/revolution.extension.slideanims.min.js")); ?>' type="text/javascript"></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/revslider/revolution.extension.layeranimation.min.js")); ?>' type="text/javascript"></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/revslider/revolution.extension.navigation.min.js")); ?>' type="text/javascript"></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/instagram/sb-instagram.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/custom.js")); ?>' type="text/javascript"></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/superfish.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_utils.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_init.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_debug.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_googlemap.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/template.init.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/mediaelement/mediaelement-and-player.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_shortcodes.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/custom/_messages.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/magnific-popup/jquery.magnific-popup.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/swiper/swiper.js")); ?>' type='text/javascript'></script>
    <script src='<?php echo e(URL::asset("slider_home/js/vendor/isotope.js")); ?>' type='text/javascript'></script>  
    <script src='<?php echo e(URL::asset("slider_home/js/toastr.min.js")); ?>' type='text/javascript'></script> 
    <script src='<?php echo e(URL::asset("slider_home/js/aos.js")); ?>' type='text/javascript'></script> 
    <!-- <script src='http://maps.google.com/maps/api/js?key=' type='text/javascript'></script> -->
   
    <!-- login -->
    <script>
        
        jQuery("#login_button").click(function(e){
            //e.preventDefault();
             $('#email-error-mess').text('');
             $('#pass-error-mess').text('');

             var csrf_token = $( "#csrf_token" ).val();
             var email = $("#log_email").val();
             var password = $( "#log_password" ).val();
             // console.log(email);
            $.ajax({
               type:'POST',
               url:'/login',
               data:{'_token':csrf_token,'email':email,'password':password},
               success:function(data){
                  // console.log(data);
                    
                    if (data == "admin") {
                        window.location.href = '/admin-dashboard';
                    }else if(data == "user"){
                        window.location.href = '/dashboard';
                    }else{

                    toastr.warning(data);
                    }
               },
               error: function(data){
                    // console.log(data.responseJSON);
                    $('#email-error-mess').text(data.responseJSON.errors.email);
                    $('#pass-error-mess').text(data.responseJSON.errors.password);
                    
               }
            });

        });
    </script>
    <!-- forgot password -->
    <script>
        
        jQuery("#forgot_button").click(function(e){
            //e.preventDefault();
             $('#forg_email_error').text('');

             var csrf_token = $( "#csrf_token" ).val();
             var email = $("#forgot_email").val();
             // console.log(email);
            $.ajax({
               type:'POST',
               url:'/forgot-password',
               data:{'_token':csrf_token,'email':email},
               success:function(data){
                if(data.trim() == 1)
                {
                   toastr.success('Reset code is send to your email');
                }else{
                    // console.log(data);
                    toastr.warning(data); 
                }
               },
               error: function(data){
                    // console.log(data.responseJSON);
                    $('#forg_email_error').text(data.responseJSON.errors.email);
                    
               }
            });

        });
    </script>
    <!--registration  -->
    <script>
        
        jQuery("#register_button").click(function(e){
            //e.preventDefault();
             $('#user_error_mess').text('');
             $('#email_error_mess').text('');
             $('#pass_error_mess').text('');
             $('#confpass_error_mess').text('');
             $('#registration_agree_error_mess').text('');
             
             var csrf_token = $( "#csrf_token" ).val();
             var username = $("#reg_username").val();
             var email = $( "#reg_email" ).val();
             var password = $( "#reg_pwd" ).val();
             var conf_password = $( "#reg_conf_pwd" ).val();
             var referral_code = $( "#referral_code" ).val();
             var is_agree = document.getElementById("registration_agree").checked;
             
             if(is_agree==true){
                registration_agree = $( "#registration_agree" ).val();
            }else{
                registration_agree = '';
            }
             

            $.ajax({
               type:'POST',
               url:'/register',
               data:{'_token':csrf_token,'user_name':username,'email':email,'password':password,'confirm_password':conf_password,'referral_code':referral_code,'registration_agree':registration_agree},
               success:function(data){
                  // console.log(data);
                  setTimeout(function() 
                      {
                        window.location.href = '/home';
                      }, 5000);
                    
                    toastr.success(data);
               },
               error: function(data){
                    console.log(data.responseJSON);
                    console.log(data.responseJSON.errors);
                    $('#user_error_mess').text(data.responseJSON.errors.user_name);
                    $('#email_error_mess').text(data.responseJSON.errors.email);
                    $('#pass_error_mess').text(data.responseJSON.errors.password);
                    $('#confpass_error_mess').text(data.responseJSON.errors.confirm_password);
                    $('#registration_agree_error_mess').text(data.responseJSON.errors.registration_agree);
                    
               }
            });

        });
    </script>

   <script>
      <?php if(Session::has('message')): ?>
        var type = "<?php echo e(Session::get('alert-type', 'info')); ?>";
        switch(type){
            case 'info':
                toastr.info("<?php echo e(Session::get('message')); ?>");
                break;
            
            case 'warning':
                toastr.warning("<?php echo e(Session::get('message')); ?>");
                break;

            case 'success':
                toastr.success("<?php echo e(Session::get('message')); ?>");
                break;

            case 'error':
                toastr.error("<?php echo e(Session::get('message')); ?>");
                break;
        }
      <?php endif; ?>
    </script>

    

    <script type="text/javascript">
      $(".switcher__button").on('click', function(e) { 
      $(".switcher__button").toggleClass('switcher__button--enabled');
    // $(".pricing__value").removeClass('pricing__value--hidden');
    // $(".pricing__value").toggleClass('pricing__value--show pricing__value--hide');  
    if($('.monthly').hasClass('switcher__button--enabled'))
    {
      $('#hourly').removeClass('show');
      $('#hourly').addClass('hidden');
      $('#monthly').removeClass('hidden');
      $('#monthly').addClass('show');
    }
    if($('.yearly').hasClass('switcher__button--enabled'))
    {
      $('#monthly').removeClass('show');
      $('#monthly').addClass('hidden');
      $('#hourly').removeClass('hidden');
      $('#hourly').addClass('show');
    }
  });
  

      $('.modal__overlay--toggle').on('click', function(e) {
        e.preventDefault();
        $('.modal').removeClass('modal--visible');
        $('.modal__content').removeClass('modal__content--visible');
      });

  </script>

  <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5c528534ab5284048d0fada2/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

    <script>
  AOS.init();
</script>
