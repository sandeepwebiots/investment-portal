<?php $__env->startSection('title'); ?> Commission History | Invetex <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style type="text/css">
        #position-error{
            color: red;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">History</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Commission History</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Commission History
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable "
            id="commission-hist-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Commission Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    <?php ($i=1); ?>
                    <?php $__currentLoopData = $pv_bonus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bonus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($i++); ?></td>
                        <td><?php echo e($bonus->amount); ?></td>
                        <td><?php echo e($bonus->created_at->format('d-m-Y')); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        PV Points
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable " id="pv_points-hist-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Referred</th>
                        <th>Amount</th>
                        <th>Commission Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    <?php ($i=1); ?>
                    <?php $__currentLoopData = $pv_points; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $points): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($i++); ?></td>
                        <td><?php echo e($points->givepoints->user_name); ?></td>
                        <td><?php echo e($points->points); ?></td>
                        <td><?php echo e($points->created_at->format('d-m-Y')); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Final Bonus
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">
            <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" id="final-hist-table">

                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Commission Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    <?php ($i=1); ?>
                    <?php $__currentLoopData = $final_bonus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bonus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($i++); ?></td>
                        <td><?php echo e($bonus->amount); ?></td>
                        <td><?php echo e($bonus->created_at->format('d-m-Y')); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#commission-hist-table').DataTable();
    } );

    $(document).ready(function() {
        $('#final-hist-table').DataTable();
    } );

    $(document).ready(function() {
        $('#pv_points-hist-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>