 

<?php $__env->startSection('title'); ?> Our Products | Investment <?php $__env->stopSection(); ?> 

<?php $__env->startSection('style'); ?>
<style>
    .complete{
        display:none;
    }
    .checked {
      color: orange;
    } 
    .more{
        background:#fafafa;
        color:#db9f25;
        font-size:13px;
        padding:3px;
        cursor:pointer;
    }
    .sub-text img{
      height: 100%;
      width: 100%;
      margin-bottom: 30px;
      border: 10px solid #eee;
      border-radius: 50%;
    }
    .production-section-row{
    margin: 0px!important;
}
.production-section-row .col-md-3{
  margin-bottom: 15px;
}
.production-section-row img{
  border: 10px solid #eee;
  border-radius: 50%;
}
</style>
<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
<section class="top_panel_image top_panel_image_1">
  <div class="top_panel_image_hover"></div>
  <div class="top_panel_image_header">
    <h1 class="top_panel_image_title">Our Products</h1>
    <div class="breadcrumbs"> <a class="breadcrumbs_item home" href="<?php echo e(url('/')); ?>">Home</a>  <span class="breadcrumbs_delimiter"></span>
      <span class="breadcrumbs_item current">Our Products</span>
    </div>
  </div>
</section>
<div class="content_wrap set-content-wrap">
  <div class="sc_empty_space" data-height="2em" style="height: 2em;"></div>
  <div class="sc_services_wrap ">
    <div class="sc_services sc_services_style_services-4 sc_services_type_icons_img margin_top_huge margin_bottom_huge ">
      <div class="card-header">
        <h2 class="sc_services_title sc_item_title aos-init aos-animate m-b-0" data-aos="fade-left" data-aos-duration="3000">Our Plans</h2>
        <div class="sc_services_descr sc_item_descr aos-init aos-animate" data-aos="fade-right" data-aos-duration="3000"></div>
      </div>
      <div class="card height-equal">
        <div class="card-body">
          <form class="mega-vertical">
            <div class="row row-set package">
             <!--  <div class="col-sm-12">
                <h5 class="mega-title m-b-5">Our Plans</h5>
              </div> -->
              <?php $__currentLoopData = $package; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pack): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="col-sm-6">
                <div class="card">
                  <div class="media p-20">
                    <div class="media-body">
                      <h6 class="mt-0 mega-title-badge"><?php echo e($pack->title); ?><span class="badge badge-primary pull-right digits">$ <?php echo e($pack->min); ?> - $ <?php echo e($pack->max); ?></span></h6>
                        <?php if($pack->duration == '35'): ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                        <?php elseif($pack->duration == '40'): ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                        <?php elseif($pack->duration == '45'): ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                        <?php elseif($pack->duration == '50'): ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                        <?php endif; ?>
                      <p><?php echo e($pack->profit); ?>% daily ROI for <?php echo e($pack->duration); ?> weeks</p>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
          </form>
        </div>
      </div>
      
      <h2 class="sc_services_title sc_item_title aos-init aos-animate m-b-0" data-aos="fade-left" data-aos-duration="3000">Our Products</h2>
      <div class="sc_services_descr sc_item_descr aos-init aos-animate" data-aos="fade-right" data-aos-duration="3000"></div>
      <div>
        <div class="products-section">
            <div class="row production-section-row" style="text-align: center;">
              <div class="col-md-3  col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/apprale.png')); ?>" alt="">
              </div>
              <div class="col-md-3  col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/beauty.png')); ?>" alt="">
              </div>
              <div class="col-md-3  col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/books.png')); ?>" alt="">
              </div>
              <div class="col-md-3  col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/cctv.png')); ?>" alt="">
              </div>
           
         
          
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/clothes.png')); ?>" alt="">
              </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/electronics.png')); ?>" alt="">
              </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/exercise.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/foods and drinks.png')); ?>" alt="">
              </div>
        
         
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/furniture.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/gadgets.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/grocery.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/home.png')); ?>" alt="">
              </div>
         
       
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/jewwllery.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/kids.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/kitchen.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/mobiles.png')); ?>" alt="">
              </div>
     
   
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/smart devices.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/smart watches.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/textile.png')); ?>" alt="">
                </div>
              <div class="col-md-3 col-xs-6">
                <img src="<?php echo e(URL::asset('assets/home/images/products/toys.png')); ?>" alt="">
              </div>
       
        
              <div class="col-md-3 col-xs-6" style="text-align: center;">
                <img src="<?php echo e(URL::asset('assets/home/images/products/video games.png')); ?>" alt="">
              </div>
            </div>
          </div>
         </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
<script type="text/javascript">
   
     $(".more").toggle(function(){
      $(this).text("Less..").siblings(".complete").show();    
    }, function(){
        $(this).text("More..").siblings(".complete").hide();    
    });


    jQuery(window).scroll(function () {

        var scroll = $(window).scrollTop();

        if (scroll >= 1110) {

            $(".menu").addClass("fixed");
        } else {
            $(".menu").removeClass("fixed");
        }
    });

    $(document).ready(function () {
        $('.tab-content').click(function () {
            $('.tab-content').removeClass("active");
            $(this).addClass("active");
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.home.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>