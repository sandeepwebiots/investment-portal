<?php $__env->startSection('title'); ?> Invested | Invetex <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style type="text/css">
        #position-error{
            color: red;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Invested</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Invested</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Invested Packages
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable "
            id="invested-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Package</th>
                        <th>Duration</th>
                        <th>Payment Type</th>
                        <th>Amount</th>
                        <th>Daily ROI Amount</th>
                        <th>Transaction Id</th>
                        <th>Experience Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody align="center">
                    <?php $i=1; ?>
                    <?php $__currentLoopData = $invested; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($i++); ?></td>
                            <td><?php echo e($invest->package->min); ?>$-<?php echo e($invest->package->max); ?>$</td>
                            <td><?php echo e($invest->package->duration); ?> weeks</td>
                            <td><?php echo e(strtoupper($invest->type)); ?></td>
                            <td><?php echo e($invest->amount); ?> <?php echo e(strtoupper($invest->type)); ?></td>
                            <td><?php echo e($invest->usd_roi_amount); ?> USD</td>
                            <td><?php echo e($invest->txid); ?> <?php if($invest->txid == Null): ?> N/A <?php endif; ?></td>
                            <td><?php echo e($invest->valid_date); ?></td>
                            <td><?php if($invest->final_status == 0): ?><span class="badge badge-warning">Pending</span><?php elseif($invest->final_status == 1): ?><span class="badge badge-success">Complete</span><?php endif; ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#invested-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>