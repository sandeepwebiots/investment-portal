<!DOCTYPE html>
<html class="scheme_original" lang="en-US">
<head>
    <title><?php echo $__env->yieldContent('title'); ?></title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <?php echo $__env->make('layouts.home.head', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->yieldContent('style'); ?>
</head>
<body class="body_style_wide body_filled scheme_original top_panel_show top_panel_over sidebar_hide sidebar_outer_show sidebar_outer_yes top_panel_fixed">
	<div id="page_preloader"></div>
    <div class="body_wrap"> 
        <div class="page_wrap">
        	
        	<?php echo $__env->make('layouts.home.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        	<?php echo $__env->yieldContent('content'); ?>

        	<?php echo $__env->make('layouts.home.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
    </div>

    <?php echo $__env->make('layouts.home.footer_script', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->yieldContent('script'); ?>
</body>
</html>