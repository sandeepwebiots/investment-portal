<?php $__env->startSection('title'); ?> Withdrawal | <?php echo e(strtoupper($coin)); ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title m-subheader__title--separator">Withdraw <?php echo e(strtoupper($coin)); ?></h3>
			<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
				<li class="m-nav__item m-nav__item--home">
					<a href="#" class="m-nav__link m-nav__link--icon">
						<i class="m-nav__link-icon la la-home"></i>
					</a>
				</li>
				<li class="m-nav__separator">-</li>
				<li class="m-nav__item">
					<a href="" class="m-nav__link">
						<span class="m-nav__link-text">Withdraw <?php echo e(strtoupper($coin)); ?></span>
					</a>
				</li>			
			</ul>
		</div>
	</div>
</div>

<div class="m-content">
	<div class="row">
		<div class="col-xl-4">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								<?php if($coin == 'eur'): ?> <?php $coin = 'euro'; ?> <?php endif; ?>
								<?php $balance = $coin.'_balance'; $balance = $user->$balance; ?>
								<?php if($coin == 'euro'): ?> <?php $coin = 'eur'; ?> <?php endif; ?>
								Balance <?php echo e($balance); ?> <?php echo e(strtoupper($coin)); ?>

							</h3>
						</div>
					</div>
				</div>
				<form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(url('withdraw-partner')); ?>/<?php echo e($coin); ?>" method="post">
					<?php echo e(csrf_field()); ?>

					<div class="m-portlet__body">
						<div class="form-group m-form__group m--margin-top-10">
							<div class="alert m-alert m-alert--default" role="alert">
								<code>Please enter your partner correct email.</code>
							</div>
						</div>
						<div class="form-group m-form__group row">
							
                    		<label for="example-text-input" class="col-4 col-form-label">Partner Email</label>
                        		
							<div class="col-8">
								<input class="form-control m-input" type="email" name="email" autocomplete="off">
								<?php if($errors->first('email')): ?>
					              <strong class="text-danger"><?php echo e($errors->first('email')); ?></strong>
					            <?php endif; ?>
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label for="example-search-input" class="col-4 col-form-label">Amount</label>
							<div class="col-8">
								<input class="form-control m-input" type="text" name="amount" id="amount" onkeyup="balanceCheked()" autocomplete="off">
								<?php if($errors->first('amount')): ?>
					              <span class="text-danger"><?php echo e($errors->first('amount')); ?></span>
					            <?php endif; ?>
					            <?php if(session('error')): ?><span class="text-danger"><?php echo e(session('error')); ?></span><?php endif; ?>
							</div>
							<span class="error-balance"></span>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit ">
						<div class="m-form__actions">
							<div class="row">
								<div class="col-12 m--align-right">
									<button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="submit">Withdraw</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-xl-8">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								<?php echo e(strtoupper($coin)); ?> Withdraw Partners
							</h3>
						</div>
					</div>
				</div>


				<div class="m-portlet__body">
					<div class="table-responsive">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="withdraw-table">
						<thead align="center">
							<tr>
								<th scope="col">#</th>
		                      	<?php if($coin == 'btc'): ?>
		                      	<th scope="col">Partner Email</th>
		                      	<?php else: ?>
		                      	<th scope="col">Partner Email</th>
		                      	<?php endif; ?>
			                    <th scope="col">Amount</th>
			                    <th scope="col">Txid</th>
			                    <th scope="col">Status</th>
							</tr>
						</thead>
						<tbody align="center">
							<?php ($i=1); ?>
                        	<?php $__currentLoopData = $withdraw; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $with): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                    		<tr>
	                    			<td><?php echo e($i++); ?></td>
	                    			<td><?php echo e($with->partner_email); ?></td>
	                    			<td><?php echo e($with->amount); ?></td>
	                    			<td><?php echo e($with->txid); ?> <?php if($with->txid == Null): ?> N/A <?php endif; ?></td>
	                    			<td><?php if($with->status == 0): ?><span class="badge badge-warning">Pending</span>
	                    				<?php elseif($with->status == 1): ?><span class="badge badge-success">Complete</span>
	                    				<?php elseif($with->status == 2): ?><span class="badge badge-danger">Reject</span>
	                    				<?php endif; ?>
	                    			</td>
	                    		</tr>
                    		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#withdraw-table').DataTable();
} );
</script>

<script type="text/javascript">
	$('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

	$(document).ready(function() {
		$('#submit').attr('disabled',true);		
	} );

	var coin_blance = (<?php echo e($balance); ?>);
	function balanceCheked(){
	var amount = $('#amount').val();
	console.log(amount);
	if (0 < amount) {
		if (amount > coin_blance) {
			$('#submit').attr('disabled',true);
			$('.error-balance').html('<div class="text-danger"><b>You dont have sufficient balance to Withdraw.</b></div>');
			
		}
		else{
			$('#submit').attr('disabled',false);
			$('.error-balance').html('');
		}
	}		
	}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>