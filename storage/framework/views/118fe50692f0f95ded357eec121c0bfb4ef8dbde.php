<link href="<?php echo e(URL::asset('assets/vendors/custom/datatables/datatables.bundle.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(URL::asset('vendors/perfect-scrollbar/css/perfect-scrollbar.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(URL::asset('vendors/jstree/dist/themes/default/style.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(URL::asset('vendors/vendors/line-awesome/css/line-awesome.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(URL::asset('vendors/vendors/flaticon/css/flaticon.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(URL::asset('vendors/vendors/fontawesome5/css/all.min.css')); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('assets/dashboard/css/fontawesome.css')); ?>">
<link href="<?php echo e(URL::asset('assets/demo/base/style.bundle.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(URL::asset('assets/back/css/dashboard.css')); ?>" rel="stylesheet" type="text/css">
<link href='<?php echo e(URL::asset("slider_home/css/toastr.css")); ?>' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo e(URL::asset('assets/dashboard/images/favicon.png')); ?>" />