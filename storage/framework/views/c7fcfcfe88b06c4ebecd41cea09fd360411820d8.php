<!-- header -->

<header class="top_panel_wrap top_panel_style_3 scheme_original">
    <div class="top_panel_wrap_inner top_panel_inner_style_3 top_panel_position_over">
        <div class="top_panel_top">
            <div class="content_wrap">
                <div class="top_panel_top_contact_area icon-location-light">
                    254 Street Avenue, Los Angeles, LA 2415 US.
                </div>
                <div class="top_panel_top_open_hours icon-clock-light">
                    Mon - Fri : 09:00 - 17:00
                </div>
                <div class="top_panel_top_ophone icon-call-out">
                    8 800 256 35 87
                </div>
                <div class="top_panel_top_user_area cart_show">
                    <ul class="menu_user_nav" id="menu_user">
                        <?php if(Sentinel::check()): ?>
                            <?php
                                $slug = Sentinel::getUser()->roles()->first()->slug;
                            ?>
                            <?php if($slug == 'admin'): ?>
                            <li class="menu_user_register_login">
                                <a class="popup_link popup_login_link" href="<?php echo e(url('admin-dashboard')); ?>">Dashboard</a>
                            </li>
                            <?php elseif($slug == 'user'): ?>
                            <li class="menu_user_register_login">
                                <a class="popup_link popup_login_link" href="<?php echo e(url('dashboard')); ?>">Dashboard</a>
                            </li>
                            <?php endif; ?>
                        <?php else: ?>
                        <li class="menu_user_register_login">
                            <a class="popup_link popup_login_link icon-key-light" href="#popup_login">Login</a> or <a class="popup_link popup_register_link" href="#popup_registration">Register</a>
                        </li>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
        </div>
        <div class="top_panel_middle">
            <div class="content_wrap">
                <div class="contact_logo">
                    <div class="logo">
                        <a href="#"><img alt="" class="logo_main" src="<?php echo e(URL::asset('assets/home/images/logo_dark.png')); ?>"> 
                            <img alt="" class="logo_fixed" src="<?php echo e(URL::asset('assets/home/images/logo.png')); ?>"></a>
                    </div>
                </div>
                <div class="menu_main_wrap">
                    <nav class="menu_main_nav_area menu_hover_fade">
                        <ul class="menu_main_nav" id="menu_main">
                            <li class="menu-item <?php echo e(Request::is('/') ? 'active' : ''); ?>">
                                <a href="<?php echo e(url('/')); ?>">Home</a>
                            </li>
                            <li class="menu-item <?php echo e(Request::is('about-us') ? 'active' : ''); ?>">
                                <a href="<?php echo e(url('about-us')); ?>">About Us</a>
                            </li>
                            <li class="menu-item <?php echo e(Request::is('products') ? 'active' : ''); ?>">
                                <a href="<?php echo e(url('products')); ?>">Our Products</a>
                            </li>
                            <li class="menu-item">
                                <a href="<?php echo e(url('faqs')); ?>"><span>FAQ </span></a>
                            </li>
                            <li class="menu-item">
                                <a href="<?php echo e(url('services')); ?>"><span>Our Services </span></a>
                            </li>
                           <!--  <li class="menu-item">
                                <a href="<?php echo e(url('services')); ?>"><span>Services</span></a>
                            </li> -->
                            <!-- <li class="menu-item menu-item-has-children">
                                <a href="#"><span>Blog</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item">
                                        <a href="#"><span>All Posts</span></a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="#"><span>Blog Creative</span></a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="#"><span>Blog Masonry</span></a>
                                    </li>
                                </ul>
                            </li> -->
                            <?php if(Sentinel::check()): ?>
                             <?php
                                $slug = Sentinel::getUser()->roles()->first()->slug;
                            ?>
                            <?php if($slug == 'admin'): ?>
                            <li class="menu-item">
                                <a href="<?php echo e(url('admin-dashboard')); ?>">Dashboard</a>
                            </li>
                            <?php elseif($slug == 'user'): ?>
                            <li class="menu-item">
                                <a href="<?php echo e(url('dashboard')); ?>">Dashboard</a>
                            </li>
                            <?php endif; ?>
                            <li class="menu-item">
                                <a href="<?php echo e(url('logout')); ?>"><span>Logout</span></a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                    <!-- <div class="search_wrap search_style_fullscreen search_state_closed top_panel_el top_panel_icon">
                        <div class="search_form_wrap">
                            <form action="#" class="search_form" method="get">
                                <button class="search_submit icon-search-light" type="submit"></button> <input class="search_field" name="s" placeholder="Search" type="text" value=""> <a class="search_close icon-1460034721_close"></a>
                            </form>
                        </div>
                    </div> -->
                    <div class="top_panel_top_socials top_panel_el">
                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                            <div class="sc_socials_item">
                                <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                            </div>
                            <div class="sc_socials_item">
                                <a class="social_icons social_twitter" href="#" target="_blank"><span class="icon-twitter"></span></a>
                            </div>
                            <div class="sc_socials_item">
                                <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                            </div>
                            <div class="sc_socials_item">
                                <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header end -->
<div class="site-overlay"></div>
<!-- mobile header -->
<div class="header_mobile header_mobile_style_3">
    
    <div class="content_wrap">
        <div class="menu_button icon-menu"></div>
        <div class="logo">
            <a href="#"><img alt="" class="logo_main" src="<?php echo e(URL::asset('assets/home/images/logo_dark.png')); ?>"></a>
        </div>
        <div class="menu_main_cart top_panel_icon">
            <!-- <a class="top_panel_cart_button" href="#"><span class="contact_icon icon-bag"></span> <span class="cart_items">3</span></a> -->
            <!-- <ul class="widget_area sidebar_cart sidebar">
                <li>
                    <div class="widget woocommerce widget_shopping_cart">
                        <div class="hide_cart_widget_if_empty">
                            <div class="widget_shopping_cart_content">
                                <ul class="cart_list product_list_widget">
                                    <li class="mini_cart_item">
                                        <a class="remove" href="#">×</a> <a href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/book7-180x180.jpg')); ?>"> Finance Equations &amp; Answers Jamie C. Martin&nbsp;</a> <span class="quantity">1 × <span class="amount"><span>$</span>69.99</span></span>
                                    </li>
                                    <li class="mini_cart_item">
                                        <a class="remove" href="#">×</a> <a href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/book4-180x180.jpg')); ?>"> The Richest Man in Babylon Elin Hinderbrand&nbsp;</a> <span class="quantity">1 × <span class="amount"><span>$</span>71.99</span></span>
                                    </li>
                                    <li class="mini_cart_item">
                                        <a class="remove" href="#">×</a> <a href="#"><img alt="" src="<?php echo e(URL::asset('assets/home/images/book-180x180.jpg')); ?>"> Financial Strategy Karen Berman&nbsp;</a> <span class="quantity">1 × <span class="amount"><span>$</span>69.99</span></span>
                                    </li>
                                </ul>
                                <p class="total"><strong>Subtotal:</strong> <span class="amount"><span>$</span>211.97</span></p>
                                <p class="buttons"><a class="button" href="#">View Cart</a> <a class="button" href="#">Checkout</a></p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul> -->
        </div>
    </div>
    <div class="side_wrap">
        <div class="close">
            Close
        </div>
        <div class="panel_top">
            <nav class="menu_main_nav_area">
                <ul class="menu_main_nav" id="menu_mobile">
                    <li class="menu-item <?php echo e(Request::is('/') ? 'active' : ''); ?>">
                        <a href="<?php echo e(url('/')); ?>"><span>Home</span></a>
                    </li>
                    <li class="menu-item <?php echo e(Request::is('about-us') ? 'active' : ''); ?>">
                        <a href="<?php echo e(url('about-us')); ?>"><span>About Us</span></a>
                    </li>
                    <li class="menu-item <?php echo e(Request::is('products') ? 'active' : ''); ?>">
                        <a href="<?php echo e(url('products')); ?>"><span>Our Products</span></a>
                    </li>
                    <li class="menu-item <?php echo e(Request::is('faq') ? 'active' : ''); ?>">
                        <a href="<?php echo e(url('faq')); ?>"><span>FAQ </span></a>
                    </li>
                    <li class="menu-item">
                        <a href="<?php echo e(url('services')); ?>"><span>Our Services </span></a>
                    </li>
                    <?php if(Sentinel::check()): ?>
                        <li class="menu-item">
                            <a class="icon-user" href="<?php echo e(url('logout')); ?>"><span>Logout</span></a>
                        </li>
                    <?php endif; ?>
                    
                </ul>
            </nav>
           <!--  <div class="search_wrap search_state_fixed search_ajax">
                <div class="search_form_wrap">
                    <form action="#" class="search_form" method="get">
                        <button class="search_submit icon-search-light" type="submit"></button> <input class="search_field" name="s" placeholder="Search" type="text" value="">
                    </form>
                </div>
                <div class="search_results widget_area scheme_original">
                    <a class="search_results_close icon-cancel"></a>
                    <div class="search_results_content"></div>
                </div>
            </div> -->
            <?php if(Sentinel::check()): ?>
                <?php
                    $slug = Sentinel::getUser()->roles()->first()->slug;
                ?>
                <?php if($slug == 'admin'): ?>
                <div class="login">
                    <a class="popup_link popup_login_link icon-user" href="<?php echo e(url('admin-dashboard')); ?>">Dashboard</a>
                </div>
                <?php elseif($slug == 'user'): ?>
                <div class="login">
                    <a class="popup_link popup_login_link icon-user" href="<?php echo e(url('dashboard')); ?>">Dashboard</a>
                </div>
                <?php endif; ?>
            <?php else: ?>
            <div class="login">
                <a class="popup_link popup_login_link icon-user" href="#popup_login">Login</a>
            </div>
            <div class="login">
                <a class="popup_link popup_register_link icon-pencil" href="#popup_registration">Register</a>
            </div>
            <?php endif; ?>
        </div>
        <div class="panel_middle">
            <div class="contact_field contact_phone">
                <span class="contact_icon icon-call-out"></span> <span class="contact_label contact_phone">8 800 256 35 87</span>
            </div>
            <div class="top_panel_top_open_hours icon-clock-light">
                Mon - Fri : 09:00 - 17:00
            </div>
            <div class="top_panel_top_user_area cart_hide">
                <div class="menu_pushy_wrap">
                    <a class="menu_pushy_button icon-1460034782_menu2" href="#"></a>
                </div>
            </div>
        </div>
        <div class="panel_bottom">
            <div class="contact_socials">
                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_small">
                    <div class="sc_socials_item">
                        <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                    </div>
                    <div class="sc_socials_item">
                        <a class="social_icons social_twitter" href="#" target="_blank"><span class="icon-twitter"></span></a>
                    </div>
                    <div class="sc_socials_item">
                        <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                    </div>
                    <div class="sc_socials_item">
                        <a class="social_icons social_linkedin" href="#" target="_blank"><span class="icon-linkedin"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mask"></div>
</div>
<!-- mobile header end -->