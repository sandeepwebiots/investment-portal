
<footer class="footer_wrap ">
    <div class="container">
    <div class="row">
        <div class="col-12 col-sm-4">
            <div class="logo">
            <a href="#"><img alt="" class="logo_main" src="<?php echo e(URL::asset('assets/home/images/logo-footer.png')); ?>"></a>
            </div>
            <div class="logo_descr">
               <ul class="sc_list sc_list_style_iconed custom_cl_1">
                 <li class="sc_list_item"><span class="sc_list_icon icon-location-light custom_cl_2"></span>Los Angeles, Street Avenue, La 2540</li>
                 <li class="sc_list_item"><span class="sc_list_icon icon-mobile-light custom_cl_2"></span>Phone: 800-123-4567</li>
                 <li class="sc_list_item"><span class="sc_list_icon icon-mail-light custom_cl_2"></span>Email: invetex@yoursite.com</li>
               </ul>
            </div>
        </div>

        <div class="col-12 col-sm-4">
            <div class="footer_links">
                <h5 class="widget_title">Quick Links</h5>
                <ul class="links">

                    <li><a href="<?php echo e(url('/')); ?>"> Home</a></li>
                    <li><a href="<?php echo e(url('about-us')); ?>"> Ablout Us</a></li>
                    <li><a href="<?php echo e(url('products')); ?>"> Product</a></li>
                    <li><a href="<?php echo e(url('faqs')); ?>"> Faq</a></li>
                </ul>
            </div>
        </div>

        <div class="col-12 col-sm-4">

            <div class="input-wrap">
            <h5 class="widget_title">Contact Form</h5>
            <div class="textwidget">
            <div class="sc_form_wrap">
            <div class="sc_form sc_form_style_form_1" id="sc_form_1">
            <form action="#" class="sc_input_hover_default" data-formtype="form_1" method="post">
            <div class="sc_form_info">
            <div class="sc_form_item sc_form_field">
            <input aria-required="true" class="sc_form_email" name="email" placeholder="E-mail *" type="text">
            </div>
            </div>
            <div class="sc_form_item sc_form_button">
            <button class="sc_button sc_button_style_filled sc_button_size_medium sc_button_iconed icon-mail-light ">Subscribe</button>
            </div>
            <div class="result sc_infobox"></div>
            </form>
            </div>
            </div>
            </div>
            </div>

        </div>

    </div>

    </div>





    
        
            
                
                    
                        
                            
                        
                        
                            
                                
                                
                                
                            
                        
                    
                
                
                    
                    

                    
                    
                    
                        
                    
                
                
                    
                    
                        
                            
                                
                                    
                                        
                                            
                                        
                                    
                                    
                                        
                                    
                                    
                                
                            
                        
                    
                
            
        
    
</footer><!-- /.footer_wrap -->

<footer class="footer_area_wrap scheme_dark">
    <div class="footer_wrap_inner">
        <div class="content_wrap">
            
            <div class="follow_us">
                Follow Us on @Social  Media
            </div>
            <!-- <div class="sbi sbi_disable_mobile sbi_col_9" data-cols="9" data-id="3273597493" data-num="9" data-options='{&quot;sortby&quot;: &quot;none&quot;, &quot;headercolor&quot;: &quot;&quot;, &quot;imagepadding&quot;: &quot;0&quot;}' data-res="thumb" id="sb_instagram">
                <div id="sbi_images">
                    <div class="sbi_loader"></div>
                </div>
                <div id="sbi_load"></div>
            </div> -->
        </div>
    </div>
</footer><!-- /.footer_area_wrap -->
<div class="copyright_wrap copyright_style_soc scheme_dark">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
            <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                <div class="sc_socials_item">
                    <a class="social_icons social_twitter" href="https://twitter.com/amzapay" target="_blank"><span class="icon-twitter"></span></a>
                </div>
                <div class="sc_socials_item">
                    <a class="social_icons social_gplus" href="https://plus.google.com/117078963976314258814" target="_blank"><span class="icon-gplus"></span></a>
                </div>
                <div class="sc_socials_item">
                    <a class="social_icons pinterest" href="https://www.pinterest.com/amzapayc/" target="_blank"><i class="fa fa-pinterest" aria-hidden="true" style="font-size: 20px;"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Registration -->
    <div class="popup_wrap popup_registration bg_tint_light" id="popup_registration">
        <a class="popup_close" href="#"></a>
        <div class="form_wrap">
            <div class="form_left">
                <div class="logo">
                    <a href="index-2.html"><img alt="" class="logo_login" src="<?php echo e(URL::asset('assets/home/images/logo_dark.png')); ?>"></a>
                </div>
                <div class="registration_socials_title">
                    Cretae Your New Account
                </div>
                <!-- <div class="registration_socials_list">
                    <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                        <div class="sc_socials_item">
                            <a class="social_icons social_facebook" href="#" target="_blank"><span class="icon-facebook"></span></a>
                        </div>
                        <div class="sc_socials_item">
                            <a class="social_icons social_twitter" href="#" target="_blank"><span class="icon-twitter"></span></a>
                        </div>
                        <div class="sc_socials_item">
                            <a class="social_icons social_gplus" href="#" target="_blank"><span class="icon-gplus"></span></a>
                        </div>
                    </div>
                </div> -->
                <div class="registration_socials_or">
                    <span>***</span>
                </div> 
            </div>
            <!-- <form class="popup_form"  method="post" action="<?php echo e(url('/register')); ?>"> -->
                    <!-- <?php echo e(csrf_field()); ?> -->
            <form class="popup_form" id="registration_form">

                    <?php if(isset($_GET['referral_code'])): ?>
                        <input type="hidden" id="referral_code" name="referral_code" value="<?php echo $_GET['referral_code']; ?>">
                    <?php endif; ?>  
                <div class="form_right">
                    <div class="popup_form_field login_field iconed_field">
                        <input id="reg_username" name="user_name" placeholder="User name" type="text">
                    </div>
                    <span class="text text-danger" id="user_error_mess"></span>
                    
                    <div class="popup_form_field email_field iconed_field">
                        <input id="reg_email" name="email" placeholder="E-mail" type="email">
                    </div>
                    <span class="text text-danger" id="email_error_mess"></span>
                    
                    <div class="popup_form_field password_field iconed_field">
                        <input id="reg_pwd" name="password" placeholder="Password" type="password">
                    </div>
                    <span class="text text-danger" id="pass_error_mess"></span>
                    
                    <div class="popup_form_field password_field iconed_field">
                        <input id="reg_conf_pwd" name="confirm_password" placeholder="Confirm Password" type="password">
                    </div>
                    <span class="text text-danger" id="confpass_error_mess"></span>
                    
                    <div class="popup_form_field agree_field">
                        <input id="registration_agree" name="registration_agree" value="1" type="checkbox" checked=""> <label for="registration_agree">I agree with</label> <a href="#">Terms &amp; Conditions</a>
                    <span class="text text-danger" id="registration_agree_error_mess"></span>
                        
                    </div>
                    <div class="popup_form_field submit_field">
                        <button class="submit_button" id="register_button" type="button" value="Registration">Registration</button>
                        <!-- <button class="submit_button" id="register_button" type="submit" value="Registration">Registration</button> -->
                    </div>
                </div>
            </form>
            <div class="result message_block"></div>
        </div><!-- /.registration_wrap -->
    </div><!-- /.user-popUp -->
    <!-- end registration -->
    <!-- login -->
    <div class="popup_wrap popup_login bg_tint_light" id="popup_login">
        <a class="popup_close" href="#"></a>
        <div class="form_wrap">
            <div class="form_right">
                <div class="logo">
                    <a href="#"><img alt="" class="logo_login" src="<?php echo e(URL::asset('assets/home/images/logo_dark.png')); ?>"></a>
                </div>
                <div class="login_socials_title">
                    Login Here..
                </div>
                <div class="login_socials_or">
                    <span>***</span>
                </div>
                <div class="result message_block"></div>
            </div>
            <div class="form_left">
               <!--  <form action="<?php echo e(url('login')); ?>" class="popup_form" id="login_form" method="post">
                    <?php echo e(csrf_field()); ?> -->
                <form class="popup_form" id="login_form">

                    <input type="hidden" id="csrf_token" name="csrf_token" value="<?php echo e(csrf_token()); ?>">
                    <div class="popup_form_field login_field iconed_field">
                        <input id="log_email" name="email" placeholder="Email" type="email">
                    </div>
                    <span class="text text-danger" id="email-error-mess"></span>
                    <div class="popup_form_field password_field iconed_field">
                        <input id="log_password" name="password" placeholder="Password" type="password">
                    </div>
                    <span class="text text-danger" id="pass-error-mess"></span>
                    <div class="popup_form_field remember_field">
                        <a class="forgot_password popup_link popup_register_link" href="#popup_forgot">Forgot password?</a> <input id="rememberme" name="rememberme" type="checkbox" value="forever"> <label for="rememberme">Remember me</label>
                    </div>
                    <div class="popup_form_field submit_field">
                        <input class="submit_button" id="login_button" type="button" value="Login">
                    </div>
                </form>
            </div>
        </div><!-- /.login_wrap -->
    </div><!-- /.popup_login -->
    <!-- login -->

    <!-- forgot password -->
    <div class="popup_wrap popup_registration bg_tint_light popup_forgot" id="popup_forgot" style="z-index: 10000;">
        <a class="popup_close" href="#"></a>
        <div class="form_wrap">
            <div class="form_left">
                <div class="logo">
                    <a href="#"><img alt="" class="logo_login" src="<?php echo e(URL::asset('assets/home/images/logo_dark.png')); ?>"></a>
                </div>
                <div class="registration_socials_title">
                    Forgot Your Password
                </div>
                <div class="registration_socials_or">
                    <span>***</span>
                </div> 
            </div>
            <!-- <form class="popup_form" id="forgot_form" method="post" action="<?php echo e(url('forgot-password')); ?>"> -->
            <form class="popup_form" id="forgot_form">
                <div class="form_right">
                    <div class="popup_form_field email_field iconed_field">
                        <input id="forgot_email" name="email" placeholder="E-mail" type="email">
                    </div>
                    <span class="text text-danger" id="forg_email_error"></span>
                    <div class="popup_form_field submit_field">
                        <button class="submit_button" id="forgot_button" type="button">Forgot Password</button>
                    </div>
                </div>
            </form>
            <div class="result message_block"></div>
        </div><!-- /.registration_wrap -->
    </div>