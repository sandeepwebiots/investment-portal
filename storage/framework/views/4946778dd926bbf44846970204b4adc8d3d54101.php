<?php $__env->startSection('title'); ?> Deposit <?php echo e(strtoupper($coin)); ?> | Skrill <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Deposit <?php echo e(strtoupper($coin)); ?></h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit via Skrill </span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-xl-4">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              Deposit with Skrill
                            </h3>
                        </div>
                    </div>
                </div>
                <form class="m-form m-form--fit m-form--label-align-right skrill-form" action="https://pay.skrill.com" method="GET">
                   <!--  <?php echo e(csrf_field()); ?> -->
                    <div class="m-portlet__body">
                        <!-- <div class="form-group m-form__group m--margin-top-10">
                            <div class="alert m-alert m-alert--default" role="alert">
                                <code>Please enter your partner correct email.</code>
                            </div>
                        </div> -->
                        <div class="form-group m-form__group row">
                            
                            <label for="example-text-input" class="col-4 col-form-label">Deposit Amount</label>
                                
                            <div class="col-8">
                                <input class="form-control m-input" type="text" name="amount" id="amount" placeholder="Deposit Amount" autocomplete="off" maxlength="10">
                                <?php if($errors->has('amount')): ?><strong class="text-danger"><?php echo e($errors->first('amount')); ?></strong><?php endif; ?>
                            </div>
                        </div>
                        <input type="hidden" name="pay_to_email" value="<?php echo e($setting->skrill_email_account); ?>">
                       <!--  <input type="hidden" name="currency" value="<?php echo e(strtoupper($coin)); ?>">
                        <input type="hidden" name="return_url" value="<?php echo e(url('/deposit-skrill-return')); ?>">
                        <input type="hidden" name="return_url_target" value="4"> -->

                        <!-- <input name="pay_to_email" type="hidden" value="myemail@hotmail.com"> -->
                        <input name="recipient_description" type="hidden" value="example">
                        <input name="transaction_id" type="hidden" value="userid_datetime1">
                        <input name="return_url" type="hidden" value="http://www.example.com/PostNewPM.aspx?To=MyName">
                        <input name="return_url_text" type="hidden" value="Pm MyName">
                        <input name="return_url_target" type="hidden" value="1">
                        <input name="cancel_url" type="hidden" value="http://www.example.com/Donate.aspx">
                        <input name="cancel_url_target" type="hidden" value="1">
                        <input name="status_url" type="hidden" value="example@gmail.com">
                        <input name="dynamic_descriptor" type="hidden" value="example">
                        <input name="logo_url" type="hidden" value="https://www.example.com/logo_skrill.png">
                        <input name="language" type="hidden" value="EN">
                       <!--  <input name="confirmation_note" type="hidden" value="Do not forget to PM back MyName"> <span class="HowMuchDonate">How Much You Want To Donate (USD $):</span>
                        <input class="HowMuchDonate" maxlength="30" name="amount" style="width: 100px; text-align: center;" type="text"> -->
                        <input name="currency" type="hidden" value="USD">
                        <input name="detail1_description" type="hidden" value="Donate example">
                        <input name="detail1_text" type="hidden" value="Do not forget to PM for rewards">
                        <input name="detail2_description" type="hidden" value="The player name to PM">
                        <input name="detail2_text" type="hidden" value="MyName">
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit ">
                        <div class="m-form__actions">
                            <div class="row">
                                <div class="col-12 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="submit">Deposit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <form action="https://pay.skrill.com" method="post" target="_blank">
                     <input type="hidden" name="pay_to_email" value="<?php echo e($setting->skrill_email_account); ?>">
                     <input type="hidden" name="transaction_id" value="A10005">
                     <input type="hidden" name="return_url" value="https://example.com/
                    payment_finished.html">
                     <input type="hidden" name="cancel_url" value="https://example.com/
                    payment_cancelled.html">
                     <input type="hidden" name="status_url" value="https://example.com/
                    process_payment.cgi">
                     <input type="hidden" name="language" value="EN">
                     <input type="hidden" name="merchant_fields" value="customer_number,session_id">
                     <input type="hidden" name="customer_number" value="C1234">
                     <input type="hidden" name="session_ID" value="A3DFA2234">
                     <input type="hidden" name="pay_from_email" value="payer123@skrill.com">
                     <input type="hidden" name="amount2_description" value="Product Price:">
                     <input type="hidden" name="amount2" value="29.90">
                     <input type="hidden" name="amount3_description" value="Handling Fees & Charges:">
                     <input type="hidden" name="amount3" value="3.10">
                     <input type="hidden" name="amount4_description" value="VAT (20%):">
                     <input type="hidden" name="amount4" value="6.60">
                     <input type="hidden" name="amount" value="39.60">
                     <input type="hidden" name="currency" value="GBP">
                     <input type="hidden" name="firstname" value="John">
                     <input type="hidden" name="lastname" value="Payer">
                     <input type="hidden" name="address" value="Payerstreet">
                     <input type="hidden" name="postal_code" value="EC45MQ">
                     <input type="hidden" name="city" value="Payertown">
                     <input type="hidden" name="country" value="GBR">
                     <input type="hidden" name="detail1_description" value="Product ID:">
                     <input type="hidden" name="detail1_text" value="4509334">
                     <input type="hidden" name="detail2_description" value="Description:">
                     <input type="hidden" name="detail2_text" value="Romeo and Juliet (W.
                    Shakespeare)">
                     <input type="hidden" name="detail3_description" value="Special Conditions:">
                     <input type="hidden" name="detail3_text" value="5-6 days for delivery">
                     
                    </form>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <?php echo e(strtoupper($coin)); ?> Deposit
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                  <div class="table-responsive">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="deposit-table">
                        <thead align="center">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Amount</th>            
                                <th scope="col">Payment Batch Num</th>            
                                <th scope="col">Transaction Id</th>            
                                <th scope="col">Payment Status</th>
                                <th scope="col">Payment Date</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            <?php $i=1; ?>
                            <?php $__currentLoopData = $deposit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $depo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e($depo->amount); ?></td>
                                    <td><?php echo e($depo->payer_account); ?> <?php if($depo->payer_account == Null): ?> N/A <?php endif; ?></td>
                                    <td><?php echo e($depo->txid); ?> <?php if($depo->txid == Null): ?> N/A <?php endif; ?></td>
                                    <td><?php if($depo->status == 0): ?><span class="badge badge-warning">Pending</span>
                                        <?php elseif($depo->status == 1): ?><span class="badge badge-success">Complete</span>
                                        <?php elseif($depo->status == 2): ?><span class="badge badge-danger">Cancelled</span>
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e($depo->created_at->format('d M Y')); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit-table').DataTable();
} );
</script>
<script>
    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {

    $('.skrill-form').validate({ // initialize the plugin
        rules: {
            amount: {
                required: true,
            }
        }
    });

});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>