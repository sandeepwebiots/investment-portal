<?php $__env->startSection('title'); ?> Dashboard | Invetex <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<link href="<?php echo e(URL::asset('vendors/chartist/dist/chartist.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
   <div class="d-flex align-items-center">
      <div class="mr-auto">
         <h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>
         <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
               <a href="<?php echo e(url('/admin-dashboard')); ?>" class="m-nav__link m-nav__link--icon">
               <i class="m-nav__link-icon la la-home"></i>
               </a>
            </li>
            <li class="m-nav__separator">-</li>
            <li class="m-nav__item">
               <a href="<?php echo e(url('/admin-dashboard')); ?>" class="m-nav__link">
               <span class="m-nav__link-text">Dashboard</span>
               </a>
            </li>
         </ul>
      </div>
   </div>
</div>
<div class="m-content">
   <div class="m-portlet">
      <div class="m-portlet__body  m-portlet__body--no-padding">
         <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-xl-4">
               <!--begin:: Widgets/Stats2-1 -->
               <div class="m-widget1">
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Available Balance</h3>
                        </div>
                        <div class="col m--align-right">
                           <h3 class="m-widget1__title"><button data-toggle="modal" data-target="#txidform" class="btn btn-skrill">View Balance</button></h3>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Today's profit</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-danger">$ <?php echo e($daily_roi); ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total Return of investment</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-success">$ <?php echo e(Sentinel::getUser()->roi_amount); ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end:: Widgets/Stats2-1 -->
            </div>
            <div class="col-xl-4">
               <!--begin:: Widgets/Revenue Change-->
               <div class="m-widget14">
                  <div class="m-widget14__header">
                     <h3 class="m-widget14__title">
                        Revenue Change
                     </h3>
                     <span class="m-widget14__desc">
                     Revenue change breakdown by cities
                     </span>
                  </div>
                  <div class="row  align-items-center">
                     <div class="col">
                        <div id="m_chart_revenue_change" class="m-widget14__chart1" style="height: 180px">
                        </div>
                     </div>
                     <div class="col">
                        <div class="m-widget14__legends">
                           <div class="m-widget14__legend">
                              <span class="m-widget14__legend-bullet m--bg-accent"></span>
                              <span class="m-widget14__legend-text">+10% New York</span>
                           </div>
                           <div class="m-widget14__legend">
                              <span class="m-widget14__legend-bullet m--bg-warning"></span>
                              <span class="m-widget14__legend-text">-7% London</span>
                           </div>
                           <div class="m-widget14__legend">
                              <span class="m-widget14__legend-bullet m--bg-brand"></span>
                              <span class="m-widget14__legend-text">+20% California</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end:: Widgets/Revenue Change-->
            </div>
            <div class="col-xl-4">
               <!--begin:: Widgets/Stats2-1 -->
               <div class="m-widget1">
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total investment</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-brand">$ <?php echo e($invest); ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total Withdrawal</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-danger">$ <?php echo e(Sentinel::getUser()->roi_amount); ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total earned amount</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-success">$ <?php echo e($pv_bonus); ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end:: Widgets/Stats2-1 -->
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-4">
         <!--begin:: Widgets/Sales Stats-->
         <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Sales Stats
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <!--begin::Widget 6-->
               <div class="m-widget15">
                  <div class="m-widget15__chart" style="height:180px;">
                     <canvas id="m_chart_sales_stats"></canvas>
                  </div>
                  <div class="m-widget15__items">
                     <div class="row">
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              79%
                              </span>
                              <span class="m-widget15__text">
                              Sales Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-danger" role="progressbar" style="width: 79%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              85%
                              </span>
                              <span class="m-widget15__text">
                              Orders Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-warning" role="progressbar" style="width: 85%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              95%
                              </span>
                              <span class="m-widget15__text">
                              Profit Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-success" role="progressbar" style="width: 95%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              98%
                              </span>
                              <span class="m-widget15__text">
                              Member Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-primary" role="progressbar" style="width: 98%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget15__desc">
                     <!-- * lorem ipsum dolor sit amet consectetuer sediat elit -->
                  </div>
               </div>
               <!--end::Widget 6-->
            </div>
         </div>
         <!--end:: Widgets/Sales Stats-->
      </div>
      <div class="col-xl-4">
         <div class="m-portlet">
            <div class="col-12 m-portlet__body pb-0">
               <div class="single-item single-item-1">
                  <div class="single-item-image overlay-effect">
                     <a href="">
                     <img src="assets/app/media/img/blog/blog1.jpg" alt="" class="img-fluid "></a>
                     <div class="courses-hover-info">
                     </div>
                  </div>
                  <div class="single-item-text">
                     <h5>UNIVERSITY</h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <a href="" class="btn btn-custom">Read More</a>
                  </div>
               </div>
            </div>
            <!--end:: Widgets/Inbound Bandwidth-->
            <div class="col-12 m-portlet__body">
               <div class="single-item">
                  <div class="single-item-image overlay-effect">
                     <a href="">
                     <img src="assets/app/media/img/blog/blog2.jpg" alt="" class="img-fluid"></a>
                     <div class="courses-hover-info">
                     </div>
                  </div>
                  <div class="single-item-text">
                     <h5>UNIVERSITY</h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <a href="" class="btn btn-custom">Read More</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xl-4">
         <!--begin:: Widgets/Top Products-->
         <div class="m-portlet m-portlet--full-height m-portlet--fit ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Top Products
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <!--begin::Widget5-->
               <div class="m-widget4 m-widget4--chart-bottom" style="min-height: 520px">
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <span class="m-widget4__title">
                        <b>Products</b>
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__title"><b>Quantity</b></span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="<?php echo e(url('assets/demo/media/img/icon/1.png')); ?>" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Toys & Games
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">32810</span>
                     </span>
                  </div>
                   <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <span class="m-widget4__title">
                        <img src="<?php echo e(url('assets/demo/media/img/icon/6.png')); ?>" alt="" class="img-fluid ">
                        Grocery
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">31982</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <span class="m-widget4__title">
                        <img src="<?php echo e(url('assets/demo/media/img/icon/2.png')); ?>" alt="" class="img-fluid ">
                        Electronics
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">24861</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="<?php echo e(url('assets/demo/media/img/icon/5.png')); ?>" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Books
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">17428</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="assets/demo/media/img/icon/7.png" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Clothing, Shoes & Jewelry
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">13271</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="assets/demo/media/img/icon/8.png" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Sports & Outdoors
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">6607</span>
                     </span>
                  </div>
                  
                  <div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20 height-setting" >
                     <canvas id="m_chart_trends_stats_2"></canvas>
                  </div>
               </div>
               <!--end::Widget 5-->
            </div>
         </div>
         <!--end:: Widgets/Top Products-->
      </div>
   </div>
   <div class="row">
      <div class="col-xl-6 col-lg-12">
         <!--Begin::Portlet-->
         <div class="m-portlet  m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Recent Login
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <div class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">
                  <!--Begin::Timeline 2 -->
                  <div class="m-timeline-2">
                     <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                        
                        <?php $__currentLoopData = $recent_login; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $login): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="m-timeline-2__item m--margin-top-30">
                           <span class="m-timeline-2__item-time"><?php echo e($login->created_at->format('d-m')); ?> </span>
                           <div class="m-timeline-2__item-cricle">
                              <i class="fa fa-genderless m--font-success"></i>
                           </div>
                           <div class="m-timeline-2__item-text m-timeline-2__item-text--bold">
                              Signed From <?php echo e($login->browser); ?> (<?php echo e($login->process); ?>)
                           </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                     </div>
                  </div>
                  <!--End::Timeline 2 -->
               </div>
            </div>
         </div>
         <!--End::Portlet-->
      </div>
      <div class="col-xl-6 col-lg-12">
         <!--Begin::Portlet-->
         <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Recent Investment
                     </h3>
                  </div>
               </div>
               <!-- <div class="m-portlet__head-tools">
                  <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                     <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget2_tab1_content" role="tab">
                        Today
                        </a>
                     </li>
                     <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget2_tab2_content" role="tab">
                        Month
                        </a>
                     </li>
                  </ul>
               </div> -->
            </div>


            <div class="m-portlet__body">
               <div class="table-responsive">

               <table class="table table-striped- table-bordered table-hover table-checkable " id="invested-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Package</th>
                        <th>Duration</th>
                        <th>Payment Type</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody align="center">
                    <?php $i=1; ?>
                    <?php $__currentLoopData = $invested; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($i++); ?></td>
                            <td><?php echo e($invest->package->min); ?>$-<?php echo e($invest->package->max); ?>$</td>
                            <td><?php echo e($invest->package->duration); ?> weeks</td>
                            <td><?php echo e(strtoupper($invest->type)); ?></td>
                            <td><?php echo e($invest->amount); ?> <?php echo e(strtoupper($invest->type)); ?></td>
                            <td><?php if($invest->final_status == 0): ?><span class="badge badge-warning">Pending</span><?php elseif($invest->final_status == 1): ?><span class="badge badge-success">Complete</span><?php endif; ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            </div>
         </div>
         </div>
         <!--End::Portlet-->
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="m-portlet m-portlet--mobile">
           <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                   <div class="m-portlet__head-title">
                       <h3 class="m-portlet__head-text">
                           Deposit History
                       </h3>
                   </div>
               </div>
           </div>
           <div class="m-portlet__body">
            <div class="table-responsive">
               <!--begin: Datatable -->
              <div class="table-responsive">
               <table class="table table-striped- table-bordered table-hover table-checkable " id="depo-hist-table">
                   <thead align="center">
                       <tr>
                           <th>#</th>
                           <th>Payment Type</th>
                           <th>currency</th>
                           <th>Account</th>
                           <th>Address</th>
                           <th>Amount</th>            
                           <th>Payment Batch Num</th>            
                           <th>Payment Status</th>
                           <th>Payment Date</th>
                       </tr>
                   </thead>
                   <tbody align="center">
                       <?php $i=1; ?>
                       <?php $__currentLoopData = $deposit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $depo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <tr>
                               <td><?php echo e($i++); ?></td>
                               <td><?php if($depo->type == 'PM'): ?> Perfect Money <?php elseif($depo->type == 'skrill'): ?> Skrill <?php endif; ?></td>
                               <td><?php echo e($depo->coin); ?></td>
                               <td><?php echo e($depo->payer_account); ?> <?php if($depo->payer_account == Null): ?> N/A <?php endif; ?></td>
                               <td><?php echo e($depo->address); ?> <?php if($depo->address == Null): ?> N/A <?php endif; ?></td>
                               <td><?php echo e($depo->amount); ?></td>
                               <td><?php if($depo->status == 2): ?><span class="text-danger">Cancelled</span> <?php endif; ?> <?php echo e($depo->txid); ?> <?php if($depo->status == 0): ?> N/A <?php endif; ?></td>
                               <td><?php if($depo->status == 0): ?><span class="badge badge-warning">Pending</span>
                                   <?php elseif($depo->status == 1): ?><span class="badge badge-success">Complete</span>
                                   <?php elseif($depo->status == 2): ?><span class="badge badge-danger">Cancelled</span>
                                   <?php endif; ?>
                               </td>
                               <td><?php echo e($depo->created_at->format('d M Y')); ?></td>
                           </tr>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   </tbody>
               </table>
              </div>
            </div>

           </div>
       </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="m-portlet m-portlet--mobile">
           <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                   <div class="m-portlet__head-title">
                       <h3 class="m-portlet__head-text">
                           Withdrawal History
                       </h3>
                   </div>
               </div>
           </div>

           <div class="m-portlet__body">
            <div class="table-responsive">

               <!--begin: Datatable -->
               <table class="table table-striped- table-bordered table-hover table-checkable " id="with-hist-table">
                   <thead align="center">
                       <tr>
                           <th>#</th>
                           <th>Currency</th>
                           <th>Withdrawa Account</th>
                           <th>Partner Email</th>
                           <th>Amount</th>
                           <th>Txid</th>
                           <th>Status</th>
                       </tr>
                   </thead>
                   <tbody align="center">
                       <?php($i=1) ?>
                       <?php $__currentLoopData = $withdraw; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $with): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <tr>
                               <td><?php echo e($i++); ?></td>
                               <td><?php echo e($with->coin); ?></td>
                               <td><?php echo e($with->address); ?> <?php if($with->address == Null): ?> N/A <?php endif; ?></td>
                               <td><?php echo e($with->partner_email); ?> <?php if($with->partner_email == Null): ?> N/A <?php endif; ?></td>
                               <td><?php echo e($with->amount); ?></td>
                               <td><?php echo e($with->txid); ?> <?php if($with->txid == Null): ?> N/A <?php endif; ?></td>
                               <td><?php if($with->status == 0): ?><span class="badge badge-warning">Pending</span>
                                   <?php elseif($with->status == 1): ?><span class="badge badge-success">Complete</span>
                                   <?php elseif($with->status == 2): ?><span class="badge badge-danger">Reject</span>
                                   <?php endif; ?>
                               </td>
                           </tr>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   </tbody>
               </table>
            </div>
           </div>
       </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="m-portlet m-portlet--mobile ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Shipping Details
                     </h3>
                  </div>
               </div>
               
            </div>
            <div class="m-portlet__body table-responsive">
               <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                  <thead>
                     <tr>
                        <th>Order ID</th>
                        <th>Ship name</th>
                        <th>Ship Date</th>
                        <th>Status</th>
                        <th>Type</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>008-3230 – AZ </td>
                        <td>Emma William</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3229 – AZ</td>
                        <td>Mia Ethan</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3228 – AZ</td>
                        <td>Isabella Mason</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3227 – AZ</td>
                        <td>Retail Michael</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3226 – AZ </td>
                        <td>Abigail Noah</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3225 – AZ </td>
                        <td>Madison Jayden</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3224 – AZ </td>
                        <td>Daniel James</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3223 – AZ </td>
                        <td>Grace Joseph</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3222 – AZ </td>
                        <td>Joseph Lucas</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3221 – AZ</td>
                        <td>John Ryan</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3220 – AZ</td>
                        <td>Audrey  Nathan</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3219 – AZ</td>
                        <td>Caleb Wyatt</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Danger</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3218 – AZ</td>
                        <td>Sarah Aaron</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr> 
                     <tr>
                        <td>008-3217 – AZ</td>
                        <td>Nicholas Cameron</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3216 – AZ</td>
                        <td>Taylor Adrian</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3215 – AZ</td>
                        <td>Violet Ayden</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3214 – AZ</td>
                        <td>Sophie Grayson</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3213 – AZ</td>
                        <td>Molly Asher</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3212 – AZ</td>
                        <td>Trinity Alex</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3211 – AZ</td>
                        <td>Brooke Miles</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3210 – AZ</td>
                        <td>Santiago Colin</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3209 – AZ</td>
                        <td>Leonardo Miguel</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3208 – AZ</td>
                        <td>Victor Joel</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3207 – AZ</td>
                        <td>Kyle Patrick</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3206 – AZ</td>
                        <td>Marcus Jude</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3205 – AZ</td>
                        <td>Alejandro Grant</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3204 – AZ</td>
                        <td>Gage Sean</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3203 – AZ</td>
                        <td>Ryleigh Abel</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3202 – AZ</td>
                        <td>Hope Dean</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3201 – AZ</td>
                        <td>Carly Braylon</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3200 – AZ</td>
                        <td>Reed Dawson</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-6">
         <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Recent Registration History
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <!--begin: Datatable -->
               <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="col-sm-12 table-responsive">

                        <table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 1528px;">
                           <thead>
                              <tr role="row">
                                 <th class="dt-right sorting_disabled" rowspan="1" colspan="1" style="width: 30.5px;" aria-label="Record ID">
                                    #
                                 </th>
                                 
                                 <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 113.25px;" aria-label="Country: activate to sort column ascending">User Name</th>
                                 <!-- <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 144.25px;" aria-label="Ship City: activate to sort column ascending">Email</th> -->
                                 <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 179.25px;" aria-label="Ship Address: activate to sort column ascending">Status</th>
                                 <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 59.25px;" aria-label="Status: activate to sort column ascending">Date</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $i =1; ?>
                              <?php $__currentLoopData = $recent_regstr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <tr role="row" class="odd">
                                 <td><?php echo e($i++); ?></td>
                                 <td><?php echo e($recent->user_name); ?></td><!-- 
                                 <td><?php echo e($recent->email); ?></td> -->
                                 <td><?php if($recent->status == 0): ?><span class="badge  badge-warning">Pending</span><?php elseif($recent->status == 1): ?><span class="badge  badge-success">Active</span><?php endif; ?></td>
                                 <td><?php echo e($recent->created_at->format('d-m-Y')); ?></td>
                              </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xl-6">

         <!--begin:: Widgets/Best Sellers-->
         <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Best Sellers
                     </h3>
                  </div>
               </div>
               <div class="m-portlet__head-tools">
                  <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                     <!-- <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget5_tab1_content" role="tab">
                           Last Month
                        </a>
                     </li> -->
                     <!-- <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab2_content" role="tab">
                           last Year
                        </a>
                     </li>
                     <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab3_content" role="tab">
                           All time
                        </a>
                     </li> -->
                  </ul>
               </div>
            </div>
            <div class="m-portlet__body">

               <!--begin::Content-->
               <div class="tab-content">
                  <div class="tab-pane active" id="m_widget5_tab1_content" aria-expanded="true">

                     <!--begin::m-widget5-->
                     <div class="m-widget5">
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__number">
                                    Sellers
                                 </h4>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__sales"><b>Employees</b></span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__sales"><b>Employees Net income</b></span>
                              </div>
                           </div>
                        </div>
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="<?php echo e(URL::asset('assets/app/media/img//products/Amazon.png')); ?>" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    Amazon.com
                                 </h4>
                                 <p>The wideest online store all over the world!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">613,300</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$3.033</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="<?php echo e(URL::asset('assets/app/media/img//products/Alibaba.png')); ?>" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    Alibaba.com
                                 </h4>                                 
                                 <p>Global trade starts here!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">66,421</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$1.412</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="<?php echo e(URL::asset('assets/app/media/img//products/Ebay.png')); ?>" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    Ebay.com
                                 </h4>
                                 <p>The trader of Most-Shopped iteams!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">14,100</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$1.016</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                         <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="<?php echo e(URL::asset('assets/app/media/img//products/walmart.png')); ?>" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    walmart
                                 </h4>
                                 <p>One shop for all!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">2.3 M</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$9.862</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                     </div>

                     <!--end::m-widget5-->
                  </div>
               </div>

               <!--end::Content-->
            </div>
         </div>

         <!--end:: Widgets/Best Sellers-->
      </div>
   </div>
   <div class="modal fade txidform" id="txidform"  role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
          <div class="modal-content">
            <div class="m-portlet m--bg-warning m-portlet--bordered-semi m-portlet--full-height " style="margin-bottom:0 !important">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                           Balance
                        </h3>
                     </div>
                  </div>
               </div>
               <div class="m-portlet__body">
                  <div class="m-widget29">
                     <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Skrill</h3>
                        <div class="m-widget_content-items">
                           <div class="m-widget_content-item">
                              <span>USD</span>
                              <span class="m--font-accent"><?php echo e($skrill_usd); ?></span>
                           </div>
                           <div class="m-widget_content-item">
                              <span>EUR</span>
                              <span class="m--font-brand"><?php echo e($skrill_eur); ?></span>
                           </div>
                           <div class="m-widget_content-item">
                              <span>GBP</span>
                              <span><?php echo e($skrill_gbp); ?></span>
                           </div>
                        </div>
                     </div>
                     <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Perfect Money</h3>
                        <div class="m-widget_content-items">
                           <div class="m-widget_content-item">
                              <span>USD</span>
                              <span class="m--font-accent"><?php echo e($pm_usd); ?></span>
                           </div>
                           <div class="m-widget_content-item">
                              <span>EUR</span>
                              <span class="m--font-brand"><?php echo e($pm_eur); ?></span>
                           </div>
                        </div>
                     </div>
                     <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Coinbase</h3>
                        <div class="m-widget_content-items">
                           <div class="m-widget_content-item">
                              <span>BTC</span>
                              <span class="m--font-accent"><?php echo e($coinbase_btc); ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          </div>
      </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(URL::asset('vendors/raphael/raphael.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('vendors/morris.js/morris.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('vendors/chartist/dist/chartist.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('vendors/chart.js/dist/Chart.bundle.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#depo-hist-table').DataTable();
    });
     $(document).ready(function() {
        $('#with-hist-table').DataTable();
    });
     $(document).ready(function() {
        $('#m_table_1').DataTable();
    });
     $(document).ready(function() {
        $('#invested-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>