<?php $__env->startSection('title'); ?> Package <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<style>
    .error {
        margin: 0px!important;
        color: #ff2b2b!important;
    }
    .error-balance{
        margin-left:12px;
        color: #ff2b2b;
    }
    p {
        font-size: 16px!important;
    }   
    .media-body .badge{
        font-size: 16px;
    }
    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 10px;
        cursor: pointer;
        background: #db9f25;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        background: #00c5dc;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        background: #eeeeee;
        cursor: pointer;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Package</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Package</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>


<div class="m-content">
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-invoice-1">
                <div class="m-invoice__wrapper">
                    <div class="m-invoice__body m-invoice__body--centered">
                        <div class="table-responsive">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                            Investment Package
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr> 
                                        <td>Durations</td>
                                        <td><?php echo e($pack->duration); ?> weeks</td> 
                                    </tr>
                                    <tr> 
                                        <td>Package Limit</td>
                                        <td><?php echo e($pack->min); ?>-<?php echo e($pack->max); ?> USD</td> 
                                    </tr>
                                    <tr> 
                                        <td>Profit</td>
                                        <td><?php echo e($pack->profit); ?> % daily ROI</td> 
                                    </tr>
                                    <tr> 
                                        <td>Minimum Amount</td>
                                        <td><?php echo e($pack->min); ?> USD (Need for Purchase Package <?php echo e($pack->duration); ?> weeks)</td> 
                                    </tr>
                                    <tr> 
                                        <td>Select Amount</td>
                                        <td>
                                           <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(route('package.preview')); ?>" method="post" id="package-amount">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="hidden" name="currency_value" id="currency_value">
                                                <input type="hidden" name="package_id" value="<?php echo e($pack->id); ?>">
                                                <input type="range" name="amount" id="myRange" min="<?php echo e($pack->min); ?>" max="<?php echo e($pack->max); ?>" class="set-full-width slider" onchange="updateTextInput(this.value);">
                                               <span><b>Note:</b> Scroll to change amount.</span><br>
                                                <div class="m-portlet__body">
                                                    <div class="form-group m-form__group row">
                                                        <label for="usd_amount" class="col-2 col-form-label"><b>USD Amount</b></label>
                                                        <div class="col-8 input-group m-input-group">
                                                            <input class="form-control m-input" id="usd_amount" name="usd_amount" type="number" readonly="">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">USD</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label for="roi_amount" class="col-2 col-form-label"><b>USD ROI Amount</b></label>
                                                        <div class="col-8 input-group m-input-group">
                                                            <input class="form-control m-input" id="roi_amount" name="roi_amount" type="number" readonly="">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">USD</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label for="currency" class="col-2 col-form-label"><b>Pay with</b></label>
                                                        <div class="col-8">
                                                            <select name="currency" id="currency" class="btn form-control b-light" onchange="">
                                                                <option value="usd">USD</option>
                                                                <option value="gbp">GBP</option>
                                                                <option value="euro">EURO</option>
                                                                <option value="btc">BTC</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label for="balance" class="col-2 col-form-label"><b>Balance</b></label>
                                                        <div class="col-8 input-group m-input-group">
                                                            <input class="form-control m-input" type="number" name="balance" id="balance" readonly="">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="setCurrency">USD</span>
                                                            </div>
                                                        </div>
                                                        <span class="error-balance"></span>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label for="amount" class="col-2 col-form-label"><b>Amount</b></label>
                                                        <div class="col-8 input-group m-input-group">
                                                            <input class="form-control m-input" type="number" name="amount" id="amount" readonly="">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="setCurrency1">USD</span>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group m-form__group row">
                                                        <label for="amount_roi" class="col-2 col-form-label"><b>ROI Amount</b></label>
                                                        <div class="col-8 input-group m-input-group">
                                                            <input class="form-control m-input" id="amount_roi" name="amount_roi" type="number" readonly="">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="setCurrency2">USD</span>
                                                            </div>
                                                        </div>
                                                    </div>                                               
                                                </div>
                                                <button type="submit" id="next_btn" class="btn m-btn--pill m-btn--air         btn-outline-accent m-btn m-btn--custom m-btn--outline-4x"><i class="fa fa-eye"></i>&nbsp; Preview Payment</button>
                                            </form>
                                        </td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(URL::asset('assets/dashboard/js/validation/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('assets/dashboard/js/validation/additional-methods.min.js')); ?>"></script>
<script>

$('#package-amount').validate({

    rules:{
        usd_amount:{
            required: true,
        },
        roi_amount:{
            required: true,
        },
        currency:{
            required: true,
        }
    }
});


// var usd_btc = 0.00030;  // USD to BTC convert
// var usd_eur = 0.88;  // USD to EUR convert
// var usd_gbp = 0.79;  // USD to GBP convert

function updateTextInput(val) {
  document.getElementById('usd_amount').value=val; 
  var profit = (<?php echo e($pack->profit); ?>);
  var roi = val * profit / 100;
  document.getElementById('roi_amount').value=roi;

  balanceCheck();
  // console.log(roi);  
}



function balanceCheck()
{
    var currency = $('#currency').val();
    var profit = (<?php echo e($pack->profit); ?>);
    var currency_value = $('#currency_value').val();

    // console.log(currency);
       var usd_amount =  $("#usd_amount").val();
    if (currency == 'usd') {
        var balance = "<?php echo e(Sentinel::getUser()->usd_balance); ?>";
        var amount = $("#usd_amount").val();       
        var amount_roi = amount * profit / 100;
    }else if(currency == 'gbp'){
        var balance = "<?php echo e(Sentinel::getUser()->gbp_balance); ?>";
        var amount = usd_amount * currency_value;
        var amount_roi = amount * profit / 100;
    }else if(currency == 'euro'){
        var balance = "<?php echo e(Sentinel::getUser()->euro_balance); ?>";
        var amount = usd_amount * currency_value;
        var amount_roi = amount * profit / 100;
    }else if(currency == 'btc'){
        var balance = "<?php echo e(Sentinel::getUser()->btc_balance); ?>";
        var amount = usd_amount * currency_value;
        var amount_roi = amount * profit / 100;
    }
    $("#amount").val(amount);
    $("#balance").val(balance);
    if (parseInt(balance) > parseInt(amount)) {
        $('#next_btn').prop('disabled',false);
        $('.error-balance').html('');
    }else{
        $('.error-balance').html('Insufficient Balance');
        $('#next_btn').prop('disabled',true);
    }
    var setcurrency = currency.toUpperCase();
    $("#setCurrency,#setCurrency1,#setCurrency2").html(setcurrency);
    document.getElementById('amount_roi').value=amount_roi;
}

    

$("#currency").change(function(){   // 1st

    var currency = $('#currency').val();
   
    $.ajax({    //create an ajax request to display.php
        type: "get",
        url: "balance",             
        dataType: "html",
        data: {
            'currency': currency,
            '_token': '<?php echo e(csrf_token()); ?>',
           //expect html to be returned                
        },
        success: function(response){ 
             // console.log(response);
           var arr = JSON.parse(response);
           // console.log(arr);
           if (arr['GBP']) {
            $('#currency_value').val(arr['GBP']);
           }else if (arr['BTC']) {
            $('#currency_value').val(arr['BTC']);
           }else if (arr['EUR']) {
            $('#currency_value').val(arr['EUR']);
           }
            balanceCheck();
        }
    }); 
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>