<?php $__env->startSection('title'); ?> Invoice <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Invoice</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Invoice</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet">
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div class="m-invoice-2">
                        <div class="m-invoice__wrapper">
                            <div class="m-invoice__head">
                                <div class="m-invoice__container m-invoice__container--centered">
                                    <div class="m-invoice__logo">
                                            <h1>Final Invoice</h1>
                                    </div>
                                    <!-- <span class="m-invoice__desc">
                                        <span>Cecilia Chapman, 711-2880 Nulla St, Mankato</span>
                                        <span>Mississippi 96522</span>
                                    </span> -->
                                    <div class="m-invoice__items">
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">DATA</span>
                                            <span class="m-invoice__text"><?php echo e($purchases->start_date); ?></span>
                                        </div>
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">INVOICE NO.</span>
                                            <span class="m-invoice__text"><?php echo e($purchases->id); ?></span>
                                        </div>
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">Payment Status</span>
                                            <span class="m-invoice__text m--font-warning"><strong>Pending</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-invoice__body m-invoice__body--centered">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>DESCRIPTION</th>
                                                <th>DETAILS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Package</td>
                                                <td><?php echo e($package->min); ?>-<?php echo e($package->max); ?> USD</td>
                                            </tr>
                                            <tr>
                                                <td>Durations</td>
                                                <td><?php echo e($package->duration); ?> weeks</td>
                                            </tr>
                                            <tr>
                                                <td>Profit</td>
                                                <td><?php echo e($package->profit); ?> %</td>
                                            </tr>
                                            <tr>
                                                <td>Start Date</td>
                                                <td><?php echo e($purchases->start_date); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Valid Date</td>
                                                <td><?php echo e($purchases->valid_date); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Status</td>
                                                <td class="m--font-warning">Pending</td>
                                            </tr>
                                            <tr>
                                                <td>Payment Type</td>
                                                <td><strong><?php echo e(strtoupper($purchases->type)); ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td><strong><?php echo e($purchases->amount); ?> <?php echo e(strtoupper($purchases->type)); ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td>Daily Profit Amount ( <?php echo e($package->profit); ?> %)</td>
                                                <td><strong><?php echo e($purchases->usd_roi_amount); ?> USD</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center"><a href="<?php echo e(url('pay')); ?>/<?php echo e($purchases->id); ?>" class="btn m-btn--pill m-btn--air         btn-outline-success m-btn m-btn--custom m-btn--outline-2x">Pay</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>