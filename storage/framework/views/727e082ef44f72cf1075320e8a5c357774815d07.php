<?php
/**
 *
 * @author Charles Patterson <charlesassets@gmail.com>
 *
 */
?>
<form action="https://perfectmoney.is/api/step1.asp" method="POST">
    <input type="hidden" name="PAYEE_ACCOUNT" value="<?php echo e($rander->PAYEE_ACCOUNT); ?>">
    <input type="hidden" name="PAYEE_NAME" value="<?php echo e($PAYEE_NAME); ?>">
    <input type="text" name="PAYMENT_AMOUNT" value="<?php echo e($PAYMENT_AMOUNT); ?>" placeholder="Amount">
    <input type="hidden" name="PAYMENT_UNITS" value="<?php echo e($PAYMENT_UNITS); ?>">
	<input type="hidden" name="PAYMENT_URL" value="<?php echo e($PAYMENT_URL); ?>">
	<input type="hidden" name="NOPAYMENT_URL" value="<?php echo e($NOPAYMENT_URL); ?>">
	<?php if($PAYMENT_ID): ?>
		<input type="hidden" name="PAYMENT_ID" value="<?php echo e($PAYMENT_ID); ?>">
	<?php endif; ?>
	<?php if($STATUS_URL): ?>
		<input type="hidden" name="STATUS_URL" value="<?php echo e($STATUS_URL); ?>">
	<?php endif; ?>
	<?php if($PAYMENT_URL_METHOD): ?>
		<input type="hidden" name="PAYMENT_URL_METHOD" value="<?php echo e($PAYMENT_URL_METHOD); ?>">
	<?php endif; ?>
	<?php if( $NOPAYMENT_URL_METHOD ): ?>
		<input type="hidden" name="NOPAYMENT_URL_METHOD" value="<?php echo e($NOPAYMENT_URL_METHOD); ?>">
	<?php endif; ?>
	
	<?php if( $MEMO ): ?>
		<input type="hidden" name="SUGGESTED_MEMO" value="<?php echo e($MEMO); ?>">
	<?php endif; ?>
    <input type="submit" value="Proceed">
</form>