
    <link href="http://fonts.googleapis.com/css?family=Poppins:300,300italic,400,400italic,500,500italic,600,600italic,700,700italic%7CLora:300,300italic,400,400italic,500,500italic,600,600italic,700,700italic&amp;subset=latin,latin-ext&amp;" media="all" property="stylesheet" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('assets/home/css/bootstrap.css')); ?>">
    <link href='<?php echo e(URL::asset("slider_home/css/fontello/css/fontello.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/instagram/sb-instagram.min.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/essgrid/ess-grid.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/revslider/rev-slider.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/woocommerce/woocommerce.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/style.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/colors.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/aos.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/woocommerce/woocommerce-layout.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/woocommerce/plugin.woocommerce.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/animation.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/shortcodes.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/messages.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/magnific-popup/magnific-popup.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/js/vendor/swiper/swiper.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/responsive.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link href='<?php echo e(URL::asset("slider_home/css/toastr.css")); ?>' media='all' property="stylesheet" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('assets/dashboard/css/fontawesome.css')); ?>">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href='<?php echo e(URL::asset("slider_home/images/favi.png")); ?>' rel="icon" sizes="192x192">