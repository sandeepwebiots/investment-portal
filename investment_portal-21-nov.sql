-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 21, 2018 at 01:21 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `investment_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'MEuxl0Fzu6v7xGDqLmhjV7gMmd5O9GYe', 1, '2018-11-09 18:30:00', '2018-11-10 03:31:43', '2018-11-10 03:31:43'),
(2, 2, 'yzATWLfFKIy9cRKPzg3dJo5T1b02Dx9o', 1, '2018-11-10 03:43:34', '2018-11-10 03:35:57', '2018-11-10 03:43:34'),
(3, 3, 'n2KxB69AhSktsRL1DEO6BzwkE2LuJgSn', 1, '2018-11-10 03:46:14', '2018-11-10 03:46:00', '2018-11-10 03:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `coin` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
CREATE TABLE IF NOT EXISTS `persistences` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'uq5ULcbuZUwcodAaAx8LZWpCblv12v7S', '2018-11-10 05:16:27', '2018-11-10 05:16:27'),
(2, 1, 'kKUU0TtKj2hiDMDltG4JUfupnLM9wwks', '2018-11-10 05:19:28', '2018-11-10 05:19:28'),
(3, 1, 'xW84l3NKCCd5x394fLKDieKlyKsvhWbW', '2018-11-10 05:39:48', '2018-11-10 05:39:48'),
(4, 1, 'PbQJE49wuVHMLwSlYYNkrOgJblicHng6', '2018-11-10 05:40:02', '2018-11-10 05:40:02'),
(5, 1, 'rgY16gWyecNwWx71GtfM97Syy5Hz7Xy3', '2018-11-10 05:40:57', '2018-11-10 05:40:57'),
(6, 1, 'dAXlTZXcpS9orSmQ27NDtEkPdiMtX7sZ', '2018-11-10 05:43:27', '2018-11-10 05:43:27'),
(14, 1, 'qMeIPGGu1YeqIWa5nfD1O2yiAsZKVHp0', '2018-11-10 06:37:24', '2018-11-10 06:37:24'),
(15, 1, 'koVciYetjxuWJfQWwy8WUJZI2FNBePYq', '2018-11-10 06:38:14', '2018-11-10 06:38:14'),
(24, 1, 'UQcUeDsoJexPsUVGEQfORj0WqJa1oSCC', '2018-11-10 07:06:31', '2018-11-10 07:06:31'),
(29, 3, 'BlJ6ApGC6XPcG9uw9NJ39vrGuAM3lZMq', '2018-11-10 08:32:02', '2018-11-10 08:32:02'),
(34, 1, 'mH3EIQUQqUSwJ5eLsNjpE7rPiJ7ZwZam', '2018-11-12 03:23:57', '2018-11-12 03:23:57'),
(35, 1, 'dfSTfFkWyaHz74R1YZdxnh6aRUId1p0I', '2018-11-12 04:01:34', '2018-11-12 04:01:34'),
(41, 3, 'bUD8TxjQ3dvTeldIjQWFe7ZM8SopQ5eA', '2018-11-13 07:37:37', '2018-11-13 07:37:37'),
(44, 3, 'DLmhAKD9fonzR1ha6sSRAufh7V6amCjo', '2018-11-17 07:54:29', '2018-11-17 07:54:29'),
(45, 2, 'Pm4ieRlrihFElDAWPTxoCbDrPq8fWWOm', '2018-11-21 04:18:01', '2018-11-21 04:18:01'),
(46, 2, 'c961xzLWPzTh0Fk1wpKiAoDetNm3Gsqb', '2018-11-21 06:40:56', '2018-11-21 06:40:56');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

DROP TABLE IF EXISTS `referrals`;
CREATE TABLE IF NOT EXISTS `referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `earn_amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `user_id`, `referral_id`, `earn_amount`, `created_at`, `updated_at`) VALUES
(1, 19, 3, NULL, '2018-11-16 03:44:23', '2018-11-16 03:44:23');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reminders`
--

INSERT INTO `reminders` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(2, 3, 'MXBQDmcq', 1, NULL, '2018-11-15 03:42:06', '2018-11-15 03:50:48'),
(3, 3, 'gEuTARGe', 1, NULL, '2018-11-15 03:51:26', '2018-11-15 04:07:05'),
(4, 3, 'g1IYbg4S', 1, NULL, '2018-11-15 04:08:23', '2018-11-15 04:20:49'),
(5, 3, 'u1UBKgH6', 1, '2018-11-15 04:21:51', '2018-11-15 04:21:03', '2018-11-15 04:21:51'),
(6, 3, 'nEcVlLub', 1, '2018-11-15 04:22:44', '2018-11-15 04:22:01', '2018-11-15 04:22:44'),
(7, 3, 'pqBmShUb', 1, '2018-11-15 04:30:38', '2018-11-15 04:29:48', '2018-11-15 04:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, '2018-11-09 18:30:00', '2018-11-09 18:30:00'),
(2, 'user', 'User', NULL, '2018-11-09 18:30:00', '2018-11-09 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-11-10 03:31:44', '2018-11-10 03:31:44'),
(2, 2, '2018-11-10 03:35:57', '2018-11-10 03:35:57'),
(3, 2, '2018-11-10 03:46:00', '2018-11-10 03:46:00');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) DEFAULT NULL,
  `phone_no` int(11) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `daily_profit` float DEFAULT NULL,
  `total_deposit` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=Deactivate, 1=Active, 2=Block, 3=Delete ',
  `btc_balance` float DEFAULT '0',
  `eth_balance` float DEFAULT '0',
  `bch_balance` float DEFAULT '0',
  `usd_balance` float DEFAULT '0',
  `gbp_balance` float DEFAULT '0',
  `euro_balance` float DEFAULT '0',
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0' COMMENT '0=Active 3=Delete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `email`, `password`, `referral_id`, `status`, `btc_balance`, `eth_balance`, `bch_balance`, `usd_balance`, `gbp_balance`, `euro_balance`, `permissions`, `last_login`, `first_name`, `last_name`, `profile`, `referral_code`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$2X9oxrvuzYhf8DKgaEE9feQNGiYFgmnHl3hvT9DOmVrqmEJVJEu3K', NULL, 1, 0, 0, 0, 0, 0, 0, NULL, '2018-11-17 01:31:10', 'Admin', 'Admin', '1542015157.jpg', 'AgmIEfGHPxVCXcKz', 0, '2018-11-10 03:31:43', '2018-11-17 01:31:10'),
(2, 'user', 'user@gmail.com', '$2y$10$1HZ8ndrKqGHDZ0mo7cZhueNbjZ/RotEdiM9fiJJlHbypeU8b/3/Oy', NULL, 1, 0, 0, 0, 0, 0, 0, NULL, '2018-11-21 06:40:56', 'User', 'Test', '1542014961.png', 'AgmIEflHPxVCXcKu', 0, '2018-11-10 03:35:57', '2018-11-21 06:40:56'),
(3, 'test', 'test@gmail.com', '$2y$10$VsrqsLshNjvL5K4bp4saxu/PuPDdj2.71Z5tKiR9nFyxbDoscHoo.', NULL, 1, 0, 0, 0, 0, 0, 0, NULL, '2018-11-17 07:54:29', 'Test', 'Test', NULL, 'BgmIEdhfvgHPxVCXcKu', 0, '2018-11-10 03:46:00', '2018-11-17 07:54:29');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
