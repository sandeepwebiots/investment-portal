<!DOCTYPE html>
<html class="scheme_original" lang="en-US">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <meta content="telephone=no" name="format-detection">
    @include('layouts.home.head')

    @yield('style')
</head>
<body class="body_style_wide body_filled scheme_original top_panel_show top_panel_over sidebar_hide sidebar_outer_show sidebar_outer_yes top_panel_fixed">
	<div id="page_preloader"></div>
    <div class="body_wrap"> 
        <div class="page_wrap">
        	
        	@include('layouts.home.header')

        	@yield('content')

        	@include('layouts.home.footer')

        </div>
    </div>

    @include('layouts.home.footer_script')

    @yield('script')
</body>
</html>