<script src="{{ URL::asset('vendors/jquery/dist/jquery.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<!-- <script src="assets/demo/custom/crud/datatables/advanced/column-visibility.js" type="text/javascript"></script> -->
<script src="{{ URL::asset('vendors/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/demo/base/scripts.bundle.js') }}" type="text/javascript"></script>
<script src='{{ URL::asset("slider_home/js/toastr.min.js") }}' type='text/javascript'></script>
<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

<script>
	// active link
for (var i = 0; i < document.links.length; i++) {
    if (document.links[i].href == document.URL) {
        document.links[i].className += ' active';
        document.links[i].parentElement.parentElement.parentElement.className += ' active';
    }
}
</script>