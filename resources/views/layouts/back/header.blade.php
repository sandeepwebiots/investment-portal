@php
    $slug = Sentinel::getUser()->roles()->first()->slug;
@endphp
<!-- Header -->
<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
	<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop">

			<div class="m-stack__item m-brand ">
				<div class="m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-stack__item--middle m-brand__logo">
						<a href="#" class="m-brand__logo-wrapper">
							<img alt="" src="{{ URL::asset('assets/dashboard/images/favicon.png') }}" />
						</a>
					</div>
					<div class="m-stack__item m-stack__item--middle m-brand__tools">
						<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
							<i class="flaticon-more"></i>
						</a>
					</div>
				</div>
			</div>

			<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

				<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
				<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
					<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
						<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" >
							<div class="row">
								<div class="form-group m-form__group">
									<div class="col-lg-12">
					                    <div class="input-group pill-input-group border-ref">
					                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-copy"></i></span></div>
					                        <input type="text" id="copy-link" class="form-control " name="ref_link" value="{{ url('/').'/?referral_code='.Sentinel::getUser()->referral_code }}" readonly="">
					                    </div>
					                </div>
					            </div>
				            </div>
						</li>
						<!-- <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link"><span
								 class="m-menu__item-here"></span><span class="m-menu__link-text">Reports</span></a>
							
						</li>
						<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link"><span
								 class="m-menu__item-here"></span><span class="m-menu__link-text">Orders</span></a>
						</li> -->
					</ul>
				</div>

				<!-- END: Horizontal Menu -->

				<!-- BEGIN: Topbar -->
				<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-topbar__nav-wrapper">
						<ul class="m-topbar__nav m-nav m-nav--inline">
							<!-- @if($slug=='user')
							<li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
								<a href="#" class="m-nav__link m-dropdown__toggle">
									<span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
									<span class="m-nav__link-icon"><span class="m-nav__link-icon-wrapper"><i class="flaticon-share"></i></span></span>
								</a>
								<div class="m-dropdown__wrapper">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__header m--align-center">
											<span class="m-dropdown__header-title">Quick Actions</span>
											<span class="m-dropdown__header-subtitle">Shortcuts</span>
										</div>
										<div class="m-dropdown__body m-dropdown__body--paddingless">
											<div class="m-dropdown__content">
												<div class="m-scrollable" data-scrollable="false" data-height="380" data-mobile-height="200">
													<div class="m-nav-grid m-nav-grid--skin-light">
														<div class="m-nav-grid__row">
															<a href="#" class="m-nav-grid__item">
																<i class="m-nav-grid__icon flaticon-file"></i>
																<span class="m-nav-grid__text">Deposit History</span>
															</a>
															<a href="#" class="m-nav-grid__item">
																<i class="m-nav-grid__icon flaticon-time"></i>
																<span class="m-nav-grid__text">Add New Event</span>
															</a>
														</div>
														<div class="m-nav-grid__row">
															<a href="#" class="m-nav-grid__item">
																<i class="m-nav-grid__icon flaticon-folder"></i>
																<span class="m-nav-grid__text">Create New Task</span>
															</a>
															<a href="#" class="m-nav-grid__item">
																<i class="m-nav-grid__icon flaticon-clipboard"></i>
																<span class="m-nav-grid__text">Completed Tasks</span>
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
							@endif -->
							
							<li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
								<a href="#" class="m-nav__link m-dropdown__toggle">
									<span class="m-topbar__userpic m--hide">
										<img src="{{ URL::asset('/assets/dashboard/images/45.png') }}" class="m--img-rounded m--marginless m--img-centered" alt="" />
									</span>
									<span class="m-nav__link-icon m-topbar__usericon">
										<span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
									</span>
									<span class="m-topbar__username m--hide">{{Sentinel::getUser()->user_name}}</span>
								</a>
								<div class="m-dropdown__wrapper">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__header m--align-center">
											<div class="m-card-user m-card-user--skin-light">
												<div class="m-card-user__pic">
													@if(Sentinel::getUser()->profile != "")
					                                <img src="{{ url('/assets/profiles/'.Sentinel::getUser()->profile) }}" id="profile" />
					                                @else
					                                <img src="{{ URL::asset('/assets/dashboard/images/45.png') }}" alt=""  id="profile"/>
					                                @endif
												</div>
												<div class="m-card-user__details">
													<span class="m-card-user__name m--font-weight-500">{{Sentinel::getUser()->user_name}}</span>
													<a href="" class="m-card-user__email m--font-weight-300 m-link">{{Sentinel::getUser()->email}}</a>
												</div>
											</div>
										</div>
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav m-nav--skin-light">
													<li class="m-nav__section m--hide">
														<span class="m-nav__section-text">Section</span>
													</li>
														@if($slug=='user')
													<li class="m-nav__item">
														<a href="{{ url('profile') }}" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-profile-1"></i>
															<span class="m-nav__link-title">
																<span class="m-nav__link-wrap">
																	<span class="m-nav__link-text">My Profile</span>
																</span>
															</span>
														</a>
													</li>
													@elseif($slug=='admin')
													<li>
														<a href="{{ url('myprofile') }}" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-profile-1"></i>
															<span class="m-nav__link-title">
																<span class="m-nav__link-wrap">
																	<span class="m-nav__link-text">My Profile</span>
																</span>
															</span>
														</a>
													</li>
													@endif 
													
													<li class="m-nav__separator m-nav__separator--fit">
													</li>
													<li class="m-nav__item">
														<a href="{{ url('logout') }}" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>

				<!-- END: Topbar -->
			</div>
		</div>
	</div>
</header>
<!-- END Header -->

<script type="text/javascript">
    document.getElementById("copy-link").onclick = function() {
        this.select();
        document.execCommand('copy');
        toastr.success("Link Copied.",{timeOut: 1000});
    }
</script>