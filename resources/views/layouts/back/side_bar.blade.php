
<?php
    $slug = Sentinel::getUser()->roles()->first()->slug;
?>
<!-- BEGIN Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

	<!-- BEGIN: Brand -->
	<div class="m-brand  m-brand--skin-light ">
		<a href="{{ url('/') }}" class="m-brand__logo">
			<img alt="" src="{{ URL::asset('assets/dashboard/images/favicon.png') }}" />
		</a>
	</div>

	<!-- END: Brand -->
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" m-menu-scrollable="true" m-menu-dropdown-timeout="500">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
		@if($slug == 'user')
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Dashboard"><a href="{{ url('dashboard') }}" class="m-menu__link">
				<i class="m-menu__link-icon flaticon-line-graph"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Packages"><a href="{{ url('package-list') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-business"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Deposit"><a href="{{ url('deposit') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-piggy-bank"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Witdraw"><a href="{{ url('withdraw') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-paper-plane"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Invested"><a href="{{ url('invest') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-statistics"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Referrals"><a href="{{ url('my-network') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-network"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="History"><a href="{{ url('history/commission') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-stopwatch"></i></a>
			</li>
			<!-- <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="History"><a href="{{ url('history/commission') }}:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-stopwatch"></i><span
					 class="m-menu__link-text">History</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">History</span></span></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('history/deposit') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Deposit History</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('history/commission') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Commission History</span></a></li>
						
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('history/withdraw') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">withdraw History</span></a></li>
					</ul>
				</div>
			</li> -->
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true" m-menu-submenu-toggle="click"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-settings"></i><span
					 class="m-menu__link-text">Settings</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('profile') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Profile</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('setting/change-password') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Change Password</span></a></li>
					</ul>
				</div>
			</li>
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-1" aria-haspopup="true" m-menu-submenu-toggle="click"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-info"></i><span class="m-menu__link-text">Help</span><i
					 class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom-1" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Help</span></span></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="#" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Support</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="#" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Blog</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="#" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Terms</span></a></li>
					</ul>
				</div>
			</li>
		@elseif($slug == 'admin')
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Dashboard"><a href="{{ url('admin-dashboard') }}" class="m-menu__link">
				<i class="m-menu__link-icon flaticon-line-graph"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="User Manage"><a href="{{ url('users-manage') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-users"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Withdrwal Manage"><a href="{{ url('withdraw-manage') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-paper-plane"></i></a>
			</li>
			<li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Tawk Chat"><a href="https://dashboard.tawk.to/?lang=en#/dashboard" class="m-menu__link" target="_blank"><i class="m-menu__link-icon flaticon-speech-bubble-1"></i></a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="History"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-stopwatch"></i><span
					 class="m-menu__link-text">History</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">History</span></span></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('deposit-history') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Deposit History</span></a></li>
						
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('withdraw-history') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Witdraw History</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('invest-history') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Invest History</span></a></li>
					</ul>
				</div>
			</li>
			
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true" m-menu-submenu-toggle="click" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Settings"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-settings"></i><span
					 class="m-menu__link-text">Settings</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('company-setting') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Company Detials</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('myprofile') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Profile</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="password-change" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Change Password</span></a></li>
					</ul>
				</div>
			</li>
			@endif
		</ul>
	</div>
	<!-- END: Aside Menu -->
</div>
<div class="m-aside-menu-overlay"></div>
<!-- END Left Aside -->