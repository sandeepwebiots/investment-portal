@extends('layouts.back.master')

@section('title') Withdraw | Invetex @endsection

@section('style')

@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Withdraw</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Withdraw</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Withdrawals
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <tr>
                            <th width="5%">#</th>
                            <th width="10%">Coin</th>
                            <th width="40%">Total Balance</th>
                            <th >Action</th>
                        </tr>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>BTC</td>
                        <td>{{ Sentinel::getuser()->btc_balance }} BTC</td>
                        <td>
                            <a href="{{ url('withdraw/btc') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/btc') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>USD</td>
                        <td>{{ Sentinel::getuser()->usd_balance }} <i class="fas fa-dollar-sign"></i></td>
                        <td>
                            <a href="{{ url('withdraw/usd') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/btc') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>EURO</td>
                        <td>{{ Sentinel::getuser()->euro_balance }} <i class="fas fa-euro-sign"></i></td>
                        <td>
                            <a href="{{ url('withdraw/euro') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/eur') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>GBP</td>
                        <td>{{ Sentinel::getuser()->gbp_balance }} <i class="fas fa-pound-sign"></i></td>
                        <td>
                            <a href="{{ url('withdraw/gbp') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/gbp') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#m_table_1').DataTable();
} );
</script>
@endsection 