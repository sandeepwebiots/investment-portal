@extends('layouts.back.master')

@section('title') Deposit with Skrill | {{ strtoupper($coin) }} @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Deposit {{ strtoupper($coin) }}</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit {{ strtoupper($coin) }}</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-xl-4">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              Deposit with Skrill
                            </h3>
                        </div>
                    </div>
                </div>
                <form class="m-form m-form--fit m-form--label-align-right skrill-form" target="_blank" action="https://pay.skrill.com" method="POST">
                    {{ csrf_field() }}
                    <div class="m-portlet__body">
                        <!-- <div class="form-group m-form__group m--margin-top-10">
                            <div class="alert m-alert m-alert--default" role="alert">
                                <code>Please enter your partner correct email.</code>
                            </div>
                        </div> -->
                        <div class="form-group m-form__group row">
                            
                            <label for="example-text-input" class="col-4 col-form-label">Deposit Amount</label>
                                
                            <div class="col-8">
                                <input class="form-control m-input" type="text" name="amount" id="amount" placeholder="Deposit Amount" autocomplete="off" maxlength="10">
                                @if($errors->has('amount'))<strong class="text-danger">{{ $errors->first('amount') }}</strong>@endif
                            </div>
                        </div>
                        <input type="hidden" name="pay_to_email" value="{{ $setting->skrill_email_account }}">
                        <input type="hidden" name="currency" value="{{ strtoupper($coin) }}">
                        <input type="hidden" name="return_url" value="{{ url('/deposit-skrill-return') }}">
                        <input type="hidden" name="return_url_target" value="4">
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit ">
                        <div class="m-form__actions">
                            <div class="row">
                                <div class="col-12 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="submit">Deposit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{ strtoupper($coin) }} Deposit
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="deposit-table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Amount</th>            
                                <th scope="col">Payment Batch Num</th>            
                                <th scope="col">Transaction Id</th>            
                                <th scope="col">Payment Status</th>
                                <th scope="col">Payment Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach($deposit as $depo)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $depo->amount }}</td>
                                    <td>{{ $depo->payer_account }} @if($depo->payer_account == Null) N/A @endif</td>
                                    <td>{{ $depo->txid }} @if($depo->txid == Null) N/A @endif</td>
                                    <td>@if($depo->status == 0)<span class="badge badge-warning">Pending</span>
                                        @elseif($depo->status == 1)<span class="badge badge-success">Complete</span>
                                        @elseif($depo->status == 2)<span class="badge badge-danger">Cancelled</span>
                                        @endif
                                    </td>
                                    <td>{{ $depo->created_at->format('d M Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit-table').DataTable();
} );
</script>
<script>
    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {

    $('.skrill-form').validate({ // initialize the plugin
        rules: {
            amount: {
                required: true,
            }
        }
    });

});
</script>
@endsection