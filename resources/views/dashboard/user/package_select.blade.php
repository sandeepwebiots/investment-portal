@extends('layouts.back.master')

@section('title') Package @endsection

@section('style')
<style>
    .error {
        margin: 0px!important;
        color: #ff2b2b!important;
    }
    .error-balance{
        margin-left:12px;
        color: #ff2b2b;
    }
    p {
        font-size: 16px!important;
    }   
    .media-body .badge{
        font-size: 16px;
    }
    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 10px;
        cursor: pointer;
        background: #73aee1;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        background: #eeeeee;
        cursor: pointer;
    }
</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Package</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Package</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Invets This Package
                            </h3>
                        </div>
                    </div>
                </div>
                <table class="table m-0">
                    <tbody>
                        <tr> 
                            <td>Durations</td>
                            <td>{{ $pack->duration }} weeks</td> 
                        </tr>
                        <tr> 
                            <td>Package Limit</td>
                            <td>{{ $pack->min }}-{{ $pack->max }} USD</td> 
                        </tr>
                        <tr> 
                            <td>Profit</td>
                            <td>{{ $pack->profit }} % daily ROI</td> 
                        </tr>
                        <tr> 
                            <td>Minimum Amount</td>
                            <td>{{ $pack->min }} USD (Need for Purchase Package {{ $pack->duration }} weeks)</td> 
                        </tr>
                        <tr> 
                            <td>Select Amount</td>
                            <td>
                                <form action="{{ route('package.preview') }}" method="post" id="package-amount">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="currency_value" id="currency_value">
                                    <input type="hidden" name="package_id" value="{{ $pack->id }}">
                                   <input type="range" name="amount" id="myRange" min="{{ $pack->min }}" max="{{ $pack->max }}" class="set-full-width slider" onchange="updateTextInput(this.value);">
                                   <span><b>Note:</b> Scroll to change amount.</span><br>
                                   <table>
                                    <tbody>
                                        <tr>
                                            <td><b>USD Amount</b> </td>
                                            <td><input id="usd_amount" name="usd_amount" type="number" readonly=""></td>
                                        </tr>
                                        <tr>
                                            <td><b>USD ROI Amount</b></td>
                                            <td><input id="roi_amount" name="roi_amount" type="number" readonly=""></td>
                                        </tr>
                                        <tr>
                                            <td><b>Pay with</b><strong class="text-danger"> * </strong></td>
                                            <td><select name="currency" id="currency" class="btn form-control b-light" onchange="">
                                                    <option value="usd">USD</option>
                                                    <option value="gbp">GBP</option>
                                                    <option value="euro">EURO</option>
                                                    <option value="btc">BTC</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>Balance</b></td>
                                            <td><input type="number" name="balance" id="balance" readonly="">
                                                <span id="setCurrency"></span><span class="error-balance"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Amount</b></td>
                                            <td><input type="number" name="amount" id="amount" readonly="">
                                                <span id="setCurrency1"></span>
                                                </td>
                                        </tr>
                                         <tr>
                                            <td><b>ROI Amount</b></td>
                                            <td><input id="amount_roi" name="amount_roi" type="number" readonly="">
                                                <span id="setCurrency2"></span> </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" id="next_btn" class="btn" style="background-color:#021325;color: white;"><i class="fa fa-eye"></i>&nbsp; Preview Payment</button>
                               </form>
                            </td> 
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ URL::asset('assets/dashboard/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/validation/additional-methods.min.js') }}"></script>
<script>

$('#package-amount').validate({

    rules:{
        usd_amount:{
            required: true,
        },
        roi_amount:{
            required: true,
        },
        currency:{
            required: true,
        }
    }
});


var usd_btc = 0.00030;  // USD to BTC convert
var usd_eur = 0.88;  // USD to EUR convert
var usd_gbp = 0.79;  // USD to GBP convert

function updateTextInput(val) {
  document.getElementById('usd_amount').value=val; 
  var profit = ({{ $pack->profit }});
  var roi = val * profit / 100;
  document.getElementById('roi_amount').value=roi;

  balanceCheck();
  // console.log(roi);  
}



function balanceCheck()
{
    var currency = $('#currency').val();
    var profit = ({{ $pack->profit }});
    var currency_value = $('#currency_value').val();

    // console.log(currency_value);
       var usd_amount =  $("#usd_amount").val();
    if (currency == 'usd') {
        var balance = "{{Sentinel::getUser()->usd_balance}}";
        var amount = $("#usd_amount").val();        
        var amount_roi = amount * profit / 100;
    }else if(currency == 'gbp'){
        var balance = "{{Sentinel::getUser()->gbp_balance}}";
        var amount = usd_amount * currency_value;
        var amount_roi = amount * profit / 100;
    }else if(currency == 'euro'){
        var balance = "{{Sentinel::getUser()->euro_balance}}";
        var amount = usd_amount * currency_value;
        var amount_roi = amount * profit / 100;
    }else if(currency == 'btc'){
        var balance = "{{Sentinel::getUser()->btc_balance}}";
        var amount = usd_amount * currency_value;
        var amount_roi = amount * profit / 100;
    }
    $("#amount").val(amount);
    $("#balance").val(balance);
    if (balance > amount) {
        $('#next_btn').prop('disabled',false);
        $('.error-balance').html('');
    }else{
        $('.error-balance').html('Insufficient Balance');
        $('#next_btn').prop('disabled',true);
    }
    var setcurrency = currency.toUpperCase();
    $("#setCurrency,#setCurrency1,#setCurrency2").html(setcurrency);
    document.getElementById('amount_roi').value=amount_roi;
}

    

$("#currency").change(function(){   // 1st

    var currency = $('#currency').val();
   
    $.ajax({    //create an ajax request to display.php
        type: "get",
        url: "balance",             
        dataType: "html",
        data: {
            'currency': currency,
            '_token': '{{ csrf_token() }}',
           //expect html to be returned                
        },
        success: function(response){ 
             // console.log(response);
           var arr = JSON.parse(response);
           if (arr['GBP']) {
            $('#currency_value').val(arr['GBP']);
           }else if (arr['BTC']) {
            $('#currency_value').val(arr['BTC']);
           }else if (arr['EUR']) {
            $('#currency_value').val(arr['EUR']);
           }
            balanceCheck();
        }
    }); 
});
</script>
@endsection