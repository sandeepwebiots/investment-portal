@extends('layouts.back.master')

@section('title') Invoice @endsection

@section('style')

@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Invoice</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Invoice</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>


<div class="m-content">
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-invoice-1">
                <div class="m-invoice__wrapper">
                    <div class="m-invoice__body m-invoice__body--centered">
                        <div class="table-responsive">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                            Final Invoice
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                <tbody>
                                     <tr> 
                                            <td>Package</td>
                                            <td>{{ $package->min }}-{{ $package->max }} USD</td> 
                                        </tr>
                                        <tr> 
                                            <td>Durations</td>
                                            <td>{{ $package->duration }} weeks</td> 
                                        </tr>
                                         <tr> 
                                            <td>Profit</td>
                                            <td>{{ $package->profit }} %</td> 
                                        </tr>
                                        <tr> 
                                            <td>Start Date</td>
                                            <td>{{ $purchases->start_date }}</td> 
                                        </tr>
                                        <tr> 
                                            <td>Valid Date</td>
                                            <td>{{ $purchases->valid_date }}</td> 
                                        </tr>
                                        <tr> 
                                            <td>Transaction Status</td>
                                            <td class="text-warning">Pending</td> 
                                        </tr>
                                        <tr> 
                                            <td>Payment Type</td>
                                            <td><strong>{{ strtoupper($purchases->type) }}</strong></td> 
                                        </tr>
                                        <tr> 
                                            <td>Amount</td>
                                            <td><strong>{{ $purchases->amount }} {{ strtoupper($purchases->type) }}</strong></td> 
                                        </tr>
                                        <tr> 
                                            <td>Daily Profit Amount ( {{ $package->profit }} %)</td>
                                            <td><strong>{{ $purchases->usd_roi_amount }} USD</strong></td> 
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center"><a href="{{ url('pay') }}/{{ $purchases->id }}" class="btn btn-success">Pay</a></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection