@extends('layouts.back.master')

@section('title') Packages @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Our Plans</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Our Plans</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Invets This Package
                            </h3>
                        </div>
                    </div>
                </div>
                <form class="m-form" method="post" action="{{ route('package.select') }}">
                    {{ csrf_field() }}
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group">
                                @if($user->referral_id)
                                    @if($user->parent_id == '')
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        <strong>Oh snap!</strong> Your position is not set yet! Please say your Referrer to set your position.
                                    </div>
                                    @endif
                                @endif
                                @if($errors->has('package'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    <strong>Error</strong> {{ $errors->first('package') }}
                                </div>
                                @endif
                                <div class="row">
                                    @foreach($package as $pack)
                                    <div class="col-lg-6">
                                        <label class="m-option">
                                            <span class="m-option__control">
                                                <span class="m-radio m-radio--brand m-radio--check-bold">
                                                    <input type="radio" name="package" id="radio{{ $pack->id }}" value="{{ $pack->id }}">
                                                    <span></span>
                                                </span>
                                            </span>
                                            <span class="m-option__label">
                                                <span class="m-option__head">
                                                    <span class="m-option__title">
                                                        {{ $pack->duration }} weeks Package
                                                    </span>
                                                    <span class="m-option__focus">
                                                       <span class="m-badge m-badge--focus m-badge--wide"> {{ $pack->min }}$-{{ $pack->max }}$ </span>
                                                    </span>
                                                </span>
                                                <span class="m-option__body">
                                                    Estimated {{ $pack->duration }}
                                                    (&nbsp;{{ $pack->profit }}% daily ROI for {{ $pack->duration }} weeks&nbsp;)
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                    @endforeach
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <button type="reset" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection