@extends('layouts.back.master')

@section('title') Company Details | Invetex @endsection

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Company Details</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Dashboard</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Company Details</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    Company Details
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                    Payment Accounts
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                    Settings
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        <div class="m-portlet__body">
                            <div>
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{ url('update-profile') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">1. Company Details</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Company Email</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="company_email" value="{{$company->email}}">
                                        @if($errors->has('company_email'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('company_email') }}</strong>
                                        </span> 
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Company Phone No</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="company_phone_no" value="{{$company->phone_no}}">
                                        @if ($errors->has('company_phone_no'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('company_phone_no') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Company Address</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="email" value="{{$company->address}}">
                                        @if ($errors->has('company_address'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('company_address') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>
                                <div class="col-lg-10 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_2">
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection