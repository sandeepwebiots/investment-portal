@extends('layouts.back.master')

@section('title') User Manage | Invetex @endsection

@section('style')

@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">User Manage</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">User Manage</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        User Manage
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $user->user_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if($user->status == 1 )<span class="m-badge m-badge--success m-badge--wide">Active</span>
                            @elseif($user->status == 0)<span class="m-badge m-badge--primary m-badge--wide">Deactive</span>
                            @elseif($user->status == 2)<span class="m-badge m-badge--primary m-badge--wide">Block</span>
                            @endif
                        </td>
                        <td>
                            <input type="hidden" name="id" class="userid" value="{{$user->id}}">
                            <a href="{{ url('user-delete') }}/{{ $user->id }}" title="Delete" onclick="return confirm('Are you sure you want to delete this User ?');" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning">Delete</a>
                            @if($user->status == 1)
                            <a href="{{ url('status-change') }}/{{ $user->id }}" onclick="return confirm('Are you sure you want to block this User ?');" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent" title="Block">Block</a>
                            @elseif($user->status == 2)
                            <a href="{{ url('status-change') }}/{{ $user->id }}" onclick="return confirm('Are you sure you want to active this User ?');" title="Active" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">Active</a>
                            @endif
                            
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
            </table>
        </div>
    </div>
</div>

@endsection