@extends('layouts.back.master')

@section('title') Withdrawal History | Invetex @endsection

@section('style')

@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Withdrawal History</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Withdrawal History</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Withdrawal History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>coin</th>
                        <th>Type</th>
                        <th>Address | Account no</th>
                        <th>Amount</th>            
                        <th>Transaction id</th>            
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($withdraw as $withd)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $withd->withdr_user->user_name }}</td>
                            <td>{{ strtoupper($withd->coin) }}</td>
                            <td>@if($withd->partner_email)
                                <span class="m-badge m-badge--info m-badge--wide m-badge--rounded" >Withdraw for partner</span>
                                @else  
                                @endif
                            </td>
                            <td>{{ $withd->address }}</td>
                            <td>{{ $withd->amount }}</td>
                            <td>{{ $withd->txid }}</td>
                            <td>@if($withd->status == 0)<span class="m-badge m-badge--warning m-badge--wide">Pending</span>
                                @elseif($withd->status == 1)<span class="m-badge m-badge--success m-badge--wide">Complete</span>
                                @elseif($withd->status == 2)<span class="m-badge m-badge--danger m-badge--wide">Reject</span>
                                @endif
                            </td>
                            <td>{{ $withd->created_at->format('d M Y') }}</td>
                        </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection