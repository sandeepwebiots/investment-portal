@extends('layouts.home.master')

@section('title')  Investment | Blogs @endsection

@section('content')

<section class="top_panel_image top_panel_image_1">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title">Blogs</h1>
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="{{ url('/') }}">Home</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current">Blogs</span>
        </div>
    </div>
</section>
<div class="page_content_wrap page_paddings_yes">
    <div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                  <article class="post_item post_item_excerpt">
                            <div class="post_featured">
                                <div class="post_thumb">
                                    <a class="hover_icon hover_icon_link" href="single-post.html"><img alt="" src="{{ URL::asset('assets/home/images/post-11-770x434.jpg') }}"></a>
                                </div>
                                <div class="cat_post_info">
                                    <span class="post_categories"><a class="category_link" href="#">Money</a></span>
                                </div>
                            </div>
                            <div class="post_content clearfix">
                                <h3 class="post_title"><a href="single-post.html">Why buying a big house is a bad investment</a></h3>
                                <div class="post_info">
                                    <span class="post_info_item"><a class="post_info_date" href="#">April 29, 2016</a></span> <span class="post_info_item post_info_tags">in <a class="category_link" href="#">Money</a></span> <span class="post_info_item post_info_counters"><a class="post_counters_item icon-comment-light" href="single-post.html" title=""><span class="post_counters_number">0</span> Comments</a></span>
                                </div>
                                <div class="post_descr">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci...</p><a class="sc_button sc_button_style_filled sc_button_size_small readmore" href="single-post.html">Read more</a>
                                </div>
                            </div>
                        </article>
            </div>
            <div class="col-md-4">
                 <div class="sidebar widget_area scheme_original">
                        <div class="sidebar_inner widget_area_inner">
                            <aside class="widget widget_categories">
                                <h5 class="widget_title">Categories</h5>
                                <ul>
                                    <li class="cat-item">
                                        <a href="all-posts.html">Business</a>
                                    </li>
                                    <li class="cat-item">
                                        <a href="all-posts.html">Consulting</a>
                                    </li>
                                    <li class="cat-item">
                                        <a href="all-posts.html">Economy</a>
                                    </li>
                                    <li class="cat-item">
                                        <a href="all-posts.html">Funding Trends</a>
                                    </li>
                                    <li class="cat-item">
                                        <a href="all-posts.html">Money</a>
                                    </li>
                                    <li class="cat-item">
                                        <a href="all-posts.html">Our Gallery</a>
                                    </li>
                                </ul>
                            </aside>
                            <aside class="widget widget_recent_posts">
                                <h5 class="widget_title">Recent Posts</h5>
                                <article class="post_item with_thumb">
                                    <div class="post_thumb"><img alt="" src="{{ URL::asset('assets/home/images/post-9-75x75.jpg')}}"></div>
                                    <div class="post_content">
                                        <h6 class="post_title"><a href="single-post.html">Broker-dealer owner indicated in $17 million dump scheme</a></h6>
                                        <div class="post_info">
                                            <span class="post_info_item"><a class="post_info_date" href="single-post.html">April 30, 2016</a></span> <span class="post_info_item post_info_counters"><a class="post_counters_item icon-comment-light" href="single-post.html"><span class="post_counters_number">2 Comments</span></a></span>
                                        </div>
                                    </div>
                                </article>
                                <article class="post_item with_thumb">
                                    <div class="post_thumb"><img alt="" src="{{ URL::asset('assets/home/images/post-11-75x75.jpg')}}"></div>
                                    <div class="post_content">
                                        <h6 class="post_title"><a href="single-post.html">Why buying a big house is a bad investment</a></h6>
                                        <div class="post_info">
                                            <span class="post_info_item"><a class="post_info_date" href="single-post.html">April 29, 2016</a></span> <span class="post_info_item post_info_counters"><a class="post_counters_item icon-comment-light" href="single-post.html"><span class="post_counters_number">0 Comments</span></a></span>
                                        </div>
                                    </div>
                                </article>
                                <article class="post_item with_thumb">
                                    <div class="post_thumb"><img alt="" src="{{ URL::asset('assets/home/images/post-10-75x75.jpg')}}"></div>
                                    <div class="post_content">
                                        <h6 class="post_title"><a href="single-post.html">Credit Suisse CEO focuses on wealth management</a></h6>
                                        <div class="post_info">
                                            <span class="post_info_item"><a class="post_info_date" href="single-post.html">April 28, 2016</a></span> <span class="post_info_item post_info_counters"><a class="post_counters_item icon-comment-light" href="single-post.html"><span class="post_counters_number">0 Comments</span></a></span>
                                        </div>
                                    </div>
                                </article>
                            </aside>
                        </div>
                    </div><!-- /.sidebar -->
            </div>
        </div>
      </div>
    </div>
</div>

@endsection