@extends('layouts.home.master')

@section('title')  Investment | About US @endsection

@section('content')
<style type="text/css">
   /* .sc_services_item .sc_services_item_content p{
        text-align: justify;
        width: 100%;
    }*/
</style>

<section class="top_panel_image top_panel_image_1">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title">About Us</h1>
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="{{ url('/') }}">Home</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current">About Us</span>
        </div>
    </div>
</section>
<div class="page_content_wrap page_paddings_no company-detail">
    <div class="sc_section custom_bg_2">
        <div class="container">
            <div class="sc_empty_space" data-height="3em"></div>
            <div class="">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge">
                    <h2 class="sc_services_title sc_item_title">About Us</h2>
                    <div class="sc_services_descr sc_item_descr">
                        There are hundreds of thousands of investment platforms either physical or online. Whenever you have some money in your hands, you would definitely want to invest it in highly profitable platform so that you can multiply your own money many times and you can become rich within the least time. However, you may get confused as none of the platforms guarantees you about the profit. We take pride in announcing such an investment platform where your investment will be secured and profit will be guaranteed. We have come up with such a unique idea that will induce you itself to invest in because not only our idea is easy to understand but it is practically valuable. The chain of investors is getting longer and longer day by day. Hence, hold our hands and we will guarantee you the profit on your investments. ABC Plan has zero percent risk and you will surely be earning profit on every single penny that you will be investing here. 
                    </div>
                    <h2 class="">About Company:</h2>
                    <div class="">
                        <ul>
                            <li>Now a day’s E-commerce business is widely growing day by day. E-commerce concept was started in 1991 and then it grows day by day. E-commerce Business has captured Billions of Dollars Market cap all over the world.</li>
                            <li>It’s worth growing day by day. It’s not only growing in USA the E-Commerce in Europe was worth €534 Billion in 2017. </li>
                            <li>The company has been working since last 5 years. The company has E-commerce stores on wide level On Amazon, EBay, Ali Baba and Shopify.  We also selling services on Amazon allow top products like Toys and games, Grocery, Electronics, Books, sports & outdoors, shoes, jewelry clothing and more to sell professional services directly to Amazon. We are working for many years on finance and e-commerce.</li>
                            <li>Do you think that you are not skilled enough to generate profit from Amazon, EBay or other online stores? Don’t you have guts to sell the products? Are you even fed up of affiliate marketing because it pays back less and it requires a lot of time and hard work? Well, we are good to sell the products as we have a team of expert marketers. We buy the products in bulk and at sales prices and then we sell them in the market where there is big demand so that maximum profit can be generated. Every product that we will be purchasing and selling will be providing guaranteed profit. In simple words, we will be working as an agent. Purchases will be yours and off course profit will be yours. For our services, we will only charge fixed profit. This safest investment plan has been liked by all of our clients so far because they have been earning real profit without any fear of loss. If you are also interested to invest in a platform with no risk of loss and even no risk of “no profit” then delay no more to choose any of the Plan.</li>
                        </ul>
                    </div>
                    <h2 class="">Drop Shipping:</h2>
                    <div class="">
                        <ul>
                            <li>Moreover, we have also been dealing in drop shipping business. We have a big number of clients who have been partnering with us in this type of business for many years because they find their investment 100% safe and profitable here.</li>
                        </ul>
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="sc_services_item sc_services_item_2">
                            <div class="sc_services_item_content">
                                <div class="">
                                    <div class="sc_services_item_description">
                                        <h2>Plans we are offering</h2>
                                        <p>We offering different Plans to our clients so as to make investment as much flexible as much it can be. the plans have been set on the basis of profit calculation time. We are currently offering four different investment plans:</p> 
                                        <h7><b>Toddling investor’s plan:-</b></h7><p> profit on all the sales is calculated and provided to our clients on weekly basis . If you want to get the profit every week then you should go with this plan. (Conditions: 1. 20% profit will be paid out daily on all your investment.  Minimum investment should be $200 up to 1000$ to buy this plan.)This package will be expired in 35 weeks.</p>
                                        <h7><b>Hand shaker’s plan:-</b></h7><p> our clients will be able to receive profit on every week . (Conditions: 1. 20% profit will be paid daily on whole your investment).Minimum investment limit is $1200 up to 3000$.The package will be validate for 40 weeks.</p>
                                        <h7><b>Pro investor’s plan:-</b></h7><p> We will calculate and provide profit to our client at the end of every week.Although you will be receiving same profit once in a week but you will be earning big profit on this plan.This package gives you a flexibility of 5 extra weeks. 
                                        (Conditions: 1.20% profit will be paid daily.Investment limit applies to Plan 3 is 3500$ up to 6000$.</p>
                                         <h7><b>Premium Investor’s plan:-</b></h7><p> The plan 4 is highly profitable with more flexibility with extra weeks . (Conditions: 1.20% profit will be paid daily. Investment limit applies to Plan 3 is 6500$up to 30000$.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="">Benefits of Investing in our Plan</h2>
                    <p>You will not have heard about such a realistic and unique investment plan before that we have brought for you.</p>
                    <div class="">
                        <p>You must know why you should invest in this plan. Well, money is precious because it is always a scarce mean for everyone in this world. That’s why people want to invest their money in the most optimal way. These are the benefits of investing your money in this plan:</p>
                        <ul>
                            <strong><li>No Risk</li></strong>
                            <p>Off course, you would love to invest your money in such a platform where there is no risk. There is no need to invest your money in anything else but only and only in our Investment plans because it is the safest platform where you are not going to face any risk. Any business can have two types of risks. Firstly, there is the risk of loss and secondly, there is the risk of no profit. None of these risks is involved in this investment plan and for sure, you are going to enjoy the profit.</p>
                            <strong><li>Guaranteed Profit</li></strong>
                            <p>You can blindly invest your money here because without making any efforts, you are going to get the profit. People are no more putting their money in Bitcoin, they are not much interested in any other online business and it is because of the reason that those platforms are not promising the real profit. When it comes to our investment plans, you will not only get profit but you will earn “big profit”.</p>
                            <strong><li>Realistic Investment Idea</li></strong>
                            <p>In internet world, there are just a few business plans that are realistic. We are not offering any transparent plan that is difficult to understand by a human mind and on which you cannot trust. We are offering you a realistic investment idea where you will be investing your money, we will be purchasing products from authentic online stores, we will be selling those products at high prices in the markets having high demand and then we will be sharing the profit. Don’t you think that it is a realistic investment idea as compard to any other investment plan being offered in the internet world.</p>
                            <strong><li>No expertise required</li></strong>
                            <p>There would be many of you who have enough money in their hands to start their proprietary business but the problem is that they don’t have any expertise. They cannot do a business because they feel they are not skilled enough to do so. Well, you don’t need any expertise, just bring your money and expertise will be provided by us. We have a team of expert marketers who are experienced to sell the products with a big profit margin. Don’t you think that it is really great to start your own business even with zero.</p>
                            <h6>Conclusion:</h6>
                            <p>What’s your plan now! When you are going to start your realistic business! Here, at ABC Investment, your resources will be utilized in the most optimal way so as to generate big profit. With every single dollar, you will be generating hundreds of more dollars. We are building a chain of our clients and we are growing our business more and more. Join your hands with us and build a long term business relationship! </p>
                        </ul>
                        
                    </div>
                </div><!-- /.sc_services -->
            </div><!-- /.sc_services_wrap -->
        </div>
    </div>
    
    <!-- <div class="container">
        <div class="content_wrap">
            <div class="margin_top_huge">
                <h2 class="sc_team_title sc_item_title">Mass Media</h2>
                <div class="sc_team_descr sc_item_descr">
                </div>
                <div class="columns_wrap sc_skills_columns margin_bottom_medium">
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/bitcoinst.png') }}"> </a>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/live-bitcoin.png') }}"> </a>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/120btc.png') }}"> </a>
                    </div>
                </div>
                <div class="columns_wrap sc_skills_columns margin_bottom_medium">
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/merkle.png') }}"> </a>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/newsbtc.png') }}"> </a>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/bitcoiner.png') }}"> </a>
                    </div>
                </div>
                <div class="columns_wrap sc_skills_columns">
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/diario.png') }}"> </a>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/forocoin.png') }}"> </a>
                    </div>
                    <div class="sc_skills_column column-1_3">
                        <a href="#"><img alt="" src="{{ URL::asset('assets/home/images/cointurk.png') }}"> </a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="2em"></div>
            <div class="sc_team_wrap">
                <div class="sc_team sc_team_style_team-3 margin_top_huge margin_bottom_huge aligncenter">
                    <h2 class="sc_team_title sc_item_title">Our Team Members</h2>
                    <div class="sc_team_descr sc_item_descr">
                        <p>We feel proud to introduce our team members who have been providing services in their respective fields for decades. We have the best marketers in our team who have deep sense of analysis. They plan out very detailed analysis reports for our clients in which they compare the trends of purchases and sales or particular products, market demand, profit margin, pricing, interests of people and a lot of other things. Hence, they prepare such an investment plan where profit is guaranteed.</p>
                        <p>Not only they are good in internal working but when it comes to selling the products, they are the best. Because of their marketing tactics, no product stays in our stores but these are sold immediately once bought. They have been selling hundreds of products on a daily basis and imagine how much profit our marketers have been generating on behalf of the clients!</p>
                        <p>Besides marketers, we have experienced and trained members in our customer team. They are always there to answer your queries and to solve your problems. Whether you want to get any information about our Plan or you want to buy any of the offered packages or anything else, do not hesitate to contact us.</p>
                    </div>
                </div><!-- /.sc_team -->
            </div>
        </div>
            <div class="sc_empty_space" data-height="0.5em"></div>
    </div>
    </div>
</div>

@endsection