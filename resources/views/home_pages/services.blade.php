@extends('layouts.home.master')

@section('title')  Investment | Services @endsection

@section('content')

<section class="top_panel_image top_panel_image_2">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title">Services</h1>
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="{{ url('/') }}">Home</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current">Services</span>
        </div>
    </div>
</section>
    <div class="page_content_wrap page_paddings_no company-detail">
        <div class="sc_section custom_bg_2">
            <div class="content_wrap">
                <div class="sc_empty_space" data-height="3em"></div>
                <div class="sc_services_wrap">
                    <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge">
                        <h2 class="sc_services_title sc_item_title">Our Services</h2>
                        <div class="sc_services_descr sc_item_descr">
                            Why you should be investing your money in this investment Platform? How we are different from others? Which services you can expect from us? Well, there would be many things revolving in your mind. There are the following services in which we have been dealing:
                        </div>                        
                        
                        <div class="">
                            <ul>
                                <strong><li>We value your money</li></strong>
                                <p>We know that your money is precious. Everyone has Limited amount of money in his pocket or in his accounts. You must spend or invest your money in an aptimal way so that it can produce more and more money. We have been providing you the best investment plan that is guaranteed and that provides you the maximum amount of profit.</p>
                                <strong><li>Agency Services</li></strong>
                                <p>At that Investment, we will be working as an agent on your behalf. Investment will be yours and off course, business will be yours. We will be making all the activities on your behalf and we will off course be using your money in the best possible way so as to provide you with good amount of profit. Basically we purchase products from highly authentic online store and then we sell the product when there is high demand for those thus generating big amount of profit. We will charge a little Commission for our services and besides that, all the profit will be yours.</p>
                                <strong><li>Best Marketing Services</li></strong>
                                <p>You don't need to worry if you don't have marketing skills because here at this Platform Investment, we have a team of marketers who have the best marketing skills and you have up to date marketing strategies. Hence we will be providing the best marketing services so as to make your business more productive and to pay you the good amount of money against your investment. All that you have to do is to buy any of the available packages. We will be performing the rest of the activities and we will ensure you the profit.</p>
                                <strong><li>Customer Support Services</li></strong>
                                <p>You will find the best customer support here. We have the team of experienced and friendly numbers in our customer support. These members are always present to guide you in the best possible way and to answer your queries.Hence, if you have any confusion in your mind regarding investment plans that are available here, regarding the profit calculation or related to any other thing, do not hesitate to contact us through our customer support.</p>
                                <strong><li>We Guarantee No loss</li></strong>
                                <p>There are many businesses out there in internet field and we back that there would be no one who would be guaranteing you no loss. We are happy to announce that we guarantee no loss. Every single dollar that you will be providing as will be spent in the best possible way and we will ensure that you will not be facing any loss. Your investment will be safe that you can withdraw anytime.</p>
                                <strong><li>We guarantee profit</li></strong>
                                <p>Not only we guarantee no loss but we also guarantee profit. You can invest here blindly because we promise to provide you with good amount of profit. You cannot drop profit on weekly basis, bi monthly basis or monthly basis. It depends on the package that you will be going to buy. We have earned good reputation in the market. Believe in us, invest your money here and wait for as minimum as a week to get the profit.</p>
                                <p>In simple words, you will be getting the best services at this investment platform and you will have a long time work relationship with us because we are committed to provide you the best services along with providing you with maximum profit. Hence, delay no more to contact us and to join us in such a realistic business.</p>
                            </ul>
                            
                        </div>
                    </div><!-- /.sc_services -->
                </div><!-- /.sc_services_wrap -->
            </div>
        </div>
    </div>
</div>

@endsection