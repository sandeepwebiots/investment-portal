@extends('layouts.home.master')

@section('title')  Investment | FAQ @endsection

@section('content')

<section class="top_panel_image top_panel_image_1">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title">FAQ</h1>
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="{{ url('/') }}">Home</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current">Faq</span>
        </div>
    </div>
</section>




<section id="work" class="testimonial-bg">
  <div class="faq_gallery_sec">
    <div class="container">
      <div class="row">
        <div class=" col-md-12 faq_sec">
          <div class="title_container">
            <h3>Frequently Asked Questions</h3>
            <span class="decor_default"></span>
          </div>
          <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                  <div class="panel-heading">
                    <h5 class="panel-title">What are our Investment Plans ?</h5>
                  </div>
                </a>
                <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <p>Our Investment plans are very realistic source of investment where you can invest your money. In this investment platform, you will be offered different investment plans. You will be paying us any amount of money and out of that money; we will be purchasing high quality products at reasonable or discounted prices from very authentic online stores. Our marketers will analyse the markets where those products are highly demanded and are being sold at high rates. Then we will be selling those profits by creating profit margin and finally, we will be sharing the profits with our clients.</p>
                  </div>    
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                  <div class="panel-heading">
                    <h5 class="panel-title">How many investment plans are being offered ?</h5>
                  </div>
                </a>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>We are basically offering four different Investment plans.</p>
                    <ul>
                      <li><strong>Toddling investor’s plan :</strong> 1.2% Daily for 35 weeks investment limit is 200$-1000$</li>
                      <li><strong>Hand shaker’s plan :</strong> 1.2% Daily for 40weeks investment limit is 1200$-3000$</li>
                      <li><strong>Pro investor’s plan :</strong> 1.2% Daily for 45 weeks investment limit is 35000$-6000$</li>
                      <li><strong>Premium Investor’s plan :</strong> 1.2% Daily for 50weeks investment limit is 6500$-30000$</li>
                    </ul>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                  <div class="panel-heading">
                    <h5 class="panel-title">Who can invest in this investment plan ?</h5>
                  </div>
                </a>
                <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>There isn’t any sort of restriction. Anyone who has some amount in hands and wants to put it in some business can invest here. If you don’t have any skills even then you can buy an investment plan.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                  <div class="panel-heading">
                    <h5 class="panel-title">Is there any minimum limit for investment ?</h5>
                  </div>
                </a>
                <div id="collapse4" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>The minimum deposit is 200$.Thisinvestment program is very flexible where you can invest any amount of money. However, you should keep it in your mind that those investors who will be investing more will definitely be earning more.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                  <div class="panel-heading">
                    <h5 class="panel-title">Is it a realistic investment plan ?</h5>
                  </div>
                </a>
                <div id="collapse5" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Yes, it is 100% realistic plan and all the activities are going to happen in real. It is not something in the air but a team of expert marketers is working with us.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                  <div class="panel-heading">
                    <h5 class="panel-title">How can you deposit ?</h5>
                  </div>
                </a>
                <div id="collapse6" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>To activate any plan first you have to deposit through any payment gateway whatever you feel easy to use.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                  <div class="panel-heading">
                    <h5 class="panel-title">What payment methods we are offering ?</h5>
                  </div>
                </a>
                <div id="collapse7" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>We are offering now three payment gateway</p>
                    <ul>
                      <li>Bit coin</li>
                      <li>Skrill</li>
                      <li>Perfect Money</li>
                      <li>Bank wire (coming soon)</li>
                    </ul>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                  <div class="panel-heading">
                    <h5 class="panel-title">What is Bit coin ?</h5>
                  </div>
                </a>
                <div id="collapse8" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>If you don’t have bit coin wallet just visit these links to learn more about bit coin or create your wallet click bellow links</p>
                    <p><a href="https://login.blockchain.com/#/signup"><i class="fa fa-arrow-right" aria-hidden="true"></i> Blockchain </a></p>
                    <p><a href="https://www.coinbase.com/signup?locale=en"><i class="fa fa-arrow-right" aria-hidden="true"></i> Coinbase </a></p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                  <div class="panel-heading">
                    <h5 class="panel-title">What is perfect money ?</h5>
                  </div>
                </a>
                <div id="collapse9" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Perfect money is a financial service which allow user to make payments and transfer money securely and easy to convert their funds. if you don’t have perfect money account just signup through given below link</p>
                    <p><a href="https://perfectmoney.is/signup.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> PerfectMoney </a></p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                  <div class="panel-heading">
                    <h5 class="panel-title">What is skrill ?</h5>
                  </div>
                </a>
                <div id="collapse10" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Skrill also allow you to transfer money through internet Just sign up skrill account </p>
                    <p><a href="https://account.skrill.com/signup/v3/page1"><i class="fa fa-arrow-right" aria-hidden="true"></i> Skrill </a></p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
                  <div class="panel-heading">
                    <h5 class="panel-title">What’s about profit ?</h5>
                  </div>
                </a>
                <div id="collapse11" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>You will be paid 1.2 % daily on your whole investment the duration will depend upon your investment plan.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
                  <div class="panel-heading">
                    <h5 class="panel-title">After how long this investment will start generating profit ?</h5>
                  </div>
                </a>
                <div id="collapse12" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Anyways, you don't need to wait for long like any other business but you will start generating profit on daily basis.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
                  <div class="panel-heading">
                    <h5 class="panel-title">What’s about withdraw ?</h5>
                  </div>
                </a>
                <div id="collapse13" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>1.2 % profit will be show in your dashboard on daily basis but all your profit will be funded weekly through payment method which you use.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">
                  <div class="panel-heading">
                    <h5 class="panel-title">Why we funded profit weekly ?</h5>
                  </div>
                </a>
                <div id="collapse14" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>All our sales profit is received on weekly basis through Amazon so all user will be funded weekly.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">
                  <div class="panel-heading">
                    <h5 class="panel-title">How much profit one can earn by investing here ?</h5>
                  </div>
                </a>
                <div id="collapse15" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>The amount of profit may fluctuate on the basis of different factors. For example, it depends on the amount of money that you will be investment. Big Investors will be earning big profit off course. It may also depend on the basis of profit margin a product will be providing. Some of the products can be sold at very expensive rates and they create big difference in purchase and sale amount. However, there are some products that do not create such huge margin. Either more or less but the profit will be guaranteed. </p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">
                  <div class="panel-heading">
                    <h5 class="panel-title">What amount can be invested ?</h5>
                  </div>
                </a>
                <div id="collapse16" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>You can invest any amount that you want to. There is no investment limit and that's why this plan is very flexible for all the investors. Whether you want to take the start from just a 200$ dollars or you want to invest a big amount, the choice will be yours.</p>
                  </div> 
                </div>
              </div>

          <div class="title_container">
            <h3>Referral</h3>
            <span class="decor_default"></span>
          </div>
              <div class="panel panel-default">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">
                  <div class="panel-heading">
                    <h5 class="panel-title">Do we are offering any referral or affiliated program ?</h5>
                  </div>
                </a>
                <div id="collapse17" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Yes off course its big opportunity to everyone to get more profit through referral program.</p>
                  </div>    
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">
                  <div class="panel-heading">
                    <h5 class="panel-title">Why we offering referral program ?</h5>
                  </div>
                </a>
                <div id="collapse18" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Referral program is a opportunity for members to earn more profit and it will helps us to expand our business very quicklely and fast.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">
                  <div class="panel-heading">
                    <h5 class="panel-title">What’s about referral beneficial ?</h5>
                  </div>
                </a>
                <div id="collapse19" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>The user will be paid 10% direct bonus on all investment of your referral at the spot.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse20">
                  <div class="panel-heading">
                    <h5 class="panel-title">I don’t have any expertise but can I still invest in this plan ?</h5>
                  </div>
                </a>
                <div id="collapse20" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Almost all of our clients are not having expertise or any skills to start their own business. Hence you can also invest here even if you don't have any expertise or skills. We just want you to invest your money here and all the business related skills will be provided by the expert marketers that we have in our team.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">
                  <div class="panel-heading">
                    <h5 class="panel-title">How many clients have been working with you already ?</h5>
                  </div>
                </a>
                <div id="collapse21" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Thousands of clients have already been working with ask and they have pooled their money here because they are earning a lot of profit. They keep on investing more and more money because they trust on us blindly. We have proven yourself and our skills and that's why we have earned good reputation among our clients.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">
                  <div class="panel-heading">
                    <h5 class="panel-title">Is it totally an online investment ?</h5>
                  </div>
                </a>
                <div id="collapse22" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>It will be a real business but all the business activities will be held online. We will find the buyers and the sellers of the products online, build long term relationship with them and we will be expanding our business. All these activities will be done online.</p>
                  </div> 
                </div>
              </div>
              <div class="panel panel-default">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">
                  <div class="panel-heading">
                    <h5 class="panel-title">What to do if we have any queries or questions ?</h5>
                  </div>
                </a>
                <div id="collapse23" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>We have provided our contact details on contact us page. You can contact us any time through email or contact number. Our customer support team will be there to answer to your questions or queries and to solve your problem. What do you want to know about Investment plans or you want to explore anything else, do not hesitate to contact us. </p>
                  </div> 
                </div>
              </div>
          </div> <!-- End #accordion -->
        </div>    
      </div>
    </div>
  </div>
</section>


@endsection