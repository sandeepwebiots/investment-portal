@extends('layouts.home.master')

@section('title')  Investment | News @endsection

@section('content')

<section class="top_panel_image top_panel_image_1">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title">News</h1>
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="{{ url('/') }}">Home</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current">News</span>
        </div>
    </div>
</section>
<div class="page_content_wrap page_paddings_no">
    <div class="sc_section custom_bg_2">
        <div class="content_wrap">
            <div class="sc_empty_space" data-height="3em"></div>
            <div class="sc_services_wrap">
                <div class="sc_services sc_services_style_services-3 sc_services_type_images margin_top_huge margin_bottom_huge">
                    <h2 class="sc_services_title sc_item_title">Profitable Investments</h2>
                    <div class="sc_services_descr sc_item_descr">
                        Economic services provided by the finance industry, which encompasses<br>
                        a broad range of businesses that manage money, including credit unions.
                    </div>
                    <div class="sc_columns columns_wrap">
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_1">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="single-service.html"><img alt="" src="{{ URL::asset('assets/home/images/home_1-370x270.jpg') }}"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        01
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <span class="sc_services_item_subtitle">We Are Best Team</span>
                                    <h4 class="sc_services_item_title"><a href="single-service.html">Make Business Strategy</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Emerge as a pattern of activity as the organization adapts to its environment</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_2">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="single-service.html"><img alt="" src="{{ URL::asset('assets/home/images/home_3-370x270.jpg') }}"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        02
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <span class="sc_services_item_subtitle">The Best Corporate Company</span>
                                    <h4 class="sc_services_item_title"><a href="single-service.html">Company of Professionals</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>Certified specialists provide all types of financial, business &#038; customer support services</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                        <div class="column-1_3 column_padding_bottom">
                            <div class="sc_services_item sc_services_item_3">
                                <div class="sc_services_item_featured post_featured">
                                    <div class="post_thumb">
                                        <a class="hover_icon hover_icon_link" href="single-service.html"><img alt="" src="{{ URL::asset('assets/home/images/home_4-370x270.jpg') }}"></a>
                                    </div>
                                    <div class="sc_services_item_count">
                                        03
                                    </div>
                                </div>
                                <div class="sc_services_item_content">
                                    <span class="sc_services_item_subtitle">Always Forvard</span>
                                    <h4 class="sc_services_item_title"><a href="single-service.html">We Love Our Clients</a></h4>
                                    <div class="sc_services_item_description">
                                        <p>A wide range of customer services to assist in making cost effective use of a product</p>
                                    </div>
                                    <div class="sc_services_item_decoration"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.sc_services -->
            </div><!-- /.sc_services_wrap -->
        </div>
    </div>
</div>

@endsection