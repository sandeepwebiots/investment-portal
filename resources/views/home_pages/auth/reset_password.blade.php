<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <link href='{{ URL::asset("slider_home/images/favi.png") }}' rel="icon" sizes="192x192">
    <title>Reset Password</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/icofont.css') }}">
    <!-- fontawesome css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/fontawesome.css') }}">
    <!-- flagicon css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/flag-icon.css') }}">
    <!-- themify css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/themify.css') }}">
    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/bootstrap.css') }}">
    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/style.css') }}">      
    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/responsive.css') }}">

</head>

<body>

<!--page-wrapper Start-->
<div class="page-wrapper">

    <div class="container-fluid">

        <!--Reset Password page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-4 p-0">
                    <div class="auth-innerleft">
                        <div class="text-center">
                            <img src="{{ URL::asset('slider_home/images/reset_password.png') }}" class="img-fluid security-icon" alt="">
                        </div>

                    </div>
                </div>
                <div class="col-md-8 p-0">
                    <div class="auth-innerright">
                        <div class="authentication-box">
                            <h3>RESET YOUR PASSWORD</h3>
                            <div class="card mt-4 p-4">
                                <form class="theme-form" method="post" action="{{ url('reset-password') }}/{{ $user->email }}/{{ $resetCode }}">
                                    <input type="hidden" name="resetCode" value="{{ $resetCode }}">
                                    {{ csrf_field() }}
                                    <h5 class="f-16 mt-4 mb-3">CREATE YOUR PASSWORD</h5>

                                    <div class="form-group">
                                        <label class="col-form-label">New Password</label>
                                        <input name="new_password" type="password" class="form-control" placeholder="**********">
                                        @if($errors->has('new_password'))<span class="text-danger">{{ $errors->first('new_password') }}</span>@endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Retype Password</label>
                                        <input name="confnewpassword" type="password" class="form-control" placeholder="*********">
                                        @if($errors->has('confnewpassword'))<span class="text-danger">{{ $errors->first('confnewpassword') }}</span>@endif
                                    </div>
                                    <div class="form-group form-row mb-2">
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-secondary">Done</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Reset Password page end-->
    </div>

</div>
<!--page-wrapper Ends-->

<!-- latest jquery-->
<script src="{{ URL::asset('assets/dashboard/js/jquery-3.2.1.min.js') }}" ></script>
<!-- Bootstrap js-->
<script src="{{ URL::asset('assets/dashboard/js/popper.min.js') }}" ></script>
<script src="{{ URL::asset('assets/dashboard/js/bootstrap.js') }}" ></script>
<!-- Theme js-->
<script src="{{ URL::asset('assets/dashboard/js/script.js') }}" ></script>

</body>

</html>