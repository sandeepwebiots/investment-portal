@extends('layouts.home.master')

@section('title')  Investment | Product @endsection

@section('content')
<section class="top_panel_image top_panel_image_1">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title">Terms and Conditions</h1>
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="{{ url('/') }}">Home</a> <span class="breadcrumbs_delimiter"></span> <span class="breadcrumbs_item current">Terms and Conditions</span>
        </div>
    </div>
</section>
<main id="terms" class="inner-bottom-md">
                <div class="container">
                    <section class="section section-page-title text-center">
                        <div class="page-header">
                            <h2 class="page-title">Terms and Conditions</h2>
                            <p class="page-subtitle">This Agreement was last modified on July 20, 2014.</p>
                        </div>
                    </section><!-- /.section-page-title -->
                    <section class="section intellectual-property inner-bottom-xs">
                        <h2>Intellectual Propertly</h2>
                        <ol>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum.</li>
                            <li>Leo metus luctus sem, vel vulputate diam ipsum sed lorem. Donec tempor arcu nisl, et molestie massa scelerisque ut. Nunc at rutrum leo. Mauris metus mauris, tristique quis sapien eu, rutrum vulputate enim. </li>
                            <li>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. </li>
                            <li>Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat. Praesent varius ultrices massa at faucibus. </li>
                            <li>Aenean dignissim, orci sed faucibus pharetra, dui mi dignissim tortor, sit amet condimentum mi ligula sit amet augue. </li>
                            <li>Pellentesque vitae eros eget enim mollis placerat.</li>
                        </ol>
                    </section>
                    <section class="section intellectual-property inner-bottom-xs">
                        <h2>Termination</h2>
                        <ol>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum.</li>
                            <li>Leo metus luctus sem, vel vulputate diam ipsum sed lorem. Donec tempor arcu nisl, et molestie massa scelerisque ut. Nunc at rutrum leo. Mauris metus mauris, tristique quis sapien eu, rutrum vulputate enim. </li>
                            <li>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. </li>
                            <li>Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat. Praesent varius ultrices massa at faucibus. </li>
                            <li>Aenean dignissim, orci sed faucibus pharetra, dui mi dignissim tortor, sit amet condimentum mi ligula sit amet augue. </li>
                            <li>Pellentesque vitae eros eget enim mollis placerat.</li>
                        </ol>
                    </section>
                    <section class="section intellectual-property inner-bottom-xs">
                        <h2>Changes to this agreement</h2>
                        <p>We reserve the right, at our sole discretion, to modify or replace these Terms and Conditions by posting the updated terms on the Site. Your continued use of the Site after any such changes constitutes your acceptance of the new Terms and Conditions.</p>
                    </section>
                    <section class="section intellectual-property m-b-50">
                        <h2>Contact Us</h2>
                        <p>If you have any questions about this Agreement, please contact us filling this <a href="contact.html">contact form</a></p>
                    </section>
                </div>
            </main>
@endsection