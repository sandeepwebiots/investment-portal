
@extends('layouts.back.master')

@section('title') Dashboard | Invetex @endsection

@section('style')
<link href="{{ URL::asset('vendors/chartist/dist/chartist.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ url('/admin-dashboard') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{ url('/admin-dashboard') }}" class="m-nav__link">
                        <span class="m-nav__link-text">Dashboard</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="m-portlet">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-xl-4">

                    <!--begin:: Widgets/Stats2-1 -->
                    <div class="m-widget1">
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Total investment</h3>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-brand">+$17,800</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Total ROI</h3>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-danger">+1,800</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Total earned amount</h3>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-success">-27,49%</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end:: Widgets/Stats2-1 -->
                </div>
                <div class="col-xl-4">

                    <!--begin:: Widgets/Revenue Change-->
                    <div class="m-widget14">
                        <div class="m-widget14__header">
                            <h3 class="m-widget14__title">
                                Revenue Change
                            </h3>
                            <span class="m-widget14__desc">
                                Revenue change breakdown by cities
                            </span>
                        </div>
                        <div class="row  align-items-center">
                            <div class="col">
                                <div id="m_chart_revenue_change" class="m-widget14__chart1" style="height: 180px">
                                </div>
                            </div>
                            <div class="col">
                                <div class="m-widget14__legends">
                                    <div class="m-widget14__legend">
                                        <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                        <span class="m-widget14__legend-text">+10% New York</span>
                                    </div>
                                    <div class="m-widget14__legend">
                                        <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                        <span class="m-widget14__legend-text">-7% London</span>
                                    </div>
                                    <div class="m-widget14__legend">
                                        <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                        <span class="m-widget14__legend-text">+20% California</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end:: Widgets/Revenue Change-->
                </div>
                <div class="col-xl-4">

                    <!--begin:: Widgets/Stats2-1 -->
                    <div class="m-widget1">
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Available Balance</h3>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-brand">+$17,800</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Today's profit</h3>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-danger">+1,800</span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget1__item">
                            <div class="row m-row--no-padding align-items-center">
                                <div class="col">
                                    <h3 class="m-widget1__title">Total Return of investment</h3>
                                </div>
                                <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-success">-27,49%</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end:: Widgets/Stats2-1 -->
                </div>
            </div>
        </div>
    </div>


    <!-- <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Today's Deposit History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="deposit_tabale">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Payer Account</th>
                        <th>Type</th>
                        <th>Amount</th>            
                        <th>Payment Batch Num</th>            
                        <th>Payment Status</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Today's Withdraw History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="withdraw_tabale">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>coin</th>
                        <th>Type</th>
                        <th>Address | Account no</th>
                        <th>Amount</th>            
                        <th>Transaction id</th>            
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Today's Investment History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="invest_tabale">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User Name</th>
                        <th>Package Name</th>
                        <th>Amount</th>            
                        <th>Duration</th>            
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div> -->
</div>

@endsection

@section('script')
<script src="{{ URL::asset('vendors/raphael/raphael.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/morris.js/morris.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/chartist/dist/chartist.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/chart.js/dist/Chart.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit_tabale').DataTable();
} );

$(document).ready(function() {
    $('#withdraw_tabale').DataTable();
} );

$(document).ready(function() {
    $('#invest_tabale').DataTable();
} );
</script>
@endsection