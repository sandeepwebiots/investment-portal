@extends('layouts.back.master')

@section('title') Withdrawal Manage | Invetex @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Manage</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Withdrawal Manage</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Withdrawal Manage
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>coin</th>
                        <th>Type</th>
                        <th>Address | Account no</th>
                        <th>Amount</th>           
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($withdraw as $withd)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $withd->withdr_user->user_name }}</td>
                            <td>{{ strtoupper($withd->coin) }}</td>
                            <td>@if($withd->partner_email)
                                <span class="m-badge m-badge--info m-badge--wide" >Withdraw for partner</span>
                                @elseif($withd->type == 'perfect_money') 
                                <span class="m-badge m-badge--focus m-badge--wide" >Perfect Money</span> 
                                @elseif($withd->type == 'skrill') 
                                <span class="m-badge m-badge--primary m-badge--wide" >Skrill</span> 
                                @else 
                                <span class="m-badge m-badge--success m-badge--wide" >coinbase</span> 
                                @endif
                            </td>
                            <td>{{ $withd->address }}</td>
                            <td>{{ $withd->amount }}</td>
                            <td>@if($withd->status == 0)<span class="m-badge m-badge--warning m-badge--wide"><i class="fas fa-spinner"></i> Pending</span>
                                @elseif($withd->status == 1)<span class="m-badge m-badge--success m-badge--wide"><i class="fa fa-check"></i> Complete</span>
                                @elseif($withd->status == 2)<span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-times"></i> Cancelled</span>
                                @endif
                            </td>
                            <td>@if($withd->partner_email)
                                <a href="{{ url('withdraw-partner-approve') }}/{{ $withd->id }}" class="btn btn-success">Approve</a>
                                @else
                                <button type="button" class="btn btn-success" onclick="modelpopop({{$withd->id}})" data-toggle="modal" data-target="#txidform">Approve</button>@endif  <a href="{{ url('withdraw-reject') }}/{{ $withd->id }}" class="btn btn-danger" onclick="return confirm('Are you sure reject?');">Reject</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
        <div class="modal fade txidform" id="txidform"  role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{ url('withdraw-approve') }}" method="post"> 
                    {{ csrf_field() }} 
                        <input type="hidden" name="id" id="whith_id">
                        <div class="modal-body">
                          <input type="text" class="form-control" name="txid" placeholder="Transaction ID" required="">
                            <div>
                                <button type="submit" class="btn btn-success pull-right"  style="margin: 7px;">submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#m_table_1').DataTable();
} );
</script>
<script type="text/javascript">
function modelpopop(id)
{
    $("#whith_id").val(id);
}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {

    $('.txidform').validate({ // initialize the plugin
        rules: {
            txid: {
                required: true,
            }
        }
    });

});
</script>
@endsection