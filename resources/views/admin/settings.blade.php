@extends('layouts.dashboard.master')

@section('title') Profile @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Profile</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin-dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-xl-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header b-b-info">
                                <h5>Change Password</h5>
                            </div>
                            <div class="card-body">
                                <form class="theme-form mega-form" action="{{ url('change/password') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleInputfirstname">Old Password</label>
                                        <input type="password" name="old_password" class="form-control" placeholder="Old Password">
                                        @if($errors->has('old_password'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('old_password') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputfirstname">New Password</label>
                                        <input type="password" name="new_password" class="form-control" placeholder="New Password">
                                        @if($errors->has('new_password'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('new_password') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputlastname">Re-type New Password</label>
                                        <input type="password" name="re_password" class="form-control" placeholder="Re-type New Password">
                                        @if ($errors->has('re_password'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('re_password') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group pull-right">
                                        <button class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-sm-12 col-xl-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header b-b-info">
                                <h5>Company Detials</h5>
                            </div>
                            <div class="card-body">
                                <form class="theme-form mega-form" action="{{ url('company-details') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleInputfirstname">Email ID</label>
                                        <input type="text" name="company_email" class="form-control" value="{{ $setting->email}}" placeholder="Company Email">
                                        @if($errors->has('company_email'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('company_email') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputfirstname">Phone No</label>
                                        <input type="text" name="company_phone_no" class="form-control" value="{{ $setting->phone_no}}" placeholder="Company Phone No">
                                        @if($errors->has('company_phone_no'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('company_phone_no') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputlastname">Address</label>
                                        <input type="text" name="company_address" class="form-control" value="{{ $setting->address}}" placeholder="Company Address">
                                        @if ($errors->has('company_address'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('company_address') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group pull-right">
                                        <button class="btn btn-primary" onclick="return confirm('Are you sure change ?');">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-xl-6 width100">
                <div class="card">
                    <div class="business-card">
                        <div class="row">
                            <div class="col-md-4 align-self-center pr-md-0">
                                @if($user->profile)
                                <div class="text-center">
                                    <img src="{{ URL::asset('/assets/profiles') }}/{{ $user->profile }}" class="logo mr-md-5" alt="u-logo">
                                </div>
                                @else
                                <div class="text-center">
                                    <img src="{{ URL::asset('assets/dashboard/images/45.png') }}" class="logo mr-md-5" alt="u-logo">
                                </div>
                                @endif
                            </div>
                            <div class="col-md-8 b-l-light pt-3 pt-md-0">
                                <table>
                                    <tr>
                                        <th>User&nbsp;Name&nbsp;&nbsp;&nbsp;:</th>
                                        <td>{{ $user->user_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>First&nbsp;Name&nbsp;&nbsp;&nbsp;:</th>
                                        <td>{{ $user->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Last&nbsp;Name&nbsp;&nbsp;&nbsp;:</th>
                                        <td>{{ $user->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                        <td>{{ $user->email }}</td>
                                    </tr>

                                </table>
                                <div class="pull-right">
                                    <a href="{{ url('myprofile') }}"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

@endsection