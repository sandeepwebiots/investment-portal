<!DOCTYPE html>
<html>
<head>
    <title>OrgChart | Conditional Color</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src='{{ URL::asset("slider_home/js/vendor/jquery.js") }}' type='text/javascript'></script> 
    <script src="{{ URL::asset('assets/orgchart/getorgchart.js') }} "></script>
    <link href="{{ URL::asset('assets/orgchart/getorgchart.css') }}" rel="stylesheet" />



    <style type="text/css" id="myStylesheet">
        html, body {
            margin: 0px;
            padding: 0px;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        #people {
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body onload="getTreeData()">
    <div id="people"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript"> 
   


        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }


        var hex2rgb = function (hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? [
                parseInt(result[1], 16),
                parseInt(result[2], 16),
                parseInt(result[3], 16)
            ] : null;
        };

        var rgb2hex = function (rgb) {
            return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
        };

        var interpolateColor = function (color1, color2, factor) {
            if (arguments.length < 3) { factor = 0.5; }
            var result = color1.slice();
            for (var i = 0; i < 2; i++) {
                result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
            }
            return result;
        };

        var start = hex2rgb("#008000");
        var end = hex2rgb("#cc3300");
        var max = null;
        var min = null;
        var factor = null;
        
        function getTreeData(){
          $.ajax({
            url: "{{ url('/gettreedata/'.Sentinel::getUser()->id) }}", 
            success: function(result){
                // console.log(result);
                //$("#div1").html(result);
                //setFactor(result);
                  result = jQuery.parseJSON( result );
               //result.push( { id: 1, parentId: null, name: "Amber McKenzie", salary: "$10000",  image: "images/f-11.jpg" });
                  var peopleElement = document.getElementById("people");
                    var orgChart = new getOrgChart(peopleElement, {
                        primaryFields: ["name", "email"],
                        photoFields: ["profile"],
                        enableZoom: true,
                        enableEdit: false,
                        enableDetailsView: false,
                        dataSource: result,
                        renderNodeEvent: renderNodeEventHandler
                    });  
                }
            });
        }  

        function setFactor(chart) {
            max = null;
            min = null;
            
            factor = (max - min) / 100;
        }

        function alredyassgin()
        {
            var parent_id = $('#parent').val();
            var position = $('#position').val();
            // console.log(parent_id);
            // console.log(position);
            $.ajax({    //create an ajax request to display.php
            type: "post",
            url: "{{url('alredy-assign-position')}}",             
            dataType: "html",
            data: {
                'parent_id': parent_id,
                'position' : position,
                '_token': '{{ csrf_token() }}',
               //expect html to be returned                
            },
                success: function(response){ 
                    if (response == 1) {
                        $('#position-error').html('Alredy assign this position.');
                        $('#setbutton').prop('disabled',true);

                    }else if(response == 0)
                    {
                        $('#setbutton').prop('disabled',false);
                        $('#position-error').html('');
                    }
                }
            }); 
        }

         

        function renderNodeEventHandler(sender, args) {
            var salary = args.node.data["parent_invest_amount"];
            if (!isNumeric(salary)) {
                return;
            }

            if (!factor) {
                setFactor(sender);
            }

            var val = (salary - min) / factor;
            var rgb = interpolateColor(start, end, val / 100);
            var hex = rgb2hex(rgb);
            args.content[1] = args.content[1].replace("rect", "rect style='fill: " + hex + "; stroke: " + hex + ";'")
        }
    </script>
</body>
</html>
