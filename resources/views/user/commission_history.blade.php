@extends('layouts.back.master')

@section('title') Commission History | Invetex @endsection

@section('style')
    <style type="text/css">
        #position-error{
            color: red;
        }
    </style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">History</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Commission History</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Commission History
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable "
            id="commission-hist-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Commission Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php($i=1)
                    @foreach($pv_bonus as $bonus)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $bonus->amount }}</td>
                        <td>{{ $bonus->created_at->format('d-m-Y') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        PV Points
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable " id="pv_points-hist-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Referred</th>
                        <th>Amount</th>
                        <th>Commission Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php($i=1)
                    @foreach($pv_points as $points)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $points->givepoints->user_name }}</td>
                        <td>{{ $points->points }}</td>
                        <td>{{ $points->created_at->format('d-m-Y') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Final Bonus
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-responsive">
            <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" id="final-hist-table">

                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Commission Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php($i=1)
                    @foreach($final_bonus as $bonus)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $bonus->amount }}</td>
                        <td>{{ $bonus->created_at->format('d-m-Y') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#commission-hist-table').DataTable();
    } );

    $(document).ready(function() {
        $('#final-hist-table').DataTable();
    } );

    $(document).ready(function() {
        $('#pv_points-hist-table').DataTable();
    } );
</script>
@endsection 