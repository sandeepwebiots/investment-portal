@extends('layouts.back.master')

@section('title') My Network | Invetex @endsection

@section('style')
   <!-- <link rel="stylesheet" type="text/css" href="{{ url('assets/dashboard/css/orgchart.css') }}"> -->
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 

    
    <link href="{{ URL::asset('assets/orgchart/getorgchart.css') }}" rel="stylesheet" />
   <style>
       #people {
            width: 100%;
            height: 100%;
        }
        .col-md-3.bonus-top {
            margin-top: 62px;
        }
   </style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Network</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Referrals</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-md-9">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Referrals
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{ url('refferal-tree') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="fa fa-tree" aria-hidden="true"></i>
                                        <span>Show Tree</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
<<<<<<< HEAD
                <div class="m-portlet__body table-responsive">

                    <table class="table table-striped- table-bordered table-hover table-checkable "
=======
<<<<<<< HEAD
                <div class="m-portlet__body table-responsive">

                    <table class="table table-striped- table-bordered table-hover table-checkable "
=======
                <div class="m-portlet__body">
                    <div class="table-responsive">
                    <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" 
>>>>>>> 0ede459b23da88ce1e4b7ba6faabe3a3b533f87d
>>>>>>> a3ef97728a6f30fd091fed74f030ef899437699c
                    id="referral-table">
                        <thead align="center">
                            <tr>
                                <th width="20%">#</th>
                                <th>Name</th>
                                <th>Parent</th>
                                <th>Position</th>
                                <th>Assigning</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            @php $i=1 @endphp
                            @foreach($referral_list as $ru)
                           
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td class="text-uppercase">{{ $ru->user_name }}</td>
                                    <td> {{ $ru->parentuser['user_name']}}</td>
                                    <td>@if($ru->position == 1)<span class="m-badge m-badge--brand m-badge--wide">Left</span>@elseif($ru->position == 2)<span class="m-badge m-badge--primary m-badge--wide">Right</span>@endif</td>
                                    <td>@if($ru->position == '')<a href="{{ url('assign-position') }}/{{ $ru->id }}" class="btn btn-primary">Assign</a>@else Assigned @endif</td>
                                </tr>                                        
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 bonus-top">
            <div class="bonus-center" >
                <div class="">
                 <div class="m-portlet m-portlet--fit ">
                    <div class="m-portlet__body">
                       <!--begin::Widget5-->
                       <div class="m-widget4 m-widget4--chart-bottom">
                          
                          <div class="m-widget4__item">
                             <div class="m-widget4__info">
                                <span class="m-widget4__title">
                                PV Points Left
                                </span>
                             </div>
                             <span class="m-widget4__ext">
                             <span class="m-widget4__number m--font-brand">@if(Sentinel::getUser()->left_pv > 0) {{ Sentinel::getUser()->left_pv }} @else 0 @endif</span>
                             </span>
                          </div>
                           <div class="m-widget4__item">
                             <div class="m-widget4__info">
                                <span class="m-widget4__title">
                                PV Points Right
                                </span>
                             </div>
                             <span class="m-widget4__ext">
                             <span class="m-widget4__number m--font-brand">@if(Sentinel::getUser()->left_right > 0) {{ Sentinel::getUser()->left_right }} @else 0 @endif</span>
                             </span>
                          </div>
                       </div>
                       <!--end::Widget 5-->
                    </div>
                 </div>
                </div>
                <div class="">
                    <div class="m-portlet m-portlet--fit ">
                        <div class="m-portlet__body">
                           <div class="m-widget4 m-widget4--chart-bottom">
                              
                              <div class="m-widget4__item">
                                 <div class="m-widget4__info">
                                    <span class="m-widget4__title">
                                    Total Final Bonus
                                    </span>
                                 </div>
                                 <span class="m-widget4__ext">
                                 <span class="m-widget4__number m--font-brand">{{ $final_bonus }}</span>
                                 </span>
                              </div>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="">
                 <div class="m-portlet m-portlet--fit ">
                    <div class="m-portlet__body">
                       <!--begin::Widget5-->
                       <div class="m-widget4 m-widget4--chart-bottom">
                          
                          <div class="m-widget4__item">
                             <div class="m-widget4__info">
                                <span class="m-widget4__title">
                                Total Direct Bonus
                                </span>
                             </div>
                             <span class="m-widget4__ext">
                             <span class="m-widget4__number m--font-brand">@if(Sentinel::getUser()->referral_amount > 0) {{ Sentinel::getUser()->referral_amount }} @else 0 @endif</span>
                             </span>
                          </div>
                           <div class="m-widget4__item">
                             <div class="m-widget4__info">
                                <span class="m-widget4__title">
                                Total PV Bnous
                                </span>
                             </div>
                             <span class="m-widget4__ext">
                             <span class="m-widget4__number m--font-brand">{{ $pv_bonus }}</span>
                             </span>
                          </div>
                       </div>
                       <!--end::Widget 5-->
                    </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- <script src="{{ url('assets/dashboard/js/jquery.orgchart.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/dashboard/js/tree.js') }}" type="text/javascript"></script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#referral-table').DataTable();
} );
</script>
@endsection 