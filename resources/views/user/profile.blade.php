@extends('layouts.back.master')

@section('title') Profile | Invetex @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/style1.css') }}">
@endsection

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Setting</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Setting</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Profile</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <form action="{{ url('update-profile-pic')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    @if(Sentinel::getUser()->profile != "")
                                    <img src="{{ url('/assets/profiles/'.Sentinel::getUser()->profile) }}" id="profile_pic" />
                                    @else
                                    <img src="{{ URL::asset('/assets/dashboard/images/45.png') }}" alt=""  id="profile_pic"/>
                                    @endif
                                </div>
                                <div class="image-upload">
                                    <label for="file-input set-edit" style="position: relative;">
                                        <div class="profile-edit custom-profile">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </div>
                                    </label>
                                    <input id="file-input" type="file" accept="image/x-png,image/gif,image/jpeg" name="profile_image"
                                        onchange="readURL(this);">
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">{{Sentinel::getUser()->user_name}}</span>
                                <a href="" class="m-card-profile__email m-link">{{Sentinel::getUser()->email}}</a>
                            </div>
                            <div class="col-lg-12 m--align-right">
                                <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Update</button>               
                            </div>
                        </form>
                    </div>
                    <!-- <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">Section</span>
                        </li>
                        
                        <li class="m-nav__item">
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h5 class="m-form__section">Your Sponsor Code</h5>
                                    <input value="{{ $user->referral_code }}" id="copy-refer" class="form-control col-8 m-input" readonly="">
                                </div>
                            </div>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link @if(!session('validator')) active @endif" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    Update Profile
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link @if(session('validator')) active @endif" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                    Personal Info
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane @if(!session('validator')) show active @endif" id="m_user_profile_tab_1">
                        <div class="m-portlet__body">
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{ url('update-profile-info') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group m-form__group m--margin-top-10 m--hide">
                                    <div class="alert m-alert m-alert--default" role="alert">
                                        
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">1. Profile Details</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">First Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="first_name" value="{{$user->first_name}}">
                                        @if($errors->has('first_name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span> 
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Last Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="last_name" value="{{$user->last_name}}">
                                        @if ($errors->has('last_name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Email address</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="email" value="{{$user->email}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">User Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{$user->user_name}}" readonly="">
                                    </div>
                                </div>
                                <div class="col-lg-10 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane @if(session('validator')) show active @endif" id="m_user_profile_tab_2">
                        <div class="m-portlet__body">
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{ url('update-personal-info') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">1. Personal Details</h3>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Gender</label>
                                        <div class="m-radio-inline">
                                            <label class="m-radio">
                                                <input type="radio" name="gender" value="1" @if($user->gender == 1) checked @endif> Male
                                                <span></span>
                                            </label>
                                            <label class="m-radio">
                                                <input type="radio" name="gender" value="2" @if($user->gender == 2) checked @endif> Female
                                                <span></span>
                                            </label>
                                        @if($errors->has('gender'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span> 
                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Date Of Birth</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="date" name="date_of_birth" value="{{$user->date_birth}}">
                                            @if($errors->has('date_of_birth'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('date_of_birth') }}</strong>
                                            </span> @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Mobile No</label>
                                        <div class="col-7">
                                            <div class="input-group m-input-group m-input-group--square">
                                                <div class="input-group-prepend"><span class="input-group-text">+ 91</span></div>
                                                <input class="form-control m-input" type="text" name="mobile_no" id="mobile_no" value="{{$user->phone_no}}" >
                                            </div>
                                            @if($errors->has('mobile_no'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('mobile_no') }}</strong>
                                            </span> @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-10 m--align-right">
                                        <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                    </div>
                                </form>
                                <form class="m-form m-form--fit m-form--label-align-right" action="{{ url('update/address') }}" method="post">
                                    {{ csrf_field() }}
                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">2. Address</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Address</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="address" value="{{$user->address}}">
                                        @if ($errors->has('address'))<span class="text-danger"><strong>
                                            {{ $errors->first('address') }}</strong></span> 
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">City</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="city" value="{{$user->city}}">
                                        @if ($errors->has('city'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">State</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="state" value="{{$user->state}}">
                                        @if ($errors->has('state'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Country</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="country" value="{{$user->country}}">
                                        @if ($errors->has('country'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Postcode</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="zipcode" value="{{$user->zip_code}}" min="6">
                                        @if ($errors->has('zipcode'))<span class="text-danger"><strong>
                                            {{ $errors->first('zipcode') }}</strong></span> 
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-10 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile_pic').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
<script>
    $('#mobile_no').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

</script>

<script type="text/javascript">
    document.getElementById("copy-refer").onclick = function() {
        this.select();
        document.execCommand('copy');
        toastr.success("Link Copied.",{timeOut: 1000});
    }
</script>
@endsection