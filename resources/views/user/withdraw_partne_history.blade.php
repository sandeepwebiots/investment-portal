@extends('layouts.back.master')

@section('title') Withdrawal History | Invetex @endsection

@section('style')
    <style type="text/css">
        #position-error{
            color: red;
        }
    </style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">History</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Withdrawal History</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Withdrawal History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="depo-hist-table">
                <thead align="center">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Partner Email</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Txid</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php($i=1)
                    @foreach($withdraw as $with)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $with->partner_email }}</td>
                            <td>{{ $with->amount }}</td>
                            <td>{{ $with->txid }}</td>
                            <td>@if($with->status == 0)<span class="badge badge-warning">Pending</span>
                                @elseif($with->status == 1)<span class="badge badge-success">Complete</span>
                                @elseif($with->status == 2)<span class="badge badge-danger">Reject</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#depo-hist-table').DataTable();
    } );
</script>
@endsection 