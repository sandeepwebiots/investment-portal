@extends('layouts.back.master')

@section('title') Invoice @endsection

@section('style')

@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Invoice</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Invoice</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet">
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div class="m-invoice-2">
                        <div class="m-invoice__wrapper">
                            <div class="m-invoice__head">
                                <div class="m-invoice__container m-invoice__container--centered">
                                    <div class="m-invoice__logo">
                                            <h1>Final Invoice</h1>
                                    </div>
                                    <!-- <span class="m-invoice__desc">
                                        <span>Cecilia Chapman, 711-2880 Nulla St, Mankato</span>
                                        <span>Mississippi 96522</span>
                                    </span> -->
                                    <div class="m-invoice__items">
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">DATA</span>
                                            <span class="m-invoice__text">{{ $purchases->start_date }}</span>
                                        </div>
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">INVOICE NO.</span>
                                            <span class="m-invoice__text">{{ $purchases->id }}</span>
                                        </div>
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">Payment Status</span>
                                            <span class="m-invoice__text m--font-warning"><strong>Pending</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-invoice__body m-invoice__body--centered">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>DESCRIPTION</th>
                                                <th>DETAILS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Package</td>
                                                <td>{{ $package->min }}-{{ $package->max }} USD</td>
                                            </tr>
                                            <tr>
                                                <td>Durations</td>
                                                <td>{{ $package->duration }} weeks</td>
                                            </tr>
                                            <tr>
                                                <td>Profit</td>
                                                <td>{{ $package->profit }} %</td>
                                            </tr>
                                            <tr>
                                                <td>Start Date</td>
                                                <td>{{ $purchases->start_date }}</td>
                                            </tr>
                                            <tr>
                                                <td>Valid Date</td>
                                                <td>{{ $purchases->valid_date }}</td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Status</td>
                                                <td class="m--font-warning">Pending</td>
                                            </tr>
                                            <tr>
                                                <td>Payment Type</td>
                                                <td><strong>{{ strtoupper($purchases->type) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td><strong>{{ $purchases->amount }} {{ strtoupper($purchases->type) }}</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Daily Profit Amount ( {{ $package->profit }} %)</td>
                                                <td><strong>{{ $purchases->usd_roi_amount }} USD</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center"><a href="{{ url('pay') }}/{{ $purchases->id }}" class="btn m-btn--pill m-btn--air         btn-outline-success m-btn m-btn--custom m-btn--outline-2x">Pay</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection