@extends('layouts.back.master')

@section('title') Assgin Position @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('assets/app/css/referralchart.css') }}">
<link href="{{ URL::asset('assets/orgchart/getorgchart.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ URL::asset('assets/back/css/tree.css') }}">   
@endsection

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Assgin Position</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Assgin Position</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    
    <div class="row">
        <div class="col-md-12">
            <div id="people"></div>
        </div>
    </div>
    <div class="modal fade" id="m_modal_4" >
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Assgin Position</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="m-form m-form--fit m-form--label-align-right" action="{{ url('assign-position') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="parent" id="parent">
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="m-portlet__body">
                            
                            <div class="form-group m-form__group">
                                <label for="position">Position</label>
                                <select class="form-control m-input" id="position" name="position" onchange="alredyassgin();">
                                    <option value="1">Left</option>
                                    <option value="2">Right</option>
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <span id="position-error"></span>
                            </div>
                            @if($errors->has('position'))<span class="text-danger">{{ $errors->first('position') }}</span>@endif
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m--align-right">
                                <button type="submit" id="setbutton" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--custom">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src='{{ URL::asset("slider_home/js/vendor/jquery.js") }}' type='text/javascript'></script> 
<script src="{{ URL::asset('assets/orgchart/getorgchart.js') }} "></script>
<script type="text/javascript">
$(document).ready(function() {
    getTreeData();
    $('#referral-table').DataTable();
} );
</script>
<script>
    function alredyassgin()
    {
        var parent_id = $('#parent').val();
        var position = $('#position').val();
        // console.log(parent_id);
        // console.log(position);
        $.ajax({    //create an ajax request to display.php
        type: "post",
        url: "{{url('alredy-assign-position')}}",             
        dataType: "html",
        data: {
            'parent_id': parent_id,
            'position' : position,
            '_token': '{{ csrf_token() }}',
           //expect html to be returned                
        },
            success: function(response){ 
                if (response == 1) {
                    $('#position-error').html('Alredy assign this position.');
                    $('#setbutton').prop('disabled',true);

                }else if(response == 0)
                {
                    $('#setbutton').prop('disabled',false);
                    $('#position-error').html('');
                }
            }
        }); 
    }
    var data = '{{ $children }}';
    data = data.replace( /&quot;/g, '"' ),
    jsonData = jQuery.parseJSON( data );

    


    var members = jsonData;

var testImgSrc = "{{ URL::asset('assets/dashboard/images/user/user.png') }}";
(function heya( parentId ){
    // This is slow and iterates over each object everytime.
    // Removing each item from the array before re-iterating 
    // may be faster for large datasets.
    if(parentId == null)
        parentId = parseInt('{{ $parent_id }}');

    for(var i = 0; i < members.length; i++){
        var member = members[i];            
        if(parseInt(member.parent_id) === parentId){
            var parent = parentId != parseInt('{{ $parent_id }}') ? $("#containerFor" + parentId) : $("#mainContainer"),
                memberId = member.id,
                    metaInfo = "<img src='"+testImgSrc+"'/>" + member.email + " ($" + member.user_name + ")";
            parent.append("<div class='container' id='containerFor" + memberId + "'><div class='member' data-val="+ memberId + "><img src='"+testImgSrc+"'/><br><span>"+member.user_name+"</span><div class='metaInfo'>" + metaInfo + "</div></div></div>");
            
            heya(memberId);
        } 
    }
 }( null ));

// makes it pretty:
// recursivley resizes all children to fit within the parent.
var pretty = function(){
    var self = $(this),
        children = self.children(".container"),
        // subtract 4% for margin/padding/borders.
        width = (100/children.length) - 2;
    children
        .css("width", width + "%")
        .each(pretty);
    
};
$("#mainContainer").each(pretty);
</script>
<script type="text/javascript">

    
    $('.member').on('click', function() {

        var memberId = $(this).attr('data-val');
        var assined_members = $(members).filter(function (i,n){
            return parseInt(n.parent_id)===parseInt(memberId);
        });
        if(assined_members.length >= 2)
        {
            $('#m_modal_4').modal('hide');
        }else{
            $('#m_modal_4').modal('show');
            $('#parent').val(memberId);
            alredyassgin();
        }
    })
</script>

<script type="text/javascript"> 
   
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }


        var hex2rgb = function (hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? [
                parseInt(result[1], 16),
                parseInt(result[2], 16),
                parseInt(result[3], 16)
            ] : null;
        };

        var rgb2hex = function (rgb) {
            return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
        };

        var interpolateColor = function (color1, color2, factor) {
            if (arguments.length < 3) { factor = 0.5; }
            var result = color1.slice();
            for (var i = 0; i < 2; i++) {
                result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
            }
            return result;
        };

        var start = hex2rgb("#008000");
        var end = hex2rgb("#cc3300");
        var max = null;
        var min = null;
        var factor = null;
        
        function getTreeData(){
          $.ajax({
            url: "{{ url('/gettreedata/'.Sentinel::getUser()->id) }}", 
            success: function(result){
                
                //$("#div1").html(result);
                //setFactor(result);
                  result = jQuery.parseJSON( result );
                  // console.log(result);
               //result.push( { id: 1, parentId: null, name: "Amber McKenzie", salary: "$10000",  image: "images/f-11.jpg" });
                  var peopleElement = document.getElementById("people");
                    var orgChart = new getOrgChart(peopleElement, {
                        primaryFields: ["name","name"],
                        photoFields: ["image"],
                        enableZoom: false,
                        enableEdit: false,
                        enableMove: true,
                        scale: 0.5,
                        // linkType: "B",
                        enableGridView: true,
                        enableDetailsView: false,
                        expandToLevel: 3,
                        dataSource: result,
                        renderNodeEvent: renderNodeEventHandler
                    });  
                }
            });
        }  

        function setFactor(chart) {
            max = null;
            min = null;
            
            factor = (max - min) / 100;
        }

        function alredyassgin()
        {
            var parent_id = $('#parent').val();
            var position = $('#position').val();
            // console.log(parent_id);
            // console.log(position);
            $.ajax({    //create an ajax request to display.php
            type: "post",
            url: "{{url('alredy-assign-position')}}",             
            dataType: "html",
            data: {
                'parent_id': parent_id,
                'position' : position,
                '_token': '{{ csrf_token() }}',
               //expect html to be returned                
            },
                success: function(response){ 
                    if (response == 1) {
                        $('#position-error').html('Alredy assign this position.');
                        $('#setbutton').prop('disabled',true);

                    }else if(response == 0)
                    {
                        $('#setbutton').prop('disabled',false);
                        $('#position-error').html('');
                    }
                }
            }); 
        }

         

        function renderNodeEventHandler(sender, args) {
            var salary = args.node.data["parent_invest_amount"];
            if (!isNumeric(salary)) {
                return;
            }

            if (!factor) {
                setFactor(sender);
            }

            var val = (salary - min) / factor;
            var rgb = interpolateColor(start, end, val / 100);
            var hex = rgb2hex(rgb);
            args.content[1] = args.content[1].replace("rect", "rect style='fill: " + hex + "; stroke: " + hex + ";'")
        }

        
        function getdata(e) {
            // console.log($(e).attr("data-node-id"));

            var memberId = $(e).attr("data-node-id");
            var assined_members = $(members).filter(function (i,n){
                return parseInt(n.parent_id)===parseInt(memberId);
            });
            if(assined_members.length >= 2)
            {
                $('#m_modal_4').modal('hide');
            }else{
                $('#m_modal_4').modal('show');
                $('#parent').val(memberId);
                alredyassgin();
            }
            
        }
        $(document).ready(function(e) {

            setInterval(function(){ $('a[title="GetOrgChart jquery plugin"]').hide(); }, 10);

         });
    </script>
@endsection 