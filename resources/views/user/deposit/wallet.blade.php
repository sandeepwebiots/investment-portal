@extends('layouts.back.master')

@section('title') Deposit | Invetex @endsection

@section('style')
<style type="text/css">
    #position-error{
        color: red;
    }
    .error {
        margin: 0px!important;
        color: #ff2b2b!important;
        font-size: larger;
    }
    .m-radio.m-radio> span {
        border: 1px solid #bdc3d4;
        margin-top: 14px;
    }
</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto ">
            <h3 class="m-subheader__title m-subheader__title--separator">Deposit</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <!-- <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Deposit Wallet
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="deposit-table">
                <thead align="center">
                    <tr>
                        <th width="5%">#</th>
                        <th width="10%">Coin</th>
                        <th width="40%">Total Balance</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody align="center">
                    <tr>
                        <td>1</td>
                        <td>BTC</td>
                        <td>{{ Sentinel::getuser()->btc_balance }} BTC</td>
                        <td>
                            <a href="{{ url('deposit/btc') }}" class="btn btn-primary">Deposit</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>GBP</td>
                        <td>{{ Sentinel::getuser()->gbp_balance }} <i class="fas fa-pound-sign"></i></td>
                        <td>
                            <a href="{{ url('deposit-skrill/gbp') }}" class="btn btn-skrill">Deposit with Skrill</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>USD</td>
                        <td>{{ Sentinel::getuser()->usd_balance }} <i class="fas fa-dollar-sign"></i></td>
                        <td>
                            <a href="{{ url('deposit-pm/usd') }}" class="btn btn-primary">Deposit</a>
                            <a href="{{ url('deposit-skrill/usd') }}" class="btn btn-skrill">Deposit with Skrill</a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>EURO</td>
                        <td>{{ Sentinel::getuser()->euro_balance }} <i class="fas fa-euro-sign"></i></td>
                        <td>
                            <a href="{{ url('deposit-pm/eur') }}" class="btn btn-primary">Deposit</a>
                            <a href="{{ url('deposit-skrill/eur') }}" class="btn btn-skrill">Deposit with Skrill</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> -->
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Deposit Wallet
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-section__content">
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-demo__preview">

                                <!--begin::Form-->
                                <form class="m-form" action="{{ url('deposit-type') }}" method="post" id="deposit-form">
                                    {{ csrf_field() }}
                                    <div class="errorTxt"></div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-12">
                                            <div class="m-radio-list">
                                                <label class="m-radio">
                                                    <input type="radio" name="payment_type" value="perfect_money"><img src="{{ URL::asset('assets/app/media/img/products/pm.png') }}" style="width: 100px" alt=""> &nbsp;( USD, EURO )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-list">
                                                <label class="m-radio m-radio--success">
                                                    <input type="radio" name="payment_type" value="skrill"><img src="{{ URL::asset('assets/app/media/img/products/Skrill-logo.png') }}" style="width: 100px" alt="">  &nbsp;( USD, EURO, GBP )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-inline">
                                                <label class="m-radio">
                                                    <input type="radio" name="payment_type" value="coinbase"><img src="{{ URL::asset('assets/app/media/img/products/coinbase.png') }}" style="width: 100px" alt=""> &nbsp;( BTC )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row" id="currency" style="display: none;">
                                        <div class="col-6">
                                            <div class="m-radio-inline">
                                                <select class="form-control m-input" id="select_currency" name="currency">
                                                    <option value="">Select Currency</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-inline m--align-right">
                                                    <button type="submit" class="btn btn-skrill">Deposit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <!--end::Form-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $("input[name$='payment_type']").click(function() {
        var payment = $(this).val();
        // console.log(payment);
        $('#currency').css('display','block');
        currency = '';
        if (payment == 'perfect_money') {
            currency += '<option value="usd">USD</option>\
                         <option value="eur">EURO</option>';
        }else if (payment == 'skrill') {
            currency += '<option value="usd">USD</option>\
                         <option value="eur">EURO</option>\
                         <option value="gbp">GBP</option>';
        }else if (payment == 'coinbase') {
            currency += '<option value="btc">BTC</option>';
        }
        // console.log(currency);
        $('#select_currency').html(currency);
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
<script>

    $(function() {
      $("#deposit-form").validate({
        rules: {
          payment_type: {
            required: true,
          },
          currency: "required"
        },
        messages: {
          payment_type: {
            required: "Please select deposit type.",
          },
          currency: "Please select deposit currency."
        },
        errorElement : 'div',
        errorLabelContainer: '.errorTxt'
      });
    });
</script>
@endsection
