@extends('layouts.back.master')

@section('title') Deposit {{ strtoupper($coin) }} | Coinbase @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   

</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Deposit  {{ strtoupper($coin) }}</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home "></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit via Coinbase</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-xl-4">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              Deposit with Coinbase
                            </h3>
                        </div>
                    </div>
                </div>
                    <div class="m-portlet__body">
                        <div class="m--align-center">
                            <div class="form-group m-form__group row">
                                <div class="m-form__actions form-group col-md-12" align="center">
                                    <img  src="{{ $qrcode }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="m-form__actions form-group col-md-12" align="center">
                                    <input type="text" name="address" class="form-control m-input" value="{{ $coinaddress->address }}" readonly="" align="center">
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{ strtoupper($coin) }} Deposit
                            </h3>
                        </div>
                    </div>
                </div>
      <div class="m-portlet__body">
                    <div class="table-responsive">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="deposit-table">
                        <thead align="center">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Address</th>
                                <th scope="col">Txid</th>            
                                <th scope="col">Amount</th>            
                                <th scope="col">Status</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            @php $i=1; @endphp
                            @foreach($deposit as $depo)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $depo->address }}</td>
                                    <td>{{ $depo->txid }} @if($depo->txid == Null) N/A @endif</td>
                                    <td>{{ $depo->amount }}</td>
                                    
                                    <td>@if($depo->status == 0)<span class="badge badge-warning">Pending</span>
                                        @elseif($depo->status == 1)<span class="badge badge-success">Complete</span>
                                        @elseif($depo->status == 2)<span class="badge badge-danger">Cancelled</span>
                                        @endif
                                    </td>
                                    <td>{{ $depo->created_at->format('d M Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit-table').DataTable();
} );
</script>
@endsection