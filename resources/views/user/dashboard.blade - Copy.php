@extends('layouts.back.master')

@section('title') Dashboard | Invetex @endsection

@section('style')

@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Dashboard</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-md-6 col-xl-3 dashboard">
            <div class="card">
                <div class="card-body text-center">
                    <h3 class="text-left">{{ Sentinel::getUser()->btc_balance }} &nbsp; <i class="fab fa-btc"></i></h3>
                    <h5 class="text-left">
                        BTC Balance </h5>
                    <div class="svg-img">
                        <img src="{{ URL::asset('assets/home/images/bitcoin-logo.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">
                    <div class="card-body text-center">
                        <h3 class="text-left">{{ Sentinel::getUser()->usd_balance }} &nbsp; <i class="fas fa-dollar-sign"></i></h3>
                        <h5 class="text-left">
                            USD Balance</h5>
                        <div class="svg-img">
                            <img src="{{ URL::asset('assets/home/images/dollar-symbol.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">

                    <div class="card-body text-center">
                        <h3 class="text-left">{{ Sentinel::getUser()->gbp_balance }}&nbsp; <i class="fas fa-pound-sign"></i></h3>
                        <h5 class="text-left">
                            GBP Balance</h5>
                        <div class="svg-img">
                            <i class="fas fa-pound-sign" style="    color: white;font-size: 30px;"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">
                    <div class="card-body text-center">
                        <h3 class="text-left">{{ Sentinel::getUser()->euro_balance }}&nbsp; <i class="fas fa-euro-sign"></i></h3>
                        <h5 class="text-left">
                            EUR Balance</h5>
                        <div class="svg-img">
                            <img src="{{ URL::asset('assets/home/images/euro-currency-symbol.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">

                    <div class="card-body text-center">
                        <h3 class="text-left">{{ $invest }} <i class="fas fa-dollar-sign"></i></h3>
                        <h5 class="text-left">Total Investment</h5>
                        <div class="svg-img">
                        <img src="{{ URL::asset('assets/home/images/investment.png') }}" alt="">
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">
                    <div class="card-body text-center">
                        <h3 class="text-left">{{ Sentinel::getUser()->roi_amount }} <i class="fas fa-dollar-sign"></i></h3>
                        <h5 class="text-left">Total ROI</h5>
                        <div class="svg-img">
                            <img src="{{ URL::asset('assets/home/images/money.png') }}" alt=""> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">

                    <div class="card-body text-center">
                        <h3 class="text-left">{{ $pv_bonus }} <i class="fas fa-dollar-sign"></i></h3>
                        <h5 class="text-left">
                            Total Earned Amount</h5>
                        <div class="svg-img">
                            <img src="{{ URL::asset('assets/home/images/coins.png') }}" alt="">
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3 dashboard">
                <div class="card">

                    <div class="card-body text-center">
                        <h3 class="text-left">{{ Sentinel::getUser()->referral_amount }} <i class="fas fa-dollar-sign"></i></h3>
                        <h5 class="text-left">Total Referel Bonus</h5>
                        <div class="svg-img">
                            <img src="{{ URL::asset('assets/home/images/bonus.png') }}" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Today's Roi
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Package Name</th>
                        <th>Amount</th>
                        <th>Duration</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php $i = 1; @endphp
                    @foreach($todayroi as $roi)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td></td>
                            <td>{{ $roi->roi_amount }} USD</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#m_table_1').DataTable();
} );
</script>
@endsection 