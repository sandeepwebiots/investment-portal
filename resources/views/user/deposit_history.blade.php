@extends('layouts.back.master')

@section('title') Deposit History | Invetex @endsection

@section('style')
    <style type="text/css">
        #position-error{
            color: red;
        }
    </style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">History</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home "></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit History</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Deposit History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="depo-hist-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Payment Type</th>
                        <th>currency</th>
                        <th>Account</th>
                        <th>Address</th>
                        <th>Amount</th>            
                        <th>Payment Batch Num</th>            
                        <th>Payment Status</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php $i=1; @endphp
                    @foreach($deposit as $depo)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>@if($depo->type == 'PM') Perfect Money @elseif($depo->type == 'skrill') Skrill @endif</td>
                            <td>{{ $depo->coin }}</td>
                            <td>{{ $depo->payer_account }} @if($depo->payer_account == Null) N/A @endif</td>
                            <td>{{ $depo->address }} @if($depo->address == Null) N/A @endif</td>
                            <td>{{ $depo->amount }}</td>
                            <td>@if($depo->status == 2)<span class="text-danger">Cancelled</span> @endif {{ $depo->txid }} @if($depo->status == 0) N/A @endif</td>
                            <td>@if($depo->status == 0)<span class="badge badge-warning">Pending</span>
                                @elseif($depo->status == 1)<span class="badge badge-success">Complete</span>
                                @elseif($depo->status == 2)<span class="badge badge-danger">Cancelled</span>
                                @endif
                            </td>
                            <td>{{ $depo->created_at->format('d M Y') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#depo-hist-table').DataTable();
    } );
</script>
@endsection 