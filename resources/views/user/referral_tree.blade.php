@extends('layouts.back.master')

@section('title') Assgin Position @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('assets/app/css/referralchart.css') }}">
<link href="{{ URL::asset('assets/orgchart/getorgchart.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ URL::asset('assets/back/css/tree.css') }}">  
@endsection

@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Assgin Position</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Assgin Position</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    
    <div class="row">
        <div class="col-md-12">
            <div id="people"></div>
        </div> 
    </div>
    
</div>
<div class="modal fade txidform" id="txidform"  role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
          <div class="modal-content">
            <div class="m-portlet m--bg-warning m-portlet--bordered-semi m-portlet--full-height " style="margin-bottom:0 !important">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                           Packages
                        </h3>
                     </div>
                  </div>
               </div>
               <div class="m-portlet__body">
                  <div class="m-widget29 package-show">
                    
                  </div>
               </div>
            </div>
          </div>
      </div>
  </div>

@endsection

@section('script')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src='{{ URL::asset("slider_home/js/vendor/jquery.js") }}' type='text/javascript'></script> 
<script src="{{ URL::asset('assets/orgchart/getorgchart.js') }} "></script>
<script type="text/javascript">
$(document).ready(function() {
    getTreeData();
    $('#referral-table').DataTable();
} );
</script>
<script>
    function alredyassgin()
    {
        var parent_id = $('#parent').val();
        var position = $('#position').val();
        // console.log(parent_id);
        // console.log(position);
        $.ajax({    //create an ajax request to display.php
        type: "post",
        url: "{{url('alredy-assign-position')}}",             
        dataType: "html",
        data: {
            'parent_id': parent_id,
            'position' : position,
            '_token': '{{ csrf_token() }}',
           //expect html to be returned                
        },
            success: function(response){ 
                if (response == 1) {
                    $('#position-error').html('Alredy assign this position.');
                    $('#setbutton').prop('disabled',true);

                }else if(response == 0)
                {
                    $('#setbutton').prop('disabled',false);
                    $('#position-error').html('');
                }
            }
        }); 
    }

</script>
<script type="text/javascript">

    
    // $('.member').on('click', function() {

    //     var memberId = $(this).attr('data-val');
    //     var assined_members = $(members).filter(function (i,n){
    //         return parseInt(n.parent_id)===parseInt(memberId);
    //     });
    //     if(assined_members.length >= 2)
    //     {
    //         $('#m_modal_4').modal('hide');
    //     }else{
    //         $('#m_modal_4').modal('show');
    //         $('#parent').val(memberId);
    //         alredyassgin();
    //     }
    // })
</script>

<script type="text/javascript"> 
    $(document).ready(function(e) {

        setInterval(function(){ $('a[title="GetOrgChart jquery plugin"]').hide(); }, 10);

     });
   
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }


        var hex2rgb = function (hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? [
                parseInt(result[1], 16),
                parseInt(result[2], 16),
                parseInt(result[3], 16)
            ] : null;
        };

        var rgb2hex = function (rgb) {
            return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
        };

        var interpolateColor = function (color1, color2, factor) {
            if (arguments.length < 3) { factor = 0.5; }
            var result = color1.slice();
            for (var i = 0; i < 2; i++) {
                result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
            }
            return result;
        };

        var start = hex2rgb("#008000");
        var end = hex2rgb("#cc3300");
        var max = null;
        var min = null;
        var factor = null;
        var testImgSrc = "{{ URL::asset('assets/dashboard/images/user/user.png') }}";
        
        function getTreeData(){
          $.ajax({
            url: "{{ url('/gettreedata/'.Sentinel::getUser()->id) }}", 
            success: function(result){
                // console.log(result);
                //$("#div1").html(result);
                //setFactor(result);
                  result = jQuery.parseJSON( result );
               //result.push( { id: 1, parentId: null, name: "Amber McKenzie", salary: "$10000",  image: "images/f-11.jpg" });
                  // var peopleElement = document.getElementById("people");
                    var orgChart =  new getOrgChart(document.getElementById("people"), {
                        primaryFields: ["name", "name"],
                        photoFields: ["image"],
                        enableZoom: false,
                        enableEdit: false,
                        enableMove: true,
                        scale: 0.5,
                        // linkType: "B",
                        enableGridView: true,
                        enableDetailsView: false,
                        expandToLevel: 3,
                        dataSource: result,
                        renderNodeEvent: renderNodeEventHandler
                    });  
                }
            });
        }  

        function setFactor(chart) {
            max = null;
            min = null;
            
            factor = (max - min) / 100;
        }

        function alredyassgin()
        {
            var parent_id = $('#parent').val();
            var position = $('#position').val();
            // console.log(parent_id);
            // console.log(position);
            $.ajax({    //create an ajax request to display.php
            type: "post",
            url: "{{url('alredy-assign-position')}}",             
            dataType: "html",
            data: {
                'parent_id': parent_id,
                'position' : position,
                '_token': '{{ csrf_token() }}',
               //expect html to be returned                
            },
                success: function(response){ 
                    if (response == 1) {
                        $('#position-error').html('Alredy assign this position.');
                        $('#setbutton').prop('disabled',true);

                    }else if(response == 0)
                    {
                        $('#setbutton').prop('disabled',false);
                        $('#position-error').html('');
                    }
                }
            }); 
        }

         

        function renderNodeEventHandler(sender, args) {
            var salary = args.node.data["parent_invest_amount"];
            if (!isNumeric(salary)) {
                return;
            }

            if (!factor) {
                setFactor(sender);
            }

            var val = (salary - min) / factor;
            var rgb = interpolateColor(start, end, val / 100);
            var hex = rgb2hex(rgb);
            args.content[1] = args.content[1].replace("rect", "rect style='fill: " + hex + "; stroke: " + hex + ";'")
        }

        function getdata(e) {
            // console.log($(e).attr("data-node-id"));

            var memberId = $(e).attr("data-node-id");
            var package = '';
            $.ajax({    //create an ajax request to display.php
            type: "post",
            url: "{{url('referral-packages')}}",             
            dataType: "html",
            data: {
                'refe_id': memberId,
                '_token': '{{ csrf_token() }}',
               //expect html to be returned                
            },
                success: function(response){ 
                    var obj = jQuery.parseJSON(response);
                    console.log(obj);
                    if (obj.length > 0) {
                        for(var i=0;i<obj.length;i++)
                        {
                            package+= '<div class="m-widget_content">\
                                    <h3 class="m-widget_content-title">'+obj[i]['package']['duration']+' Weeks</h3>\
                                    <div class="m-widget_content-items">\
                                       <div class="m-widget_content-item">\
                                          <span class="m--font-accent">$'+obj[i]['package']['min']+' - $'+obj[i]['package']['max']+' </span>\
                                       </div>\
                                    </div>\
                                </div>';
                        }
                    }else{
                            package+= '<div class="m-widget_content">\
                                    <h3 class="m-widget_content-title">Still Not Buy the Package</h3>\
                                </div>';
                    }
                    $('#txidform').modal('show');
                    $('.package-show').html(package);

                }
            });
            // var assined_members = $(members).filter(function (i,n){
            //     return parseInt(n.parent_id)===parseInt(memberId);
            // });
            // if(assined_members.length >= 2)
            // {
            //     $('#m_modal_4').modal('hide');
            // }else{
            //     $('#m_modal_4').modal('show');
            //     $('#parent').val(memberId);
            //     alredyassgin();
            // }
            
        }

        
        // function getdata(e) {
        //     // console.log($(e).attr("data-node-id"));

        //     var memberId = $(e).attr("data-node-id");
        //     var assined_members = $(members).filter(function (i,n){
        //         return parseInt(n.parent_id)===parseInt(memberId);
        //     });
        //     if(assined_members.length >= 2)
        //     {
        //         $('#m_modal_4').modal('hide');
        //     }else{
        //         $('#m_modal_4').modal('show');
        //         $('#parent').val(memberId);
        //         alredyassgin();
        //     }
            
        // }
    </script>
@endsection 