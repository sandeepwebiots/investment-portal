@extends('layouts.back.master')

@section('title') Dashboard | Invetex @endsection

@section('style')
<style type="text/css">
   .swal-wide{
    width:2000px !important;
    height: 1000px;
}
.swal2-popup{
    border: 15px solid #ececec;
 }
 #swal2-title h2{
    color: #000;
    font-size: 35px;
    margin-bottom: 15px;
 }
 .swal2-popup .swal2-content{
    padding: 0 5% !important;
 }
 #swal2-content h4{
    margin-top: 10px;
    margin-bottom: 10px;
 }
</style>
<link href="{{ URL::asset('vendors/chartist/dist/chartist.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="m-subheader ">
   <div class="d-flex align-items-center">
      <div class="mr-auto">
         <h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>
         <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
               <a href="{{ url('/admin-dashboard') }}" class="m-nav__link m-nav__link--icon">
               <i class="m-nav__link-icon la la-home"></i>
               </a>
            </li>
            <li class="m-nav__separator">-</li>
            <li class="m-nav__item">
               <a href="{{ url('/admin-dashboard') }}" class="m-nav__link">
               <span class="m-nav__link-text">Dashboard</span>
               </a>
            </li>
         </ul>
      </div>
   </div>
</div>
<div class="m-content">
   <div class="m-portlet">
      <div class="m-portlet__body  m-portlet__body--no-padding">
         <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-xl-4">
               <!--begin:: Widgets/Stats2-1 -->
               <div class="m-widget1">
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Available Balance</h3>
                        </div>
                        <div class="col m--align-right">
                           <h3 class="m-widget1__title"><button data-toggle="modal" data-target="#txidform" class="btn btn-skrill">View Balance</button></h3>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Today's profit</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-danger">$ {{ $daily_roi }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total Return of investment</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-success">$ {{ Sentinel::getUser()->roi_amount }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end:: Widgets/Stats2-1 -->
            </div>
            <div class="col-xl-4">
               <!--begin:: Widgets/Revenue Change-->
               <div class="m-widget14">
                  <div class="m-widget14__header">
                     <h3 class="m-widget14__title">
                        Revenue Change
                     </h3>
                     <span class="m-widget14__desc">
                     Revenue change breakdown by cities
                     </span>
                  </div>
                  <div class="row  align-items-center">
                     <div class="col">
                        <div id="m_chart_revenue_change" class="m-widget14__chart1" style="height: 180px">
                        </div>
                     </div>
                     <div class="col">
                        <div class="m-widget14__legends">
                           <div class="m-widget14__legend">
                              <span class="m-widget14__legend-bullet m--bg-accent"></span>
                              <span class="m-widget14__legend-text">+10% New York</span>
                           </div>
                           <div class="m-widget14__legend">
                              <span class="m-widget14__legend-bullet m--bg-warning"></span>
                              <span class="m-widget14__legend-text">-7% London</span>
                           </div>
                           <div class="m-widget14__legend">
                              <span class="m-widget14__legend-bullet m--bg-brand"></span>
                              <span class="m-widget14__legend-text">+20% California</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end:: Widgets/Revenue Change-->
            </div>
            <div class="col-xl-4">
               <!--begin:: Widgets/Stats2-1 -->
               <div class="m-widget1">
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total investment</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-brand">$ {{ $invest }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total Withdrawal</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-danger">$ {{ Sentinel::getUser()->roi_amount }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget1__item">
                     <div class="row m-row--no-padding align-items-center">
                        <div class="col">
                           <h3 class="m-widget1__title">Total earned amount</h3>
                        </div>
                        <div class="col m--align-right">
                           <span class="m-widget1__number m--font-success">$ {{ $pv_bonus }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end:: Widgets/Stats2-1 -->
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-4">
         <!--begin:: Widgets/Sales Stats-->
         <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Sales Stats
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <!--begin::Widget 6-->
               <div class="m-widget15">
                  <div class="m-widget15__chart" style="height:180px;">
                     <canvas id="m_chart_sales_stats"></canvas>
                  </div>
                  <div class="m-widget15__items">
                     <div class="row">
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              79%
                              </span>
                              <span class="m-widget15__text">
                              Sales Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-danger" role="progressbar" style="width: 79%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              85%
                              </span>
                              <span class="m-widget15__text">
                              Orders Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-warning" role="progressbar" style="width: 85%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              95%
                              </span>
                              <span class="m-widget15__text">
                              Profit Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-success" role="progressbar" style="width: 95%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col">
                           <div class="m-widget15__item">
                              <span class="m-widget15__stats">
                              98%
                              </span>
                              <span class="m-widget15__text">
                              Member Grow
                              </span>
                              <div class="m--space-10"></div>
                              <div class="progress m-progress--sm">
                                 <div class="progress-bar bg-primary" role="progressbar" style="width: 98%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-widget15__desc">
                     <!-- * lorem ipsum dolor sit amet consectetuer sediat elit -->
                  </div>
               </div>
               <!--end::Widget 6-->
            </div>
         </div>
         <!--end:: Widgets/Sales Stats-->
      </div>
      <div class="col-xl-4">
         <div class="m-portlet">
            <div class="col-12 m-portlet__body pb-0">
               <div class="single-item single-item-1">
                  <div class="single-item-image overlay-effect">
                     <a href="">
                     <img src="assets/app/media/img/blog/blog1.jpg" alt="" class="img-fluid "></a>
                     <div class="courses-hover-info">
                     </div>
                  </div>
                  <div class="single-item-text">
                     <h5>UNIVERSITY</h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <a href="" class="btn btn-custom">Read More</a>
                  </div>
               </div>
            </div>
            <!--end:: Widgets/Inbound Bandwidth-->
            <div class="col-12 m-portlet__body">
               <div class="single-item">
                  <div class="single-item-image overlay-effect">
                     <a href="">
                     <img src="assets/app/media/img/blog/blog2.jpg" alt="" class="img-fluid"></a>
                     <div class="courses-hover-info">
                     </div>
                  </div>
                  <div class="single-item-text">
                     <h5>  The Ultimate Drop shipping </h5>
                     <p>You might be thinking that what are the benefits of Drop shipping business? There are the following benefits of such a type of business:</p>
                     <a  class="btn btn-custom" id="readdata">Read More</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xl-4">
         <!--begin:: Widgets/Top Products-->
         <div class="m-portlet m-portlet--full-height m-portlet--fit ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Top Products
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <!--begin::Widget5-->
               <div class="m-widget4 m-widget4--chart-bottom" style="min-height: 520px">
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <span class="m-widget4__title">
                        <b>Products</b>
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__title"><b>Quantity</b></span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="{{ url('assets/demo/media/img/icon/1.png')}}" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Toys & Games
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">32810</span>
                     </span>
                  </div>
                   <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <span class="m-widget4__title">
                        <img src="{{ url('assets/demo/media/img/icon/6.png') }}" alt="" class="img-fluid ">
                        Grocery
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">31982</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <span class="m-widget4__title">
                        <img src="{{ url('assets/demo/media/img/icon/2.png') }}" alt="" class="img-fluid ">
                        Electronics
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">24861</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="{{ url('assets/demo/media/img/icon/5.png') }}" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Books
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">17428</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="assets/demo/media/img/icon/7.png" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Clothing, Shoes & Jewelry
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">13271</span>
                     </span>
                  </div>
                  <div class="m-widget4__item">
                     <div class="m-widget4__info">
                        <img src="assets/demo/media/img/icon/8.png" alt="" class="img-fluid ">
                        <span class="m-widget4__title">
                        Sports & Outdoors
                        </span>
                     </div>
                     <span class="m-widget4__ext">
                     <span class="m-widget4__number m--font-brand">6607</span>
                     </span>
                  </div>
                  
                  <div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20 height-setting" >
                     <canvas id="m_chart_trends_stats_2"></canvas>
                  </div>
               </div>
               <!--end::Widget 5-->
            </div>
         </div>
         <!--end:: Widgets/Top Products-->
      </div>
   </div>
   <div class="row">
      <div class="col-xl-6 col-lg-12">
         <!--Begin::Portlet-->
         <div class="m-portlet  m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Recent Login
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <div class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">
                  <!--Begin::Timeline 2 -->
                  <div class="m-timeline-2">
                     <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                        
                        @foreach($recent_login as $login)
                        <div class="m-timeline-2__item m--margin-top-30">
                           <span class="m-timeline-2__item-time">{{ $login->created_at->format('d-m') }} </span>
                           <div class="m-timeline-2__item-cricle">
                              <i class="fa fa-genderless m--font-success"></i>
                           </div>
                           <div class="m-timeline-2__item-text m-timeline-2__item-text--bold">
                              Signed From {{ $login->browser }} ({{ $login->process }})
                           </div>
                        </div>
                        @endforeach
                        
                     </div>
                  </div>
                  <!--End::Timeline 2 -->
               </div>
            </div>
         </div>
         <!--End::Portlet-->
      </div>
      <div class="col-xl-6 col-lg-12">
         <!--Begin::Portlet-->
         <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Recent Investment
                     </h3>
                  </div>
               </div>
               <!-- <div class="m-portlet__head-tools">
                  <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                     <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget2_tab1_content" role="tab">
                        Today
                        </a>
                     </li>
                     <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget2_tab2_content" role="tab">
                        Month
                        </a>
                     </li>
                  </ul>
               </div> -->
            </div>


            <div class="m-portlet__body">
               <div class="table-responsive">

               <table class="table table-striped- table-bordered table-hover table-checkable " id="invested-table">
                <thead align="center">
                    <tr>
                        <th>#</th>
                        <th>Package</th>
                        <th>Duration</th>
                        <th>Payment Type</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @php $i=1; @endphp
                    @foreach($invested as $invest)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $invest->package->min }}$-{{ $invest->package->max }}$</td>
                            <td>{{ $invest->package->duration }} weeks</td>
                            <td>{{ strtoupper($invest->type) }}</td>
                            <td>{{ $invest->amount }} {{ strtoupper($invest->type) }}</td>
                            <td>@if($invest->final_status == 0)<span class="badge badge-warning">Pending</span>@elseif($invest->final_status == 1)<span class="badge badge-success">Complete</span>@endif</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
         </div>
         </div>
         <!--End::Portlet-->
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="m-portlet m-portlet--mobile">
           <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                   <div class="m-portlet__head-title">
                       <h3 class="m-portlet__head-text">
                           Deposit History
                       </h3>
                   </div>
               </div>
           </div>
           <div class="m-portlet__body">
            <div class="table-responsive">
               <!--begin: Datatable -->
              <div class="table-responsive">
               <table class="table table-striped- table-bordered table-hover table-checkable "
                id="depo-hist-table">
                   <thead align="center">
                       <tr>
                           <th>#</th>
                           <th>Payment Type</th>
                           <th>currency</th>
                           <th>Account</th>
                           <th>Address</th>
                           <th>Amount</th>            
                           <th>Payment Batch Num</th>            
                           <th>Payment Status</th>
                           <th>Payment Date</th>
                       </tr>
                   </thead>
                   <tbody align="center">
                       @php $i=1; @endphp
                       @foreach($deposit as $depo)
                           <tr>
                               <td>{{ $i++ }}</td>
                               <td>@if($depo->type == 'PM') Perfect Money @elseif($depo->type == 'skrill') Skrill @endif</td>
                               <td>{{ $depo->coin }}</td>
                               <td>{{ $depo->payer_account }} @if($depo->payer_account == Null) N/A @endif</td>
                               <td>{{ $depo->address }} @if($depo->address == Null) N/A @endif</td>
                               <td>{{ $depo->amount }}</td>
                               <td>@if($depo->status == 2)<span class="text-danger">Cancelled</span> @endif {{ $depo->txid }} @if($depo->status == 0) N/A @endif</td>
                               <td>@if($depo->status == 0)<span class="badge badge-warning">Pending</span>
                                   @elseif($depo->status == 1)<span class="badge badge-success">Complete</span>
                                   @elseif($depo->status == 2)<span class="badge badge-danger">Cancelled</span>
                                   @endif
                               </td>
                               <td>{{ $depo->created_at->format('d M Y') }}</td>
                           </tr>
                       @endforeach
                   </tbody>
               </table>
              </div>
            </div>

           </div>
       </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="m-portlet m-portlet--mobile">
           <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                   <div class="m-portlet__head-title">
                       <h3 class="m-portlet__head-text">
                           Withdrawal History
                       </h3>
                   </div>
               </div>
           </div>

           <div class="m-portlet__body">
            <div class="table-responsive">

               <!--begin: Datatable -->
               <table class="table table-striped- table-bordered table-hover table-checkable " id="with-hist-table">
                   <thead align="center">
                       <tr>
                           <th>#</th>
                           <th>Currency</th>
                           <th>Withdrawa Account</th>
                           <th>Partner Email</th>
                           <th>Amount</th>
                           <th>Txid</th>
                           <th>Status</th>
                       </tr>
                   </thead>
                   <tbody align="center">
                       @php($i=1) @endphp
                       @foreach($withdraw as $with)
                           <tr>
                               <td>{{ $i++ }}</td>
                               <td>{{ $with->coin }}</td>
                               <td>{{ $with->address }} @if($with->address == Null) N/A @endif</td>
                               <td>{{ $with->partner_email }} @if($with->partner_email == Null) N/A @endif</td>
                               <td>{{ $with->amount }}</td>
                               <td>{{ $with->txid }} @if($with->txid == Null) N/A @endif</td>
                               <td>@if($with->status == 0)<span class="badge badge-warning">Pending</span>
                                   @elseif($with->status == 1)<span class="badge badge-success">Complete</span>
                                   @elseif($with->status == 2)<span class="badge badge-danger">Reject</span>
                                   @endif
                               </td>
                           </tr>
                       @endforeach
                   </tbody>
               </table>
            </div>
           </div>
       </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="m-portlet m-portlet--mobile ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Shipping Details
                     </h3>
                  </div>
               </div>
               
            </div>
            <div class="m-portlet__body table-responsive">
               <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                  <thead>
                     <tr>
                        <th>Order ID</th>
                        <th>Ship name</th>
                        <th>Ship Date</th>
                        <th>Status</th>
                        <th>Type</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>008-3230 – AZ </td>
                        <td>Emma William</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3229 – AZ</td>
                        <td>Mia Ethan</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3228 – AZ</td>
                        <td>Isabella Mason</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3227 – AZ</td>
                        <td>Retail Michael</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3226 – AZ </td>
                        <td>Abigail Noah</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3225 – AZ </td>
                        <td>Madison Jayden</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3224 – AZ </td>
                        <td>Daniel James</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3223 – AZ </td>
                        <td>Grace Joseph</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3222 – AZ </td>
                        <td>Joseph Lucas</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3221 – AZ</td>
                        <td>John Ryan</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3220 – AZ</td>
                        <td>Audrey  Nathan</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3219 – AZ</td>
                        <td>Caleb Wyatt</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Danger</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3218 – AZ</td>
                        <td>Sarah Aaron</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr> 
                     <tr>
                        <td>008-3217 – AZ</td>
                        <td>Nicholas Cameron</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3216 – AZ</td>
                        <td>Taylor Adrian</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3215 – AZ</td>
                        <td>Violet Ayden</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3214 – AZ</td>
                        <td>Sophie Grayson</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3213 – AZ</td>
                        <td>Molly Asher</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3212 – AZ</td>
                        <td>Trinity Alex</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3211 – AZ</td>
                        <td>Brooke Miles</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3210 – AZ</td>
                        <td>Santiago Colin</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3209 – AZ</td>
                        <td>Leonardo Miguel</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3208 – AZ</td>
                        <td>Victor Joel</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3207 – AZ</td>
                        <td>Kyle Patrick</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3206 – AZ</td>
                        <td>Marcus Jude</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3205 – AZ</td>
                        <td>Alejandro Grant</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3204 – AZ</td>
                        <td>Gage Sean</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3203 – AZ</td>
                        <td>Ryleigh Abel</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3202 – AZ</td>
                        <td>Hope Dean</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-success">Success</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3201 – AZ</td>
                        <td>Carly Braylon</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                     <tr>
                        <td>008-3200 – AZ</td>
                        <td>Reed Dawson</td>
                        <td>05/02/2019</td>
                        <td><span class="badge badge-danger">Canceled</span></td>
                        <td>Retail</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-6">
         <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Recent Registration History
                     </h3>
                  </div>
               </div>
            </div>
            <div class="m-portlet__body">
               <!--begin: Datatable -->
               <div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="col-sm-12 table-responsive">
                        <table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 1528px;">
                           <thead>
                              <tr role="row">
                                 <th class="dt-right sorting_disabled" rowspan="1" colspan="1" style="width: 30.5px;" aria-label="Record ID">
                                    #
                                 </th>
                                 
                                 <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 113.25px;" aria-label="Country: activate to sort column ascending">User Name</th>
                                 <!-- <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 144.25px;" aria-label="Ship City: activate to sort column ascending">Email</th> -->
                                 <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 179.25px;" aria-label="Ship Address: activate to sort column ascending">Status</th>
                                 <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 59.25px;" aria-label="Status: activate to sort column ascending">Date</th>
                              </tr>
                           </thead>
                           <tbody>
                              @php $i =1; @endphp
                              @foreach($recent_regstr as $recent)
                              <tr role="row" class="odd">
                                 <td>{{ $i++ }}</td>
                                 <td>{{ $recent->user_name }}</td><!-- 
                                 <td>{{ $recent->email }}</td> -->
                                 <td>@if($recent->status == 0)<span class="badge  badge-warning">Pending</span>@elseif($recent->status == 1)<span class="badge  badge-success">Active</span>@endif</td>
                                 <td>{{ $recent->created_at->format('d-m-Y') }}</td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xl-6">

         <!--begin:: Widgets/Best Sellers-->
         <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                     <h3 class="m-portlet__head-text">
                        Best Sellers
                     </h3>
                  </div>
               </div>
               <div class="m-portlet__head-tools">
                  <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                     <!-- <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget5_tab1_content" role="tab">
                           Last Month
                        </a>
                     </li> -->
                     <!-- <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab2_content" role="tab">
                           last Year
                        </a>
                     </li>
                     <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab3_content" role="tab">
                           All time
                        </a>
                     </li> -->
                  </ul>
               </div>
            </div>
            <div class="m-portlet__body">

               <!--begin::Content-->
               <div class="tab-content">
                  <div class="tab-pane active" id="m_widget5_tab1_content" aria-expanded="true">

                     <!--begin::m-widget5-->
                     <div class="m-widget5">
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__number">
                                    Sellers
                                 </h4>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__sales"><b>Employees</b></span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__sales"><b>Employees Net income</b></span>
                              </div>
                           </div>
                        </div>
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="{{ URL::asset('assets/app/media/img//products/Amazon.png') }}" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    Amazon.com
                                 </h4>
                                 <p>The wideest online store all over the world!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">613,300</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$3.033</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="{{ URL::asset('assets/app/media/img//products/Alibaba.png') }}" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    Alibaba.com
                                 </h4>                                 
                                 <p>Global trade starts here!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">66,421</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$1.412</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                        <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="{{ URL::asset('assets/app/media/img//products/Ebay.png') }}" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    Ebay.com
                                 </h4>
                                 <p>The trader of Most-Shopped iteams!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">14,100</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$1.016</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                         <div class="m-widget5__item">
                           <div class="m-widget5__content">
                              <div class="m-widget5__pic">
                                 <img class="m-widget7__img" src="{{ URL::asset('assets/app/media/img//products/walmart.png') }}" alt="">
                              </div>
                              <div class="m-widget5__section">
                                 <h4 class="m-widget5__title">
                                    walmart
                                 </h4>
                                 <p>One shop for all!</p>
                              </div>
                           </div>
                           <div class="m-widget5__content">
                              <div class="m-widget5__stats1">
                                 <span class="m-widget5__number">2.3 M</span><br>
                                 <span class="m-widget5__sales">employees</span>
                              </div>
                              <div class="m-widget5__stats2">
                                 <span class="m-widget5__number">$9.862</span><br>
                                 <span class="m-widget5__votes">billion</span>
                              </div>
                           </div>
                        </div>
                     </div>

                     <!--end::m-widget5-->
                  </div>
               </div>

               <!--end::Content-->
            </div>
         </div>

         <!--end:: Widgets/Best Sellers-->
      </div>
   </div>
   <div class="modal fade txidform" id="txidform"  role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
          <div class="modal-content">
            <div class="m-portlet m--bg-warning m-portlet--bordered-semi m-portlet--full-height " style="margin-bottom:0 !important">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                           Balance
                        </h3>
                     </div>
                  </div>
               </div>
               <div class="m-portlet__body">
                  <div class="m-widget29">
                     <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Skrill</h3>
                        <div class="m-widget_content-items">
                           <div class="m-widget_content-item">
                              <span>USD</span>
                              <span class="m--font-accent">{{ $skrill_usd }}</span>
                           </div>
                           <div class="m-widget_content-item">
                              <span>EUR</span>
                              <span class="m--font-brand">{{ $skrill_eur }}</span>
                           </div>
                           <div class="m-widget_content-item">
                              <span>GBP</span>
                              <span>{{ $skrill_gbp }}</span>
                           </div>
                        </div>
                     </div>
                     <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Perfect Money</h3>
                        <div class="m-widget_content-items">
                           <div class="m-widget_content-item">
                              <span>USD</span>
                              <span class="m--font-accent">{{ $pm_usd }}</span>
                           </div>
                           <div class="m-widget_content-item">
                              <span>EUR</span>
                              <span class="m--font-brand">{{ $pm_eur }}</span>
                           </div>
                        </div>
                     </div>
                     <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Coinbase</h3>
                        <div class="m-widget_content-items">
                           <div class="m-widget_content-item">
                              <span>BTC</span>
                              <span class="m--font-accent">{{ $coinbase_btc }}</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          </div>
      </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ URL::asset('vendors/raphael/raphael.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/morris.js/morris.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/chartist/dist/chartist.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('vendors/chart.js/dist/Chart.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert2@7.17.0/dist/sweetalert2.all.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#depo-hist-table').DataTable();
    });
     $(document).ready(function() {
        $('#with-hist-table').DataTable();
    });
     $(document).ready(function() {
        $('#m_table_1').DataTable();
    });
     $(document).ready(function() {
        $('#invested-table').DataTable();
    } );
</script>
<script type="text/javascript">
   
   $("#readdata").click(function(){
  // action goes here!!
  console.log('hi');
  swal({
  title: "<h2>The ultimate Drop shipping</h2> ", 
  html: "<h4>Low investment cost-</h4>Most of the people are preferring Drop shipping business because there is just low investment cost involved. By spending just a little money, you can start your own business and you can start earning.</br><h4>No business entity required-</h4>If you want to have a drop shipping business then there is no need to have a business entity. It means that without going through the formalities of a traditional business, you can start a real business of your own.</br><h4>No hassle of warehousing-</h4>you will not be bearing any hassle of warehousing or inventory management. A drop shipper does not maintain the stock of any product but he works like an agent or the middle man. You will not be focusing on packing or tracking but your main focus will be marketing and promotion. Well, it is the best part of a drop shipping business that you will not have the risk of products expiry, products damage, warehousing costs, etc. <h4>Variety of products-</h4>- In such a business, you can deal in thousands of products at a time. In fact, you can change you’re the products from your list if you don’t find them profitable. For example, you have been dealing in cosmetics and you think that you are not getting good number of orders or even if the profit margin is less then you can stop dealing in cosmetics and you can think about any other type of products. <br><h4>Big profit margin- </h4>In case of drop shipping business, you set the prices yourself. It means that you can generate big profit margin. However, you must keep it in your mind if you will setting the prices so high then customers will not find any attraction in your products as they could find the same products anywhere else at much reasonable prices. Hence, you need to be a good marketer otherwise you will not earn much through this business. </br><h4>How does Drop Shipping Business Work?</h4><p>When you search for any product online, you find that there are many competitors of the same products and there are many online stores dealing in the same products. However, you will have seen that there is some difference in the prices of even the same products. The mind of an ordinary person can just think that it is all because of the quality difference. For example, if you look for beauty products, there are so many being offered by different brands. You will find the lipstick shade of Brand A in one store in $10 but you will find the same shade of the same brand in any other store in $8. Do you think that it is because of the quality difference? Even though if it is quality difference then how you will come to know that which one is original because both products are of the same brand?</p><p>Well, there is no game of quality here but it is all because of DROP SHIPPING. What it is! It is a type of business in which orders are taken from the customers and then orders for the same product are placed from whole seller or the manufacturing company. Products are delivered directly by that third party. </p><h4>Mechanism of Drop shipping business:</h4><p>You might want to know about the mechanism of drop shipping business. There are three parties involved in the entire process. There is a customer who is actually looking for a specific product. The second party is the drop shipper who is doing this business and the third party will be the maker of the product. When drop shipper will get an order from the customer, he will order the manufacturer of the product to deliver that product at the address of his customer. It means that the function of drop shipper will be same like an agent. However, he will not show to the customer that he actually owns those products. </p><p>How simple is this! Well, no, it is not! Drop shipping seems to be a very simple business but actually it is very challenging. Nowadays, there is a very tough competition as there are thousands of sellers for the same products. If you want to be a drop shipper and if you want to make good earnings with little or no efforts through this business then you have to be expert in marketing skills. If you are good marketer then off course, you can succeed in this field but if you are not then you can get into such a business by partnering with any reputable company who has actually been dealing in drop shipping business. </p>", 
  confirmButtonText: "<u>Oky</u>", 
  customClass: 'swal-wide',
});
});
</script>
@endsection