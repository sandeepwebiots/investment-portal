@extends('layouts.back.master')

@section('title') Withdrawals | Invetex @endsection

@section('style')
<style type="text/css">
   
    .m-radio.m-radio> span {
        border: 1px solid #bdc3d4;
        margin-top: 14px;
    }
</style>
@endsection

@section('content')

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Withdrawals</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Withdrawals</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
   <!--  <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Withdrawals
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead align="center">
                    <tr>
                        <tr>
                            <th width="5%">#</th>
                            <th width="10%">Coin</th>
                            <th width="40%">Total Balance</th>
                            <th >Action</th>
                        </tr>
                    </tr>
                </thead>
                <tbody align="center">
                    <tr>
                        <td>1</td>
                        <td>BTC</td>
                        <td>{{ Sentinel::getuser()->btc_balance }} BTC</td>
                        <td>
                            <a href="{{ url('withdraw/btc') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/btc') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>USD</td>
                        <td>{{ Sentinel::getuser()->usd_balance }} <i class="fas fa-dollar-sign"></i></td>
                        <td>
                            <a href="{{ url('withdraw/usd') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/btc') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>EURO</td>
                        <td>{{ Sentinel::getuser()->euro_balance }} <i class="fas fa-euro-sign"></i></td>
                        <td>
                            <a href="{{ url('withdraw/euro') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/eur') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>GBP</td>
                        <td>{{ Sentinel::getuser()->gbp_balance }} <i class="fas fa-pound-sign"></i></td>
                        <td>
                            <a href="{{ url('withdraw/gbp') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw</a>
                            <a href="{{ url('withdraw-partner/gbp') }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp;&nbsp; withdraw partner</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> -->

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Withdrawal
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-section__content">
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-demo__preview">

                                <!--begin::Form-->
                                <form class="m-form" action="{{ url('withdraw-coin') }}" method="post" id="deposit-form">
                                    {{ csrf_field() }}
                                    <div class="errorTxt"></div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-12">
                                            <div class="m-radio-list">
                                                <label class="m-radio">
                                                    <input type="radio" name="withdraw" value="perfect_money"><img src="{{ URL::asset('assets/app/media/img/products/pm.png') }}" style="width: 100px" alt=""> &nbsp;&nbsp;&nbsp;( USD, EURO )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-list">
                                                <label class="m-radio">
                                                    <input type="radio" name="withdraw" value="skrill"> <img src="{{ URL::asset('assets/app/media/img/products/Skrill-logo.png') }}" style="width: 100px" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;( USD, EURO, GBP )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-list">
                                                <label class="m-radio">
                                                    <input type="radio" name="withdraw" value="coinbase"> <img src="{{ URL::asset('assets/app/media/img/products/coinbase.png') }}" style="width: 100px" alt=""> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;( BTC )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-inline">
                                                <label class="m-radio">
                                                    <input type="radio" name="withdraw" value="partner" onclick="withdrawpartner(this);"><img src="{{ URL::asset('assets/app/media/img/products/partner.png') }}" style="width: 100px" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;( Partner )
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row" id="currency" style="display: none;">
                                        <div class="col-6">
                                            <div class="m-radio-inline">
                                                <select class="form-control m-input" id="select_currency" name="currency">
                                                    <option value="">Select Currency</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <div class="col-9">
                                            <div class="m-radio-inline m--align-right">
                                                    <button type="submit" class="btn btn-skrill">Withdrawal</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#m_table_1').DataTable();
} );
</script>
<script>
    $("input[name$='withdraw']").click(function() {
        var payment = $(this).val();

        $('#currency').css('display','block');
        currency = '';
        if (payment == 'perfect_money') {
            currency += '<option value="usd">USD</option>\
                         <option value="eur">EURO</option>';
        }else if (payment == 'skrill') {
            currency += '<option value="usd">USD</option>\
                         <option value="eur">EURO</option>\
                         <option value="gbp">GBP</option>';
        }else if (payment == 'coinbase') {
            currency += '<option value="usd">BTC</option>';
        }
        else if (payment == 'partner') {
            currency += '<option value="btc">USD</option>\
                         <option value="eur">EURO</option>\
                         <option value="gbp">GBP</option>\
                         <option value="btc">BTC</option>';
        }
        // console.log(currency);
        $('#select_currency').html(currency);
    });

   
</script>
@endsection 