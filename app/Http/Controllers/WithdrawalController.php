<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Withdrawal;
use App\User;
use Sentinel;

class WithdrawalController extends Controller
{
    public function withdraw()
    {
    	return view('user.withdraw.withdraw');
    }

    public function withdraCoin(Request $request)
    {
        // return $request->all();
        $type = $request->withdraw;
        $coin = $request->currency;

        if($coin == 'partner') {
            return redirect('withdraw-partner/'.$type.'/'.$request->currency);
        }
        
        return redirect('withdraw/'.$type.'/'.$coin);
    }

    public function withdrawal($type,$coin)
    {   

    	$user = Sentinel::getUser();
        if ($type == 'partner') {
            $withdraw = Withdrawal::where('user_id',$user->id)->whereNotNull('partner_email')->where('coin',$coin)->where('type',$type)->orderBy('id')->get();
            return view('user.withdraw.withdraw_partner',compact('withdraw','coin','user'));
        }
    	$withdraw = Withdrawal::where('user_id',$user->id)->where('coin',$coin)->whereNull('partner_email')->where('type',$type)->orderBy('id')->get();
        if ($type == 'perfect_money') {
            $type = 'Perfect Money';
        }
    	return view('user.withdraw.withdrawal_coin',compact('coin','type','user','withdraw'));
    }

    public function postWithdraw(Request $request,$type,$coin)
    {
        $this->validate($request,[
            'withdrawal_address'   =>  'required',
            'amount'    =>  'required',
        ]);
        if ($coin == 'eur') { $coin = 'euro'; }
        $balance = $coin.'_balance';
        $user = Sentinel::getUser();

        if ($request->amount > 0) {
            if ($coin == 'euro') { $coin = 'eur'; }
            $coin = strtoupper($coin);
            $user->$balance = $user->$balance - $request->amount;
            $user->update();

            $withdraw = new Withdrawal;
            $withdraw->user_id = $user->id;
            $withdraw->coin = $coin;
            $withdraw->address = $request->withdrawal_address;
            $withdraw->amount = $request->amount;
            $withdraw->type = $type;
            $withdraw->save();

            $notification = array(
                'message' => 'Withdrawal request success wait for confirmation.', 
                'alert-type' => 'success'
            );

            return redirect()->back()->with($notification);
        }

        return redirect()->back()->with('error','Please enter the valid amount');
    }

    public function withdrawHistory()
   {
      $withdraw = Withdrawal::with('withdr_user')->where('status','<>','0')->orderBy('id','desc')->get();
      return view('admin.withdraw_history',compact('withdraw'));
   }

   public function withdreaManage()
   {
        $withdraw = Withdrawal::with('withdr_user')->where('status','0')->orderBy('id','desc')->get();
        return view('admin.withdraw_manage',compact('withdraw'));
   }

   public function withdrawReject(Request $request,$id)
   {
        $withdraw = Withdrawal::where('id',$id)->first();
        $withdraw->status = 2;
        $withdraw->update();
        $coin = strtolower($withdraw->coin).'_balance';
        
        $user = User::where('id',$withdraw->user_id)->first();
        $user->$coin = $user->$coin + $withdraw->amount;
        $user->update();

        $notification = array(
            'message' => 'Withdrawal request reject.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
   }

   public function withdrawApprove(Request $request)
   {
        $id = $request->id;
        
        $withdra = Withdrawal::find($id);
        $withdra->status = 1;
        $withdra->update();

        $notification = array(
            'message' => 'Withdrawal request approved.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
   }

   public function withdrawPartner($coin)
   {
        $user = Sentinel::getUser();
        $withdraw = Withdrawal::where('user_id',$user->id)->whereNotNull('partner_email')->where('coin',$coin)->get();
        return view('user.withdraw.withdraw_partner',compact('withdraw','coin','user'));
   }

   public function postwithdrawPartner(Request $request,$coin)
   {
        $this->validate($request,[
            'email'   =>  'required',
            'amount'    =>  'required',
        ]);

        $balance = $coin.'_balance';
        $user = Sentinel::getUser();
        $partner_email = User::whereEmail($request->email)->first();
        if ($request->email == $user->email) {
            $notification = array(
                'message' => 'This your email.', 
                'alert-type' => 'warning'
            );

            return redirect()->back()->with($notification);
        }
        if ($partner_email) {

            if ($request->amount > 0) {

                $coin = strtoupper($coin);
                $user->$balance = $user->$balance - $request->amount;
                $user->update();

                $withdraw = new Withdrawal;
                $withdraw->user_id = $user->id;
                $withdraw->coin = $coin;
                $withdraw->partner_email = $request->email;
                $withdraw->amount = $request->amount;
                $withdraw->type = 'partner';
                $withdraw->save();

                $notification = array(
                    'message' => 'Withdrawal request success wait for confirmation.', 
                    'alert-type' => 'success'
                );

                return redirect()->back()->with($notification);
            }

            return redirect()->back()->with('error','Please enter the valid amount');
        }

        $notification = array(
            'message' => 'This email not valid.', 
            'alert-type' => 'error'
        );

        return redirect()->back()->with($notification);
   }

   public function approveWithdraPartner($id)
   {
        $withdra = Withdrawal::find($id);
        $withdra->status = 1;
        $withdra->update();

        $coin = strtolower($withdra->coin).'_balance';
        $user = User::whereEmail($withdra->partner_email)->first();
        $user->$coin = $user->$coin + $withdra->amount;
        $user->update();

        $notification = array(
            'message' => 'Withdrawal for partner request approved.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
   }
}
