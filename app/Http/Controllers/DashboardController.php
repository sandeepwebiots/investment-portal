<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchase;
use App\Models\PvBonus;
use App\Models\DailyRoi;
use App\Models\Deposit;
use App\Models\Withdrawal;
use App\Models\RecentLogin;
use App\User;
use Sentinel;
use DB;
use Browser;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        $slug = Sentinel::getUser()->roles()->first()->slug;
        if (Sentinel::check()) 
        {        
            if ($slug == 'admin') 
            {
               return view('admin.dashboard'); 
            }
            else if($slug == 'user') 
            {
                $user = Sentinel::getUser();
                $pv_bonus = PvBonus::where('user_id',$user->id)->sum('amount');
                $invest = Purchase::where('user_id',$user->id)->where('final_status',1)->get();
                $daily_roi = $invest->sum('usd_roi_amount');
                $invest = $invest->sum('usd_amount');
                $deposit = Deposit::where('user_id',$user->id)->orderBy('id','desc')->get();
                $withdraw = Withdrawal::where('user_id',$user->id)->orderBy('id','desc')->get();
                $withdraw_amount = Withdrawal::where('user_id',$user->id)->where('status',1)->select(DB::raw('count(*) as amount'))->groupBy('coin')->get();
                $recent_regstr = User::where('id','<>',[$user->id,1])->orderBy('id','desc')->limit(3)->get();
                $todayroi = DailyRoi::with('purchase.package')->whereDate('created_at','=', date('Y-m-d'))->where('user_id',$user->id)->get();
                $recent_login = RecentLogin::where('user_id',$user->id)->orderBy('created_at','desc')->limit(4)->get();
                $invested = Purchase::with('package')->where('user_id',$user->id)->orderBy('id','desc')->limit(6)->get();
                $deposit = Deposit::where('user_id',$user->id)->where('status',1)->get();
                $pm_usd = $deposit->where('coin','USD')->where('type','PM')->sum('amount');
                $pm_eur = $deposit->where('coin','EUR')->where('type','PM')->sum('amount');
                $skrill_usd = $deposit->where('coin','USD')->where('type','Skrill')->sum('amount');
                $skrill_eur = $deposit->where('coin','EUR')->where('type','Skrill')->sum('amount');
                $skrill_gbp = $deposit->where('coin','GBP')->where('type','Skrill')->sum('amount');
                $coinbase_btc = $deposit->where('coin','BTC')->where('type','Coinbase')->sum('amount');

               return view('user.dashboard',compact('invest','pv_bonus','todayroi','deposit','withdraw','recent_regstr','recent_login','invested','pm_usd','pm_eur','skrill_usd','skrill_eur','skrill_gbp','coinbase_btc','daily_roi'));
            }
        }else{
            return redirect('/');
        }
    }
}
