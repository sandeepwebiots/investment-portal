<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
use App\User;
use App\Models\Referral;

class RegistrationController extends Controller
{

    public function postRegister(Request $request)
    {
        // return $request->all();

         $this->validate($request,[
            'user_name' =>  'required|unique:users|string|max:255',
            'email'     =>  'required|email|max:255|unique:users',
            'password'  =>  'required|string|min:8|max:20',
            'confirm_password' =>  'required|min:8|max:20|same:password',
            'registration_agree' => 'required',
        ]);
        
        $user = Sentinel::register(array(
            'user_name' =>  $request->user_name,
            'email'     =>  $request->email,
            'password'  =>  $request->password,
            'referral_code' => str_random(16),
        ));

        $referral_code = $request->referral_code;
        if($referral_code){
            $referal_id = User::where('referral_code',$referral_code)->first()->id;
            User::where('email',$request->email)->update(array('referral_id' => $referal_id ));

            $referral = new Referral;
            $referral->user_id = $user->id;
            $referral->referral_id = $referal_id;
            $referral->save();
        }

        $activation = Activation::create($user);
        $role = Sentinel::findRoleBySlug('user');
        $role->users()->attach($user);

        $text = "Activate Your Account.";
        $link = url('').'/activate/'.$request->email.'/'.$activation->code;

        // return view('mails.activation_mail',compact('user','link','text'));

        $this->sendActivationMail($user,$text,$link);

        return "Account Created Check Your Email.";
    }

    private function sendActivationMail($user,$text,$link)
    {
        Mail::send('mails.activation_mail',[
            'text'  =>  $text,
            'user'  =>  $user,
            'link'  =>  $link,
        ],function($message) use ($text,$user){
            $message->to($user->email)->subject('Activate Your Account.',$user->user_name);
        });
    }

    public function activationComplete($email,$code)
    {
        $user = User::whereEmail($email)->first();
        $sentinelUser = Sentinel::findById($user->id);

        if (Activation::complete($sentinelUser,$code)) {
            $user->status = 1;
            $user->save();

            $notification = array(
                'message' => 'Your account is successfully Activated', 
                'alert-type' => 'success'
            );
            return redirect('/')->with($notification);
        }else{
            $notification = array(
                'message' => 'This link is expires. please try to login', 
                'alert-type' => 'error'
            );
            return redirect('/')->with($notification);
        }
    }

    public function dust()
    {
        $this->validate($request,[
            'user_name' =>  'required|unique:users|string|max:255',
            'email'     =>  'required|email|max:255|unique:users',
            'password'  =>  'required|string|min:8|max:20',
            'confirm_password' =>  'required|min:8|max:20|same:password',
            'registration_agree' => 'required',
        ]);
        
        $user = Sentinel::register(array(
            'user_name' =>  $request->user_name,
            'email'     =>  $request->email,
            'password'  =>  $request->password,
            'referral_code' => str_random(16),
        ));

        $referral_code = $request->referral_code;
        if($referral_code){
            $referal_id = User::where('referral_code',$referral_code)->first()->id;
            User::where('email',$request->email)->update(array('referral_id' => $referal_id ));

            $referral = new Referral;
            $referral->user_id = $user->id;
            $referral->referral_id = $referal_id;
            $referral->save();
            // $referal_user = User::where('referral _code',$referral_code)->first();
            // $new_user = User::where('email',$request->email)->first();
            // $new_user->referral_id = $referal_user->id;
            // $new_user->leg = $referal_user->position;
            
            // $leg_set = User::where('parent_id',$referal_user->id)->where('leg',$referal_user->position)->first();
            // if ($leg_set) {
            //    $new_user->parent_id = $referal_user->id;
            // }
            // $new_user->update();
            // User::where('email',$request->email)->update(array('referral_id' => $referal_id ));
        }

        $activation = Activation::create($user);
        $role = Sentinel::findRoleBySlug('user');
        $role->users()->attach($user);

        $text = "Activate Your Account.";
        $link = url('').'/activate/'.$request->email.'/'.$activation->code;

        // return view('mails.activation_mail',compact('user','link','text'));

        $this->sendActivationMail($user,$text,$link);

        return "Account Created Check Your Email.";

        // $notification = array(
        //         'message' => 'Successfull register', 
        //         'alert-type' => 'success'
        //     );
        // return redirect('home')->with($notification);
    }
}
