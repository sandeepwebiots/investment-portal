<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Deposit;
use App\Models\Withdrawal;
use App\Models\Setting;
use Sentinel;
use Validator;

class AdminController extends Controller
{
    public function usersManage()
    {
      $users = User::where('id','<>','1')->where('is_delete',0)->orderBy('id','desc')->get();
      return view('admin.user_manage',compact('users'));
    }

    public function userStatus($id)
    {
      $user = User::find($id);
      if ($user->status == 1) 
      {
          $user->status = 2;
          $user->update();

      }elseif ($user->status == 2) {
          $user->status = 1;
          $user->update();
      }
      $notification = array(
              'message' => 'Successfull updated '.$user->user_name.' status.', 
              'alert-type' => 'success'
          );

      return redirect()->back()->with($notification);
    }

    public function userDelete($id)
    {
      $user = User::find($id);
      $user->status = 3;
      $user->is_delete = 3;
      $user->update();

      $notification = array(
          'message' => $user->user_name.'Account Deleted.', 
          'alert-type' => 'success'
      );

      return redirect()->back()->with($notification);
    }

    public function depositHistory()
   {
      $deposit = Deposit::with('deposit_user')->orderBy('id','desc')->get();
      return view('admin.deposit_history',compact('deposit'));
   }

   
   public function purchaseHistory()
   {
      return view('admin.purchase_history');
   }

   public function settings()
   {
      $setting = Setting::first();
      $user = Sentinel::getUser();
      return view('admin.settings',compact('setting','user'));
   }

   public function companyDetails(Request $request)
   {
      $this->validate($request,[
        'company_email' =>  'required|email',
        'company_phone_no'  =>  'required',
        'company_address' =>  'required|max:220',
      ]);

      $setting = Setting::first();
      $setting->email = $request->company_email;
      $setting->phone_no = $request->company_phone_no;
      $setting->address = $request->company_address;
      $setting->update();

      $notification = array(
          'message' => 'Updated Company Details.', 
          'alert-type' => 'success'
      );

      return redirect()->back()->with($notification);
   }

   public function accountDetails()
   {
      $setting = Setting::first();
      return view('admin.account_details',compact('setting'));
   }

   public function paymentAccounts(Request $request)
   {
      $validator = Validator::make($request->all(), [
        'usd_account' =>  'required',
        'eur_account' =>  'required',
        'api_key'  =>  'required',
        'api_secret'  =>  'required',
        'skrill_email'  =>  'required', 
      ]);


      if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);
      }
      $setting = Setting::first();
      $setting->pm_usd_account  = $request->usd_account;
      $setting->pm_eur_account  = $request->eur_account;
      $setting->skrill_email_account = $request->skrill_email;
      $setting->api_key = $request->api_key;
      $setting->api_secret = $request->api_secret;
      $setting->update();

      $notification = array(
          'message' => 'Account details updated.', 
          'alert-type' => 'success'
      );
      return redirect()->back()->with($notification);
   }

   public function company()
   {
      $company = Setting::first();
      return view('admin.company_details',compact('company'));
   }

   public function linksupdated(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'linkedin'   =>  'required',
            'facebook'   =>  'required',
            'twitter'    =>  'required',
            'instagram'  =>  'required',
        ]);

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);
        }
        $setting = Setting::first();
        $setting->linkedin_link = $request->linkedin;
        $setting->facebook_link = $request->facebook;
        $setting->instagram_link = $request->instagram;
        $setting->twitter_link = $request->twitter;
        $setting->update();

        $notification = array(
            'message' => 'Successfully updated social links.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
