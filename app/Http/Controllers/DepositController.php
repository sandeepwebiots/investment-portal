<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Deposit;
use App\Models\Setting;
use App\User;
use PerfectMoney;
use Sentinel;

class DepositController extends Controller
{
    public function deposit($coin)
    {
    	return view('user.deposit.deposit',compact('coin'));
    }

    public function depositType(Request $request)
    {
        $payment_type = $request->payment_type;
        $currency = $request->currency;
        if ($payment_type == 'perfect_money') {
            return redirect('deposit-pm/'.$currency);
        }elseif ($payment_type == 'skrill') {
            return redirect('deposit-skrill/'.$currency);
        }elseif ($payment_type == 'coinbase') {
            return redirect('deposit/'.$currency);
        }
    }

    public function depositPm($coin)
    {
        $setting = Setting::first();
        $user_id = Sentinel::getUser()->id;
    	$deposit = Deposit::where('user_id',$user_id)->where('coin',$coin)->get();
    	$coin = strtoupper($coin);
    	$payment_url = url('/payment-success');
		$nopayment_url = url('/payment-fail');
        $user = Sentinel::getUser();
        Session::put('user_id',$user->id);
        if ($coin == 'USD') {
            // return $setting->pm_usd_account;
		return PerfectMoney::render(['PAYMENT_UNITS' => 'USD','PAYEE_NAME' => 'Test Project', 'PAYEE_ACCOUNT' => $setting->pm_usd_account,'PAYMENT_URL'=> $payment_url, 'NOPAYMENT_URL' => $nopayment_url,'deposit' => $deposit, 'coin' => $coin]);
        }elseif ($coin == 'EUR') {
            return PerfectMoney::render(['PAYMENT_UNITS' => 'EUR','PAYEE_ACCOUNT' => $setting->pm_eur_account,'PAYMENT_URL'=> $payment_url, 'NOPAYMENT_URL' => $nopayment_url,'deposit' => $deposit, 'coin' => $coin]);

        }
    	// return PerfectMoney::render(['PAYMENT_UNITS' => 'EUR','PAYEE_ACCOUNT' => 'E17449253','PAYMENT_URL'=> $payment_url, 'NOPAYMENT_URL' => $nopayment_url],compact('coin','deposit'));
    	// return view('user.deposit.deposit_pm',compact('coin','deposit','rander'));
    }

    public function paymentSuccess(Request $request)
    {
        // return $request->all();
    	$user_id = Session::get('user_id');
        $deposit =  new Deposit;
        $deposit->user_id = $user_id;
        $deposit->payee_account = $request->PAYEE_ACCOUNT;
        $deposit->coin = $request->PAYMENT_UNITS;
        $deposit->amount = $request->PAYMENT_AMOUNT;
        $deposit->txid = $request->PAYMENT_BATCH_NUM;
        $deposit->payer_account = $request->PAYER_ACCOUNT;
        $deposit->status = 1;
        $deposit->type = 'PM';
        $deposit->save();

        $user = User::where('id',$user_id)->first();
        if ($request->PAYMENT_UNITS == 'USD') {
            $user->usd_balance = $user->usd_balance + $request->PAYMENT_AMOUNT;
        }elseif ($request->PAYMENT_UNITS == 'EUR') {
            $user->euro_balance = $user->euro_balance + $request->PAYMENT_AMOUNT;
        }
        $user->update();

        $notification = array(
            'message' => 'Successfull deposited amount.', 
            'alert-type' => 'success'
        );
        return redirect('deposit')->with($notification);
    }

    public function paymentFail(Request $request)
    {
    	// return $request->all();
    	$user_id = Session::get('user_id');
        $deposit =  new Deposit;
        $deposit->user_id = $user_id;
        $deposit->coin = $request->PAYMENT_UNITS;
        $deposit->amount = $request->PAYMENT_AMOUNT;
        $deposit->status = 2;
        $deposit->type = 'PM';
        $deposit->save();

        $notification = array(
            'message' => 'Canelled deposited amount.', 
            'alert-type' => 'error'
        );
    	return redirect('deposit')->with($notification);
    }

    public function depositSkrill($coin)
    {
        $user_id = Sentinel::getUser()->id;
        $setting = Setting::first();
        $deposit = Deposit::where('user_id',$user_id)->whereNotNull('type')->where('coin',$coin)->get();
        return view('user.deposit.deposit_skrill',compact('deposit','coin','setting'));
    }

    public function depositSkrillreturn(Request $request)
    {
        // return $request->all();
    }
}
