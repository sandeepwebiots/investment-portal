<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use Sentinel;
use Hash;
use Validator;

class ProfileController extends Controller
{
    public function userprofile()
    {
    	$user = Sentinel::getUser();
    	return view('user.profile',compact('user'));
    }

    public function adminprofile()
    {
    	$user = Sentinel::getUser();
        $setting = Setting::first();
    	return view('admin.profile',compact('user','setting'));
    }

    public function profileinfo(Request $request)
    {
    	$this->validate($request,[
    		'first_name'	=>	'required|max:255',
    		'last_name'		=>	'required|max:255',
    	]);

    	$user = Sentinel::getUser();
    	$user->first_name = $request->first_name;
    	$user->last_name  = $request->last_name;
    	$user->update();

    	$notification = array(
            'message' => 'Profile Info Updated.', 
            'alert-type' => 'success'
        );

    	return redirect()->back()->with($notification);
    }

    public function personalinfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gender'    =>  'required',
            'mobile_no'    =>  'required|max:10|min:10',
            'date_of_birth'     =>  'required|max:255',
        ]);
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);
        }

        $user = Sentinel::getUser();
        $user->phone_no = $request->mobile_no;
        $user->gender = $request->gender;
        $user->date_birth  = $request->date_of_birth;
        $user->update();

        $notification = array(
            'message' => 'Personal details updated.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function userAddressUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'country'     =>  'required|max:255',
            'state'     =>  'required',
            'city'     =>  'required|max:255',
            'address'     =>  'required|max:255',
            'zipcode'     =>  'required|min:6',
        ]);
        // return $request->all();
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);
        }

        $user = Sentinel::getUser();
        $user->country  = $request->country;
        $user->state  = $request->state;
        $user->city  = $request->city;
        $user->address  = $request->address;
        $user->zip_code  = $request->zipcode;
        $user->update();

        $notification = array(
            'message' => 'Profile Info Updated.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function profilepic(Request $request)
    {
    	$this->validate($request,[
    		'profile_image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
    	]);

    	$user = Sentinel::getUser();
    	
    	if ($request->hasFile('profile_image')) {
    		try{
    			$profile_path = public_path('/assets/profiles/').$user->profile;
    			unlink($profile_path);
    		}catch(\ErrorException  $e){

    		}
    		$profile = $request->file('profile_image');
    		$pic_name = time().'.'.$profile->getClientOriginalExtension();
	    	$path = public_path('assets/profiles/');
	    	$profile->move($path, $pic_name);

	    	$user->profile = $pic_name;
	    	$user->update();							
    	}
    	

    	$notification = array(
            'message' => 'Profile Photo Updated.', 
            'alert-type' => 'success'
        );
    	return redirect()->back()->with($notification);
    }

    public function setting()
    {
        $user = Sentinel::getUser();
        return view('user.setting',compact('user'));
    }

    public function passwordChange()
    {
        return view('admin.change_password');
    }

    public function changepassword(Request $request)
    {
        $this->validate($request,[
            'old_password'  =>  'required|string|min:8|max:20',
            'new_password'  =>  'required|string|min:8|max:20|different:old_password',
            're_password'  =>  'required|same:new_password|string|min:8|max:20',
        ]);

        $user = Sentinel::getUser();
        $old_password = $request->old_password;

        if (Hash::check($old_password,$user->password)) {
              $user->fill([  
                    'password' => Hash::make($request->new_password) 
              ])->save();

            $notification = array(
                'message' => 'Password changed.', 
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }else{
            $notification = array(
                'message' => 'Old password does not match.', 
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    //admin
    public function updateAddress(Request $request)
    {

        $this->validate($request,[
            'country'   =>  'required|max:255',
            'state'     =>  'required|max:255',
            'city'      =>  'required|max:255',
            'address'   =>  'required|max:255',
            'postal_code'   =>  'required',
        ]);

        $user = Sentinel::getUser();
        $user->country  = $request->country;
        $user->state  = $request->state;
        $user->city  = $request->city;
        $user->address  = $request->address;
        $user->zip_code  = $request->postal_code;
        $user->update();

        $notification = array(
            'message' => 'Successfully updated address.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    
}
