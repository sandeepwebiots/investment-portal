<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activation;
use App\User;
use Alert;
use Sentinel;
use Reminder;
use Mail;
use Carbon\Carbon;
use Hash;

class ForgotPasswordController extends Controller
{
    public function forgotPassword(Request $request)
    {
    	$this->validate($request,[
    		'email' => 'required|email',
    	]);

    	$forgot_email = $request->email;
    	$user = User::whereEmail($forgot_email)->first();
    	if ($user && $user->status == 3 && $user->is_delete == 3) 
    	{
            return "Your account is deleted.";
    		
    	}
    	elseif (isset($user)) 
    	{
    		$active_user = Activation::where('user_id',$user->id)->where('completed',1)->first();

    		if (empty($active_user) || $user->status == 0) 
    		{
                return "Your account is Deactive.";
    		}
            elseif($user->status == 2){
                return "Your account is blocked contact us our team.";
            }
    		elseif (!isset($user))
    		{
                return "Reset Code already sended to your email.";
            }

            $sentinelUser = Sentinel::findById($user->id);

            $alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            $reset_string = substr(str_shuffle(str_repeat($alpha_numeric, 8)), 0, 8);

            $reminder = Reminder::exists($sentinelUser) ? : Reminder::create($sentinelUser);
            $credentials = [
                'code' => $reset_string
            ];

            Reminder::where('id', $reminder->id)->update($credentials);
            $code = $reminder->code;
            $reset_code = $reset_string;

            // return view("mails.forgot_password",compact('user','code','reset_code'));
            $this->sendForgotPasswordMail($user,$reminder->code,$reset_string);

            return 1;
           
    	}
    	else
    	{
            return 'This email not valid.';
    	}
    }

    public function sendForgotPasswordMail($user, $code, $reset_string)
    {
        Mail::send('mails.forgot_password',[
            'user' => $user,
            'code' => $code,
            'reset_code' => $reset_string
        ],function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Reset your Password.");
        });
    }

    public function restetPassword($email,$resetCode)
    {
        $user = User::where('email',$email)->first();

        $sentinelUser = Sentinel::findById($user->id);
        if($reminder = Reminder::exists($sentinelUser) ? : Reminder::create($sentinelUser)){
            $now = Carbon::now();
            $date = $reminder->created_at;
            $created = new Carbon($date);
            $totalDuration = $now->diffInSeconds($created);
            if($totalDuration>480){
                $credentials = [
                    'completed' => '1'
                ];
                $update = Reminder::where('id', $reminder->id)->update($credentials);
                $notification = array(
	                'message' => 'Your reset password link has been expired !!!', 
	                'alert-type' => 'warning'
	            );
                return redirect('/')->with($notification);
            } 
            if($resetCode==$reminder->code){
                return view('home_pages.auth.reset_password',compact('user','resetCode'));
            }
            else{
	            	$notification = array(
		                'message' => 'Your reset password link has been expired !!!', 
		                'alert-type' => 'warning'
		            );
	                return redirect('/')->with($notification);
	            }
        }
        else{
            $notification = array(
	                'message' => 'Something went wrong !!!', 
	                'alert-type' => 'warning'
	            );
            return redirect('/')->with($notification);
        }
    } 

    public function postResetPassword(Request $request,$email,$resetcode)
    {
        // return $request->all();
        $this->validate($request,[
            'new_password'  =>  'required|min:8',
            'confnewpassword'  => 'required|same:new_password'
        ]); 

        $user = Sentinel::findByCredentials(['email' => $request->email]);
        if($reminder = Reminder::complete($user,$request->resetCode, $request->new_password)) {
            $new_password = Hash::make($request->new_password);
            $user->password = $new_password;
            $user->update();

            $this->resetPasswordMail($user);
            
            $notification = array(
	            'message' => 'Please Login With Your New Password.', 
	            'alert-type' => 'success'
	        );
            return redirect('/')->with($notification);
        }else
        $notification = array(
	            'message' => 'Link is Expired.', 
	            'alert-type' => 'info'
	        );
	    return redirect('/')->with($notification);
    }

    public function resetPasswordMail($user)
    {
        Mail::send('mails.reset_password',[
            'user' => $user,
        ],function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Successfully changed your new password. ");
        });
    }
}
