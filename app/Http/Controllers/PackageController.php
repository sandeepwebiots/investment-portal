<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\Purchase;
use App\Models\Setting;
use App\Models\PvPoint;
use App\Models\PvBonus;
use App\Models\FinalBonus;
use App\Models\Bonus;
use App\Models\DailyRoi;
use Carbon\Carbon;
use App\User;
use Storage;
use Sentinel;


class PackageController extends Controller
{
    public function packages()
    {
    	$package = Package::get();
        $user = Sentinel::getUser();
    	return view('user.packages',compact('package','user'));
    }

    public function packageSelect(Request $request)
    {
    	$this->validate($request,[
    		'package'	=>	'required',
    	]);
        $user = Sentinel::getUser();
        if ($user->referral_id) {
            if ($user->parent_id) {
            	$pack = Package::where('id',$request->package)->first();
            	return view('user.package.package_select',compact('pack'));
            }
            $notification = array(
              'message' => 'Your position not set.', 
              'alert-type' => 'warning'
            );
            return redirect()->back()->with($notification);
        }
        $pack = Package::where('id',$request->package)->first();
        return view('user.package.package_select',compact('pack'));
    }

    public function previewPackage(Request $request)
    {
    	// return $request->all();
        $this->validate($request,[
            'amount'    =>  'required',
        ]);
    	$user_id = Sentinel::getUser()->id;	
    	$id = $request->package_id;

    	$package = Package::where('id',$id)->first();
		$weeks = $package->duration; 
		$days = $weeks * 7;
		$today = Carbon::now();
		$today = $today->toDateString();
		$valid_date = Carbon::now()->addDay($days);
		$valid_date = $valid_date->toDateString();


    	$purchases = new Purchase;
    	$purchases->user_id = $user_id;
    	$purchases->packg_id = $id;
    	$purchases->type = $request->currency;
        $purchases->usd_amount = $request->usd_amount;
        $purchases->amount = $request->amount;
        $purchases->usd_roi_amount = $request->roi_amount;
    	// 
    	$purchases->start_date = $today;
    	$purchases->valid_date = $valid_date;
    	$purchases->save();

    	return view('user.package.invoice',compact('purchases','package'));
    }

    public function pay($id)
    {
        
        $purchases = Purchase::find($id);
        $purchases->final_status = 1;
        $purchases->txid = str_random(22);
        $purchases->update();
        
        $currency = $purchases->type.'_balance';
        $user = User::where('id',$purchases->user_id)->first();
        $user[$currency] = $user[$currency] - $purchases->amount;
        $user->update();
        
        $referral_user = User::where('id',$user->referral_id)->first();
        if ($referral_user) {
            $this->pvPoints($user,$id);
            $refer_bonus = Setting::first()->ref_bonuse_per;
            $usd_amount = $purchases->usd_amount;
            $referral_amount = $usd_amount * $refer_bonus / 100;
            $referral_user->referral_amount = $referral_user->referral_amount + $referral_amount;
            $referral_user->update();
        }

        $notification = array(
              'message' => 'Thank you invest for this package.', 
              'alert-type' => 'success'
            );
        return redirect('invest')->with($notification);
    }

    public function pvPoints($user,$id)
    {
        $purchase = Purchase::where('id',$id)->first();
        $amount = $purchase->usd_amount;
        $pv_points= ($amount * 25)/100;

        $user_id = $user->id;
        $position = $user->position;
        $parent = $user->parent_id;

        pvpoint_entry: 

        if($parent!=''){
            //PV point table entry
            $pvPoints = new PvPoint;
            $pvPoints->user_id = $user_id;
            $pvPoints->parent_id = $parent;
            $pvPoints->purchase_id = $id;
            $pvPoints->points = $pv_points;
            $pvPoints->save();

            $this->givePvBonus($parent,$position,$pv_points,$purchase);
            //fetch parent details from user table and set new parent
            $parent_user = User::where('id',$parent)->first();
            $parent = $parent_user->parent_id;
            $position = $parent_user->position;

            Goto pvpoint_entry;
        }
    }

    public function givePvBonus($parent,$position,$pv_points,$purchase)
    {
        $parent_user = User::find($parent);

        if ($position == 1) {
            $parent_user->left_pv = $parent_user->left_pv + $pv_points;
        }elseif ($position == 2) {
            $parent_user->right_pv = $parent_user->right_pv + $pv_points;
        }

        if ($parent_user->right_pv >= $parent_user->left_pv) {

            $parent_user->right_pv = $parent_user->right_pv - $parent_user->left_pv;
            $pvBonus = $parent_user->left_pv / 2 ;
            $parent_user->left_pv = 0; 
            $parent_user->usd_balance = $parent_user->usd_balance + $pvBonus;

        }
        elseif ($parent_user->right_pv < $parent_user->left_pv) {
            $parent_user->left_pv = $parent_user->left_pv - $parent_user->right_pv;
            $pvBonus = $parent_user->right_pv / 2 ;
            $parent_user->right_pv = 0;
            $parent_user->usd_balance = $parent_user->usd_balance + $pvBonus;
        }
        
        $parent_user->parent_invest_amount =  $parent_user->parent_invest_amount + $purchase->usd_amount;
        
        $this->finalBonus($parent_user);
        
        $parent_user->save();

        //insert pv bonus in new table
        if($pvBonus != 0){
            $pvbonus = new PvBonus;
            $pvbonus->user_id = $parent_user->id;
            $pvbonus->amount = $pvBonus;
            $pvbonus->save();
        }
    }

    public function finalBonus($parent_user)
    {
        $bonus = Bonus::first();
        $final_bonus = FinalBonus::where('user_id',$parent_user->id)->where('bonus_id',$bonus->id)->first();

        if (!$final_bonus) {
            if ($bonus->min < $parent_user->parent_invest_amount) {
               
               $final_bonus_amount = ($bonus->min * $bonus->bonus) / 100;
               $parent_user->usd_balance = $parent_user->usd_balance + $final_bonus_amount;

               $final_bonus = new FinalBonus;
               $final_bonus->user_id = $parent_user->id;
               $final_bonus->bonus_id = $bonus->id;
               $final_bonus->bonus_amount = $final_bonus_amount;
               $final_bonus->save();
            }
        }

        $bonus2 = Bonus::find(2);
        $final_bonus2 = FinalBonus::where('user_id',$parent_user->id)->where('bonus_id',$bonus2->id)->first();

        if (!$final_bonus2) {
            if ($bonus2->min < $parent_user->parent_invest_amount) {
               
               $final_bonus_amount = ($bonus2->min * $bonus2->bonus) / 100;
               $parent_user->usd_balance = $parent_user->usd_balance + $final_bonus_amount;

               $final_bonus = new FinalBonus;
               $final_bonus->user_id = $parent_user->id;
               $final_bonus->bonus_id = $bonus2->id;
               $final_bonus->bonus_amount = $final_bonus_amount;
               $final_bonus->save();
            }
        }

        $bonus3 = Bonus::find(3);
        $final_bonus2 = FinalBonus::where('user_id',$parent_user->id)->where('bonus_id',$bonus3->id)->first();

        if (!$final_bonus2) {
            if ($bonus3->min < $parent_user->parent_invest_amount) {
               
               $final_bonus_amount = ($bonus3->min * $bonus3->bonus) / 100;
               $parent_user->usd_balance = $parent_user->usd_balance + $final_bonus_amount;

               $final_bonus = new FinalBonus;
               $final_bonus->user_id = $parent_user->id;
               $final_bonus->bonus_id = $bonus3->id;
               $final_bonus->bonus_amount = $final_bonus_amount;
               $final_bonus->save();
            }
        }

    }

    public function currencyBalance(Request $request)
    {
        $currency = substr(strtoupper($request->currency), 0, 3);
        if($currency == 'BTC'){
            return file_get_contents('https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=BTC'); 
        }elseif ($currency == 'GBP') {
            return file_get_contents('https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=GBP');
        }elseif ($currency == 'EUR') {
            return file_get_contents('https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=EUR');
        }elseif ($currency == 'USD') {
            return 1;
        }
    }

    public function dailyroi()
    {
        // $date = Carbon::now();
        // // $date = $date->addDays(5);
        // $day = $date->toDateString();
        // $week = Carbon::now()->isWeekend();
        // if (!$week) {
        //     return "yes";
        // }else{
        //     return "no";
        // }
        
        $today = Carbon::now();
        
        
        $today = $today->toDateString();
        $purchase = Purchase::where(array('final_status' =>1, 'is_delete' =>0, 'status' =>0))->get();
        Storage::disk('local')->put('purchase - new.txt', json_encode($purchase));

        foreach ($purchase as $purch) {
            $validate = $purch->valid_date;
            $tid = $purch->txid;
            $purchs_id = $purch->id;
            $roi_amount = $purch->usd_roi_amount;
            $user_id = $purch->user_id;

            if (strtotime($today) > strtotime($validate)) {
                Storage::disk('local')->put('roi - new.txt', json_encode($purch));
                $complete = Purchase::find($purch->id);
                $complete->status = 1;
                $complete->update();
            }
            if ($today < $validate) {
                
                $saturday = Carbon::now()->isSaturday();
                if (!$saturday) {

                    $sunday = Carbon::now()->isSunday();
                    if (!$sunday) {
                        $dailyroi = DailyRoi::whereDate('created_at','=', date('Y-m-d'))->where('tid',$tid)->get();
                        
                        if (sizeof($dailyroi) == 0) {

                            $daily_roi = new DailyRoi;
                            $daily_roi->user_id = $user_id;
                            $daily_roi->purch_id = $purchs_id;
                            $daily_roi->tid = $tid;
                            $daily_roi->roi_amount = $roi_amount;
                            $daily_roi->save();
                            
                            $purchase = Purchase::where('id',$purchs_id)->first();
                            $purchase->date = date('Y-m-d');
                            $purchase->update();

                            $user = User::where('id',$user_id)->first();
                            $user->roi_amount = $user->roi_amount + $roi_amount;
                            $user->update();
                        }
                    }
                }
            }
        }
    }
    
}

