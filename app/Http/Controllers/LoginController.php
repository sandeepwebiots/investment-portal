<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Request;
use App\User;
use App\Models\RecentLogin;
use Sentinel;
use Activation;
use Browser;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request,[
            'email' =>  'required',
            'password'  =>  'required|string|min:8|max:20',
        ]);

        try {       
            $user = User::where('email',$request->email)->first();    
            if (!$user) {
                return "Email does not match.";
                
            }else{
                $activation = Activation::where('user_id',$user->id)->first();
            }
            if ($user && $user->status == 0 ) {
                return $notification = "Your account not is activated.";
            }elseif ($activation->completed == 0) {
                return "Your account is not activated.";

            }elseif ($user && $user->status == 2) {
                return "Your account is blocked.";
                
            }elseif ($user && $user->status == 3) {
                return "Your account is deleted.";
            }
            $credentials = [
                'email' =>  $request->email,
                'password' => $request->password,
            ];  

            $sentinelUser = Sentinel::authenticate($credentials);
            if ($sentinelUser) {
                
                Browser::isMobile();
                Browser::isTablet();
                Browser::isDesktop();
                $ip = $request->ip();
                $result = Browser::detect();
                $browser = Browser::browserFamily();
                $process = Browser::platformFamily();
                $login = new RecentLogin;
                $login->user_id = Sentinel::getUser()->id;
                $login->browser = $browser;
                $login->ip = $ip;
                $login->process = $process;
                $login->save();

                $slug = Sentinel::getUser()->roles()->first()->slug;

                if ($slug == 'admin') {
                    return "admin"; 
                }
                elseif ($slug == 'user') {
                    return "user";
                }
            }else{
                return "Password Wrong.";
            }  
        }catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            $delay = $e->getDelay();
            return "You are Banned with $delay seconds"; 
        }      
    }

    public function logout()
    {
        if (Sentinel::check()) {
            $slug = Sentinel::getUser()->roles()->first()->slug;
            if ($slug == 'admin' && Sentinel::check()) {
                Sentinel::logout();
                return redirect('/');
            }elseif ($slug == 'user' && Sentinel::check()) {
                Sentinel::logout();
                return redirect('/');
            }else{
                Sentinel::logout();
                return redirect('/');
            }
        }else{
            return redirect('/');
        }
    }
}
