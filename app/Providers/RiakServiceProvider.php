<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use Sentinel;
use View;

class RiakServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // $user_id = User::find(1);
        // View::share(['c_rate' =>$user_id->email]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
