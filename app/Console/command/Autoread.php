<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\Auth;
use View;
use Sentinel;
use App\Models\Package;
use App\Models\Purchase;
use App\Models\DailyRoi;
use Carbon\Carbon;
use App\User;
use Storage;
use Sentinel;
//use Illuminate\Support\Facades\Storage;
class Autotread extends Command
{
    protected $signature = 'command:autotread';
    protected $description = 'Command description';
    public function __construct()
    {
        parent::__construct();
    }
    
    public function dailyroi()
    {
               
        $today = Carbon::now();
                
        $today = $today->toDateString();
        $purchase = Purchase::where(array('final_status' =>1, 'is_delete' =>0, 'status' =>0))->get();
        Storage::disk('local')->put('purchase - new.txt', json_encode($purchase));

        foreach ($purchase as $purch) {
            $validate = $purch->valid_date;
            $tid = $purch->txid;
            $purchs_id = $purch->id;
            $roi_amount = $purch->usd_roi_amount;
            $user_id = $purch->user_id;

            if (strtotime($today) > strtotime($validate)) {
                Storage::disk('local')->put('roi - new.txt', json_encode($purch));
                $complete = Purchase::find($purch->id);
                $complete->status = 1;
                $complete->update();
            }
            if ($today < $validate) {
                
                $saturday = Carbon::now()->isSaturday();
                if (!$saturday) {

                    $sunday = Carbon::now()->isSunday();
                    if (!$sunday) {
                        $dailyroi = DailyRoi::whereDate('created_at','=', date('Y-m-d'))->where('tid',$tid)->get();
                        
                        if (sizeof($dailyroi) == 0) {

                            $daily_roi = new DailyRoi;
                            $daily_roi->user_id = $user_id;
                            $daily_roi->purch_id = $purchs_id;
                            $daily_roi->tid = $tid;
                            $daily_roi->roi_amount = $roi_amount;
                            $daily_roi->save();
                            
                            $purchase = Purchase::where('id',$purchs_id)->first();
                            $purchase->date = date('Y-m-d');
                            $purchase->update();

                            $user = User::where('id',$user_id)->first();
                            $user->roi_amount = $user->roi_amount + $roi_amount;
                            $user->update();
                        }
                    }
                }
            }
        }
    }
}


