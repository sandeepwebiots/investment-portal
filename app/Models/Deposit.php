<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    public function deposit_user()
    {
    	return $this->hasOne('\App\User','id','user_id');
    }
}
