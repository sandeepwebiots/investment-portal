<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function package()
    {
    	return $this->hasOne('App\Models\Package','id','packg_id');
    }
}
