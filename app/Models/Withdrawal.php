<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    public function withdr_user()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
}
