<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyRoi extends Model
{
    public function purchase()
    {
    	return $this->hasMany('App\Models\Purchase','id','purch_id');
    }
}
