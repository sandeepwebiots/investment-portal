<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PvPoint extends Model
{
    public function givepoints()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
}
