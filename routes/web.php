<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
});

Route::get('/news', function () {
    return view('home_pages.news');
});
Route::get('blogs', function () {
    return view('home_pages.blogs');
});

Route::get('faqs', function () {
    return view('home_pages.faq');
});

Route::get('blog-single', function () {
    return view('home_pages.blog-single');
});

Route::get('reset-password', function () {
    return view('mails.reset_password');
});

Route::get('terms-condition', function () {
    return view('home_pages.termscondition');
});

Route::get('products','HomeController@package');

Route::get('daily-roi','PackageController@dailyroi');

// registration 
Route::post('register','RegistrationController@postRegister');
Route::get('activate/{email}/{code}','RegistrationController@activationComplete');
// login
Route::post('login','LoginController@login');
Route::get('logout','LoginController@logout');

Route::post('forgot-password','ForgotPasswordController@forgotPassword');
Route::get('reset-password/{email}/{resetcode}','ForgotPasswordController@restetPassword');
Route::post('reset-password/{email}/{resetcode}','ForgotPasswordController@postResetPassword');

Route::get('about-us','HomeController@about_us');
Route::get('services','HomeController@services');

//admin
Route::group(['middleware'=>['admin']],function(){
	Route::get('admin-dashboard','DashboardController@index');
	// user manage
	Route::get('users-manage','AdminController@usersManage');
	Route::get('status-change/{id}','AdminController@userStatus');
	Route::get('user-delete/{id}','AdminController@userDelete');
	// profile
	Route::get('myprofile','ProfileController@adminprofile');
	Route::post('update-address','ProfileController@updateAddress');
	Route::post('update-profile','ProfileController@profileinfo');
	Route::post('profile-pic-update','ProfileController@profilepic'); //worke
	//password
	Route::get('password-change','ProfileController@passwordChange');
	Route::post('change/password','ProfileController@changepassword');
	//history
	Route::get('deposit-history','AdminController@depositHistory');
	Route::get('invest-history','AdminController@purchaseHistory');
	Route::get('withdraw-history','WithdrawalController@withdrawHistory');
	//company
	Route::get('company-setting','AdminController@company');
	Route::post('company-details','AdminController@companyDetails');
	Route::post('update-links','AdminController@linksupdated');
	Route::get('accounts-setting','AdminController@accountDetails');
	Route::post('update-accounts','AdminController@paymentAccounts');
	//withdraw 
	Route::get('withdraw-manage','WithdrawalController@withdreaManage');
	Route::get('withdraw-reject/{id}','WithdrawalController@withdrawReject');
	Route::post('withdraw-approve','WithdrawalController@withdrawApprove');
	Route::get('withdraw-partner-approve/{id}','WithdrawalController@approveWithdraPartner');

});

//user
Route::group(['middleware'=>['user']],function(){
	Route::get('dashboard','DashboardController@index');
	// profile
	Route::get('profile','ProfileController@userprofile');
	Route::post('update-profile-info','ProfileController@profileinfo');
	Route::post('update-personal-info','ProfileController@personalinfo');
	Route::post('update-profile-pic','ProfileController@profilepic');
	Route::post('update/address','ProfileController@userAddressUpdate');
	Route::post('change-password','ProfileController@changepassword');
	Route::get('my-network','ReferralController@myNetwork');
	Route::get('my-network-data','ReferralController@myNetworkData');
	Route::get('package-list','PackageController@packages');
	Route::get('deposit','UserController@wallet');
	Route::get('invest','UserController@invested');
	Route::post('deposit-type','DepositController@depositType');
	//coinbase
	Route::get('deposit/{coin}','CoinpaymentController@coinaddress');
	Route::get('/call-back','CoinpaymentController@callback');
	// perfect money
	Route::get('deposit-pm/{coin}','DepositController@depositPm');
	Route::post('deposit-pm','DepositController@postDepositPm');
	Route::post('payment-success','DepositController@paymentSuccess');
	Route::get('payment-success','DepositController@paymentSuccess');
	Route::get('payment-fail','DepositController@paymentFail');
	Route::get('payment-status','DepositController@paymentStatus');
	//skrill
	Route::get('deposit-skrill/{coin}','DepositController@depositSkrill');
	Route::post('deposit-skrill-return','DepositController@depositSkrillreturn');
	Route::get('deposit-skrill-return','DepositController@depositSkrillreturn');
	//withdraw
	Route::get('withdraw','WithdrawalController@withdraw');
	Route::post('withdraw-coin','WithdrawalController@withdraCoin');
	Route::get('withdraw/{type}/{coin}','WithdrawalController@withdrawal');
	Route::post('withdraw/{type}/{coin}','WithdrawalController@postWithdraw');
	//withdraw partner
	Route::get('withdraw-partner/{coin}','WithdrawalController@withdrawPartner');
	Route::post('withdraw-partner/{coin}','WithdrawalController@postwithdrawPartner');
	//package
	Route::post('package-select','PackageController@packageSelect')->name('package.select');
	Route::post('preview-package','PackageController@previewPackage')->name('package.preview');
	Route::get('balance','PackageController@currencyBalance');
	Route::get('pay/{id}','PackageController@pay');
	//reffreal 
	Route::post('referred-position','ReferralController@referrelPosition');
	Route::get('assign-position/{id}','ReferralController@assignPosition');
	Route::get('refferal-tree','ReferralController@refferalDownline');
	Route::post('assign-position','ReferralController@postassignPosition');
	Route::post('alredy-assign-position','ReferralController@alredyassginPosition');
	Route::post('referral-packages','ReferralController@referralsPackages');
	//History
	// Route::get('history/deposit','UserController@depositHistory');
	// Route::get('history/withdraw','UserController@withdrawHistory');
	Route::get('history/commission','UserController@commissionHistory');
	//changepassword
	Route::get('setting/change-password','UserController@changePassword');

	Route::get('demo-tree/{id}','ReferralController@demotree');
	Route::get('gettreedata/{id}','ReferralController@gettreedata');
});

